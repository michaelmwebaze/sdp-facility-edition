/**
 * 
 */
package org.elmis.facility.network;

/**
 * @author JBanda
 *
 */
public class NetworkPropertiesFile {
	
	private String dbdriver;
	private String dbhost;
	private String dbport;
	private String dbname;
	private String dbuser;
	private String dbpassword;
	private String dbParentServer;
	private String dbMode;
	private String clientType;

	/**
	 * 
	 */
	public NetworkPropertiesFile() {
		// TODO Auto-generated constructor stub
	}
	
	public NetworkPropertiesFile( String dbdriver, String dbhost, String dbport, 
			String dbname, String dbuser, String dbpassword, String dbParentServer, String dbMode, String clientType) {
		
		this.dbdriver = dbdriver;
		this.dbhost=dbhost;
		this.dbport=dbport;
		this.dbname=dbname;
		this.dbuser=dbuser;
		this.dbpassword=dbpassword;
		this.dbParentServer = dbParentServer;
		this.dbMode = dbMode;
		this.clientType = clientType;
		
		
		
	}
	public String getClientType() {
		return this.clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	
	
	public String getdbdriver() {
			return this.dbdriver;
		}

		public void setdbdriver(String dbdriver) {
			this.dbdriver = dbdriver;
		}
		
		public String getdbhost() {
			return this.dbhost;
		}

		public void setdbhost(String dbhost) {
			this.dbhost = dbhost;
		}
		
		public String getdbport() {
			return this.dbport;
		}

		public void setdbport(String dbport) {
			this.dbport =dbport;
		}
		
		public String getdbname() {
			return this.dbname;
		}

		public void setdbname(String dbname) {
			this.dbname = dbname;
		}
		
		public String getdbuser() {
			return this.dbuser;
		}

		public void setdbuser(String dbuser) {
			this.dbuser = dbuser;
		}
		
		public String getdbpassword() {
			return this.dbpassword;
		}

		public void setdbpassword(String dbpassword) {
			this.dbpassword = dbpassword;
		}
		
		public String getdbParentServer() {
			return this.dbParentServer;
		}

		public void setdbParentServer(String dbParentServer) {
			this.dbParentServer = dbParentServer;
		}
		
		public String getdbMode() {
			return this.dbMode;
		}

		public void setdbMode(String dbMode) {
			this.dbMode = dbMode;
		}

}
