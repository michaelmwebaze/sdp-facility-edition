package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.network.MyBatisConnectionFactory;

public class ProductsDAO {
private SqlSessionFactory sqlSessionFactory; 
	
	public ProductsDAO(){
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	

	/**
	 * Returns the list of all Contact instances from the database.
	 * @return the list of all Contact instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public List<Products> selectAll(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Products> list = session.selectList("Products.getAll");
			return list;
		} finally {
			session.close();
		}
	}

	
	
	
	
	
	
	/**
	 * Returns a Program instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A Program instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public Products selectById(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Products product = (Products) session.selectOne("Products.getById",id);
			return product;
		} finally {
			session.close();
		}
	}
	
	
	
	
	
	/**
	 * Returns a List of products from the database.
	 * @param id primary key value used for lookup.
	 * @return A Products instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public List<Products> getAllProductList(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Products> list = session.selectList("Products.getAllProducts",id);
			System.out.println(list.size() + "What is the size?");
			return list;
		} finally {
			session.close();
		}
	}
	
	
	/**
	 * Returns a Program instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A Program instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public Products selectByProgram(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Products product= (Products) session.selectOne("Products.getById",id);
			return product;
		} finally {
			session.close();
		}
	}
	
	
	

	/**
	 * Updates an instance of Program in the database.
	 * @param contact the instance to be updated.
	 */
	public void update(Products product){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.update("Products.update", product);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Insert an instance of Program into the database.
	 * @param contact the instance to be persisted.
	 */
	public void insert(Products product){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.insert("Products.insert", product);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an instance of Program from the database.
	 * @param id primary key value of the instance to be deleted.
	 */
	public void delete(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.delete("Products.deleteById", id);
			session.commit();
		} finally {
			session.close();
		}
	}
}
