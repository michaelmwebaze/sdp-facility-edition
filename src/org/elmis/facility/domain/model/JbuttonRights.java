/**
 * 
 */
package org.elmis.facility.domain.model;

/**
 * @author JBanda
 *
 */
public class JbuttonRights {
	
	private String jbuttonName;
	private String jButtonRights;
	private String jButtonAccessGroup;

	/**
	 * 
	 */
	public JbuttonRights() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getJbuttonName() {
		return jbuttonName;
	}

	public void setJbuttonName(String jbuttonName) {
		this.jbuttonName = jbuttonName;
	}

	public String getjButtonRights() {
		return jButtonRights;
	}

	public void setjButtonRights(String jButtonRights) {
		this.jButtonRights = jButtonRights;
	}

	public String getjButtonAccessGroup() {
		return jButtonAccessGroup;
	}

	public void setjButtonAccessGroup(String jButtonAccessGroup) {
		this.jButtonAccessGroup = jButtonAccessGroup;
	}

	

}
