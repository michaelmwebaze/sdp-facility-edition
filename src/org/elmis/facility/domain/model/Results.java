/**
 * this class represents test results from HIV tests,
 * used in jasper file object to generate DAR 
 */
package org.elmis.facility.domain.model;

/**
 * @author jbanda
 *
 */
public class Results {

	private String positiveResult;
	private String negativeResult;
	private String indeterminateResult;
	
	public String getPositiveResult() {
		return positiveResult;
	}

	public void setPositiveResult(String positiveResult) {
		this.positiveResult = positiveResult;
	}

	public String getNegativeResult() {
		return negativeResult;
	}

	public void setNegativeResult(String negativeResult) {
		this.negativeResult = negativeResult;
	}

	public String getIndeterminateResult() {
		return indeterminateResult;
	}

	public void setIndeterminateResult(String indeterminateResult) {
		this.indeterminateResult = indeterminateResult;
	}

	public Results() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
