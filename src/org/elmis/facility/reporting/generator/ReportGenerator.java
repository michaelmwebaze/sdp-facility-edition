package org.elmis.facility.reporting.generator;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JRViewer;

import org.elmis.facility.dashboard.util.JRViewJd;
import org.elmis.facility.reporting.dao.ArvRequisitionDao;
import org.elmis.facility.reporting.dao.EmlipRequisitionDao;
import org.elmis.facility.reporting.dao.HivRequisitionDao;
import org.elmis.facility.reporting.dao.LabRequisitionDao;
import org.elmis.facility.reporting.dao.LaboratoryTestInformationDao;
import org.elmis.facility.reporting.model.LaboratoryTestInformation;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reporting.model.ReportingPeriodRemark;
import org.elmis.facility.reporting.model.StorePhysicalCount;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.reports.utils.PropertyLoader;
import org.elmis.facility.reports.utils.ReportStorageManager;

public class ReportGenerator {
	private CalendarUtil calenderUtil = new CalendarUtil();
	private Date prevDate;
	private Map<String, Object> parameters = new HashMap<>();
	PropertyLoader prop = new PropertyLoader();
	private ReportStorageManager storageManager = new ReportStorageManager();

	public ReportGenerator()
	{
		prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
		if (!storageManager.isFolderAvailable())
			storageManager.createReportFolder();

		parameters.put("district1", prop.getDistrict());
		parameters.put("province1", prop.getProvince());
		parameters.put("facility",prop.getFacilityName());
	}
	public void generateHivReport(final String frmDate, final String toDate, final String normalOrder, final String remarks, final JDialog jDialog, boolean isEmergency)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));
		
		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(1));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		
		List<ReportRequisition> dispensedList = new HivRequisitionDao().getHivDispensaryIssues(dateFrm, dateTo, remarks, isEmergency);
		

		if (!dispensedList.isEmpty())
		{
			JasperDesign hivDesign = null;
			JasperReport hivRpt = null;
			JasperPrint hivPrnt = null;
			
			JRBeanCollectionDataSource hivDataSrc = new JRBeanCollectionDataSource(dispensedList);

			String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R HIV.pdf";
			//String sourceFileName = "C:/Users/mmwebaze/Workspaces/MyEclipse Professional/sdp/Reports/ARV_MASTER_REPORT.jasper";
			try
			{
				hivDesign = JRXmlLoader.load("./Reports/HIV_MASTER_REPORT.jrxml");
				hivRpt = JasperCompileManager.compileReport(hivDesign);
				hivPrnt  = JasperFillManager.fillReport(hivRpt, parameters, hivDataSrc);
				//printFileName = JasperFillManager.fillReportToFile(sourceFileName, parameters, arvDataSrc);
				JRViewer jv = new JRViewer(hivPrnt);
				new JRViewJd(null, true, 1, jv, pdfName, hivPrnt, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);	
			}
			catch(JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING HIV Report & Requisition for the period<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("HIV DATA PROCESSING ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No HIV Drugs Related data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateArvDrugsReport(final String frmDate, final String toDate, final String normalOrder, final String remarks, final JDialog jDialog, boolean isEmergency)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(1));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);
		

		List<ReportRequisition> dispensedList = new ArvRequisitionDao().getArvDispensaryIssues(dateFrm, dateTo, remarks, isEmergency); 

		if (!dispensedList.isEmpty())
		{
			JasperDesign arvDesign = null;
			JasperReport arvRpt = null;
			JasperPrint arvPrnt = null;
			
			JRBeanCollectionDataSource arvDataSrc = new JRBeanCollectionDataSource(dispensedList);

			String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R ARV.pdf";
			//String sourceFileName = "C:/Users/mmwebaze/Workspaces/MyEclipse Professional/sdp/Reports/ARV_MASTER_REPORT.jasper";
			try
			{
				arvDesign = JRXmlLoader.load("./Reports/ARV_MASTER_REPORT.jrxml");
				arvRpt = JasperCompileManager.compileReport(arvDesign);
				arvPrnt  = JasperFillManager.fillReport(arvRpt, parameters, arvDataSrc);
				//printFileName = JasperFillManager.fillReportToFile(sourceFileName, parameters, arvDataSrc);
				JRViewer jv = new JRViewer(arvPrnt);
				new JRViewJd(null, true, 1, jv, pdfName, arvPrnt, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);	
			}
			catch(JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING ARV Report & Requisition for the period<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("ARV DATA PROCESSING ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No ARV Drugs Related data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateLabUsageReport(String frmDate, String toDate, String normalOrder, String remarks, JDialog jDialog, boolean isEmergency)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(1));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		List<ReportRequisition> labIssuesList = new LabRequisitionDao().getLabIssues(dateFrm, dateTo, isEmergency);
		if (!labIssuesList.isEmpty())
		{
			JasperReport masterReport, equipReport;
			JasperPrint masterPrint;
			JasperDesign masterDesign, equipDesign;
			JRBeanCollectionDataSource masterDataSrc = new JRBeanCollectionDataSource(labIssuesList);
			String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" LAB USAGE REPORT.pdf";

			try {
				equipDesign = JRXmlLoader.load("./Reports/EQUIPMENT_INFO_REPORT.jrxml");
				equipReport = JasperCompileManager.compileReport(equipDesign);

				masterDesign = JRXmlLoader.load("./Reports/LAB_MASTER_REPORT.jrxml");		    
				masterReport = JasperCompileManager.compileReport(masterDesign);

				masterPrint  = JasperFillManager.fillReport(masterReport, parameters, masterDataSrc);

				//JasperExportManager.exportReportToPdfFile(masterPrint,pdfName);

				JRViewer jv = new JRViewer(masterPrint);
				new JRViewJd(null, true, 1, jv, pdfName, masterPrint, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);
			}
			catch (JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING LAB USAGE FOR THE PERIOD<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("---"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No LAB usage data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateEmlipReport(final String frmDate, final String toDate, final String normalOrder, final String remarks, final JDialog jDialog, boolean isEmergency)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(1));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		List<ReportRequisition> emlipIssuesList = new EmlipRequisitionDao().getEmlipIssues(dateFrm, dateTo, isEmergency);

		JasperDesign emlipDesign = null;
		JasperReport emlipRpt = null;
		JasperPrint emlipPrnt = null;

		JRBeanCollectionDataSource emlipDataSrc = new JRBeanCollectionDataSource(emlipIssuesList);
		if (!emlipIssuesList.isEmpty())
		{
			String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R EMLIP.pdf";
		
			try
			{
				emlipDesign = JRXmlLoader.load("./Reports/EMLIP_REPORT.jrxml");
				emlipRpt = JasperCompileManager.compileReport(emlipDesign);
				emlipPrnt  = JasperFillManager.fillReport(emlipRpt, parameters, emlipDataSrc);
				//JasperExportManager.exportReportToPdfFile(emlipPrnt,pdfName);
				JRViewer jv = new JRViewer(emlipPrnt);
				new JRViewJd(null, true, 1, jv, pdfName, emlipPrnt, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);	
			}
			catch(JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING EMLIP Report & Requisition for the period<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("EMLIP ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No EMLIP Data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateLabEquipInfoReport(String frmDate, String toDate, JDialog jDialog)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));
		
		LaboratoryTestInformationDao labTestDao = new LaboratoryTestInformationDao();
		List<LaboratoryTestInformation> listLabTestInfo = labTestDao.getLabEquipmentInfoReport(dateFrm, dateTo);

		if (!listLabTestInfo.isEmpty()){
			JasperDesign labTestDesign = null;
			JasperReport labTestRpt = null;
			JasperPrint labTestPrnt = null;

			JRBeanCollectionDataSource labTestDataSrc = new JRBeanCollectionDataSource(listLabTestInfo);
			String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" EQUIPMENT_INFO_REPORT.pdf";
			
			try
			{
				labTestDesign = JRXmlLoader.load("./Reports/EQUIPMENT_INFO_REPORT.jrxml");
				labTestRpt = JasperCompileManager.compileReport(labTestDesign);
				labTestPrnt  = JasperFillManager.fillReport(labTestRpt, parameters, labTestDataSrc);
				//JasperViewer.viewReport (labTestPrnt);
				//JasperExportManager.exportReportToPdfFile(labTestPrnt,pdfName);
				JRViewer jv = new JRViewer(labTestPrnt);
				new JRViewJd(null, true, 1, jv, pdfName, labTestPrnt, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);	
			}
			catch(JRException e)
			{
				System.out.println("EQUIPMENT_INFO_REPORT ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No Lab Equipment Data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generatePhysicalCountReport(List<StorePhysicalCount> listPhyCount, String frmDate, String toDate)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));
		
		JasperDesign physCountRptDesign = null;
		JasperReport physCountRpt = null;
		JasperPrint physCountPrnt = null;

		JRBeanCollectionDataSource physCountDataSrc = new JRBeanCollectionDataSource(listPhyCount);
		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" PHYSICAL_COUNT.pdf";
		Map<String, Object> map = new HashMap<>();
		map.put("frmDate", frmDate);
		map.put("toDate", toDate);
		try
		{
			physCountRptDesign = JRXmlLoader.load("./Reports/physical_count.jrxml");
			physCountRpt = JasperCompileManager.compileReport(physCountRptDesign);
			physCountPrnt  = JasperFillManager.fillReport(physCountRpt, map, physCountDataSrc);
			//JasperExportManager.exportReportToPdfFile(labTestPrnt,pdfName);
			JRViewer jv = new JRViewer(physCountPrnt);
			new JRViewJd(null, true, 1, jv, pdfName, physCountPrnt, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);
		}
		catch(JRException e)
		{
			System.out.println("PHYSICAL COUNT REPORT ERROR:"+e.getMessage());
		}
	}
	public void generateSubmitedRandRs(List<ReportRequisition> randrList, String fromDate, String toDate, String programArea)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(fromDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));
		
		JasperDesign submitedRandrDesign = null;
		JasperReport submitedRandrRpt = null;
		JasperPrint submitedRandrPrnt = null;
		JRBeanCollectionDataSource submitedRandrDataSrc = new JRBeanCollectionDataSource(randrList);
		//Map<String, Object> map = new HashMap<>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		String pdfName = null;
		
		try
		{
			if (programArea.equals("ARV"))
			{
				pdfName = storageManager.getReportFolder()+"/"+fromDate+" to "+toDate+" SUBMITTED ARV REPORT.pdf";
				pdfName = storageManager.getReportFolder()+"/"+fromDate+" to "+toDate+" SUBMITTED HIV REPORT.pdf";
				submitedRandrDesign = JRXmlLoader.load("./Reports/ARV_MASTER_REPORT.jrxml");
				submitedRandrRpt = JasperCompileManager.compileReport(submitedRandrDesign);
				submitedRandrPrnt  = JasperFillManager.fillReport(submitedRandrRpt, parameters, submitedRandrDataSrc);
			}
			else if (programArea.equals("HIV"))
			{
				
				//printFileName = JasperFillManager.fillReportToFile(sourceFileName, parameters, arvDataSrc);
				
			}
			else if (programArea.equals("LAB"))
			{
				pdfName = storageManager.getReportFolder()+"/"+fromDate+" to "+toDate+" SUBMITTED LAB REPORT.pdf";
			}
			else
			{
				pdfName = storageManager.getReportFolder()+"/"+fromDate+" to "+toDate+" SUBMITTED EMLIP REPORT.pdf";
			}
			JRViewer jv = new JRViewer(submitedRandrPrnt);
			new JRViewJd(null, true, 2, jv, pdfName, submitedRandrPrnt, storageManager.getReportFolder(), dateFrm, dateTo).setVisible(true);	
		}
		catch(JRException e)
		{
			System.out.println("ERROR GENERATING REPORT:"+e.getMessage());
		}
	}
}
