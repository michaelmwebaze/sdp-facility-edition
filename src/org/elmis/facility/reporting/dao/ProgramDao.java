package org.elmis.facility.reporting.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Program;

public class ProgramDao {
	private SqlSessionFactory sqlSessionFactory;

	public ProgramDao()
	{	
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Program> getPrograms()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Program> list = session.selectList("Program.getPrograms");
			return list;
		} finally {
			session.close();
		}
	}
}
