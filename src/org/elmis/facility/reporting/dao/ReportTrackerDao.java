package org.elmis.facility.reporting.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportTracker;

public class ReportTrackerDao {
	private SqlSessionFactory sqlSessionFactory;

	public ReportTrackerDao()
	{	
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<ReportTracker> selectAll()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<ReportTracker> list = session.selectList("ReportTracker.getAll");
			return list;
		} finally {
			session.close();
		}
	}
}
