/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.StorePhysicalCount;
import org.elmis.facility.reports.utils.CalendarUtil;

public class StorePhysicalCountDao {
	private SqlSessionFactory sqlSessionFactory;

	public StorePhysicalCountDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public int getpreviousPhysicalCount(@Param("prevDate") Date prevDate, @Param("productCode") String productCode)
	{
		int prevPhyCount = 0;
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap <>();
		map.put("prevDate", prevDate);
		map.put("productCode", productCode);
		try {			
			List<StorePhysicalCount> list = session.selectList("StorePhysicalCount.prevphysicalcount", map);
			
			for (StorePhysicalCount spc: list)
			{
				prevPhyCount = spc.getPreviousCount();
				//System.out.println("PHYSICAL COUNT "+prevPhyCount);
			}
			return prevPhyCount;
		} finally {
			session.close();
		}
	}
	
	public List<StorePhysicalCount> checkForPhysicalCount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String programArea)
	{
		List<StorePhysicalCount> noPhysicalCountlist = new ArrayList<>();
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		/*map.put("productCode", productCode);*/
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		try {
			
			if (programArea.equals("EM"))
				return session.selectList("StorePhysicalCount.checkphysicalemlip", map);
			else if (programArea.equals("HIV"))
				return session.selectList("StorePhysicalCount.checkphysicalhiv", map);
			else if (programArea.equals("ARV"))
				return session.selectList("StorePhysicalCount.checkphysicalarv", map);
			else
				return session.selectList("StorePhysicalCount.checkphysicallab", map);
			/*String str = df.format(toDate);
			for (StorePhysicalCount spc: list){
				
				if (!str.equals(spc.getPhysicalCountDate()))
					noPhysicalCountlist.add(spc);
			}*/
		}
		finally {
			session.close();
		}
	}
	public List<StorePhysicalCount> checkArvProductPhysicalCount(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("StorePhysicalCount.arvcheckphysicalcount", map);
		}
		finally
		{
			session.close();
		}
	}
	public static void main(String[] args)
	{
		/*CalendarUtil calendarUtil = new CalendarUtil();
		Date dateFrm = calendarUtil.getSqlDate("2013-09-01");
		Date dateTo = calendarUtil.getSqlDate("2013-09-30");
		new StorePhysicalCountDao().checkForPhysicalCount(dateFrm, dateTo);*/
	}
}
