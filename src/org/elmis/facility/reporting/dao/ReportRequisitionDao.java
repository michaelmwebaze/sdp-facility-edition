package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
//import org.elmis.facility.reporting.model.RandrArdDrug;
import org.elmis.facility.reporting.model.ReportRequisition;

/**
 * ReportRequisitionDao.java
 * Report Requisition DAO that calls the SQL statements necessary to
 * generate the required R & R
 * @author Michael Mwebaze
 * @version 1.0
 */
public class ReportRequisitionDao {

	private SqlSessionFactory sqlSessionFactory;

	/**
	 * Constructor that initializes the required session factory which can be used to 
	 * open and close connections to a database
	 */
	public ReportRequisitionDao(){

		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getSubmitedRandRs(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String programArea)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		try
		{
			if (programArea.equals("ARV"))
			{
				return session.selectList("ArvRequisition.submittedRandR", map);
			}
			else if (programArea.equals("HIV"))
			{
				return session.selectList("HivRequisition.submittedRandR", map);
			}
			else if (programArea.equals("LAB"))
			{
				return session.selectList("LabRequisition.submittedRandR", map);
			}
			else
			{
				return session.selectList("EmlipRequisition.submittedRandR", map);
			}
		}
		finally
		{
			session.close();
		}
	}
	public boolean isReportCreated(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programId)
	{
		SqlSession session = sqlSessionFactory.openSession();
		List<ReportRequisition> list = null;
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {
			if (programId == 1)//ARV
			{
				/*list = session.selectList("ReportRequisition.check_arv_report_created", map);
				System.out.println("HJY "+list.size());
				if (list.size() == 0)
					return true;
				else
					return false;*/
				return true;
			}
			else if(programId == 2)//EMLIP
			{
				list = session.selectList("ReportRequisition.check_emlip_report_created", map);
				System.out.println("Size is created "+list.size());

				if (list.size() == 0)
					return true;
				else
					return false;
			}
			else if (programId == 3)//HIV
			{System.out.println(
					"hiv");
				/*list = session.selectList("ReportRequisition.check_hiv_report_created", map);

				if (list.size() == 0)
					return true;
				else
					return false;*/
				return true;
			}
			else //LAB
			{
				list = session.selectList("ReportRequisition.check_lab_report_created", map);

				if (list.size() == 0)
					return true;
				else
					return false;
			}
		}
		finally {
			session.close();
		}	
	}
}
