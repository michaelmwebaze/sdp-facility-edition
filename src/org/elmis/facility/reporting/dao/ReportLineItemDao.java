/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.CalendarUtil;

/**
 * @author MMwebaze
 *
 */
public class ReportLineItemDao {

	private SqlSessionFactory sqlSessionFactory;
	
	public ReportLineItemDao(){

		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	
	public List<ReportRequisition> getEmlipData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap <String, Date>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {
			
			List<ReportRequisition> list = session.selectList("ReportLineItem.emlipLineItem", map);
			return list;
		} finally {
			session.close();
		}
	}
	public List<ReportRequisition> getArvData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap <String, Date>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {
			
			List<ReportRequisition> list = session.selectList("ReportLineItem.arvLineItem", map);
			return list;
		} finally {
			session.close();
		}
	}

	public static void main(String[] arg)
	{
		CalendarUtil calenderUtil = new CalendarUtil();
		ReportLineItemDao c = new ReportLineItemDao();
		List<ReportRequisition> l = c.getEmlipData(calenderUtil.getSqlDate(calenderUtil.changeDateFormat("01/09/2013")), calenderUtil.getSqlDate(calenderUtil.changeDateFormat("30/09/2013")));
		
		for(ReportRequisition r: l)
			System.out.println("SIZE "+l.size());
	}

	/**
	 * @param dateFrm
	 * @param dateTo
	 */
	public List<ReportRequisition> getHivData(Date fromDate, Date toDate) {
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap <String, Date>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {
			
			List<ReportRequisition> list = session.selectList("ReportLineItem.hivLineItem", map);
			return list;
		} finally {
			session.close();
		}
		
	}
	public List<ReportRequisition> getLabData(Date fromDate, Date toDate) {
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap <String, Date>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {
			
			List<ReportRequisition> list = session.selectList("ReportLineItem.labLineItem", map);
			return list;
		} finally {
			session.close();
		}
		
	}
}
