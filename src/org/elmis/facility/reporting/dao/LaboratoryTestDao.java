package org.elmis.facility.reporting.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.LaboratoryTest;

public class LaboratoryTestDao {
	
	private SqlSessionFactory sqlSessionFactory;

	public LaboratoryTestDao()
	{	
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<LaboratoryTest> selectAll()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<LaboratoryTest> list = session.selectList("LaboratoryTest.getAll");
			return list;
		} finally {
			session.close();
		}
	}
}
