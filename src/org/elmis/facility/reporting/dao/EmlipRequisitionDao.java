package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.CalendarUtil;

public class EmlipRequisitionDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public EmlipRequisitionDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getEmlipIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, boolean isEmergency)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		if (isEmergency)
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
		else
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
		List<ReportRequisition> emlipIssuesList = new ArrayList<>();
		try
		{
			List<ReportRequisition> issuedList = session.selectList("EmlipRequisition.emlipIssues", map);
			for (ReportRequisition r : issuedList)
			{
				map.put("productCode", r.getProductCode());
				Integer phyCount = session.selectOne("EmlipRequisition.phyCount", map);
				Integer beginBal = session.selectOne("EmlipRequisition.beginBalance", map);
				Integer adjustments = session.selectOne("EmlipRequisition.adjustments", map);
				if (r.getReportPeriodFrm() == null)
					r.setReportPeriodFrm(fromDate);
				if (r.getReportPeriodTo() == null)
					r.setReportPeriodTo(toDate);
				
				if (phyCount == null){
					System.out.println(phyCount+" Phys Count");
					r.setPhyCount(0);
				}
				else{
					System.out.println(phyCount+" Physical Count");
					r.setPhyCount(phyCount);
				}
				if (beginBal == null){
					System.out.println(beginBal+" Big Bal");
					r.setBeginningBal(0);	
				}
				else{
					System.out.println(beginBal);
					r.setBeginningBal(beginBal);
				}
				if (adjustments == null){
					System.out.println(adjustments+" Adjus");
					r.setLossAdjustment(0);	
				}
				else{
					System.out.println(adjustments);
					r.setLossAdjustment(adjustments);
				}
				session.insert("EmlipRequisition.insertEmlip",  r);
				emlipIssuesList.add(r);
			}
			session.commit();
			return issuedList;
		}
		finally
		{
			session.close();
		}
	}
	private void deleteEmlipRandRData( Date toDate, SqlSession session)
	{
		session.selectList("EmlipRequisition.del_emlip_data", toDate);
		session.commit();
	}
}
