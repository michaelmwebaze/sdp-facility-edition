package org.elmis.facility.reporting.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.LaboratoryEquipment;
import org.elmis.facility.reporting.model.Reagent;

public class LaboratoryEquipmentDao {
	
	private SqlSessionFactory sqlSessionFactory;

	public LaboratoryEquipmentDao()
	{	
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<LaboratoryEquipment> getLabEquipmentUsed()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<LaboratoryEquipment> list = session.selectList("LaboratoryEquipment.getEquip");
				
			return list;
		} finally {
			session.close();
		}
	}
	public List<LaboratoryEquipment> getAllLabEquipment()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<LaboratoryEquipment> list = session.selectList("LaboratoryEquipment.getAllEquip");
			return list;
		} finally {
			session.close();
		}
	}
	
	
	public void updateTestCount(@Param("testCount") int testCount, @Param("equipId")int equipId)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Integer> map = new HashMap<>();
		map.put("testCount", testCount);
		map.put("equipId", equipId);

		try {

			session.selectList("LaboratoryEquipment.updateTestCount", map);
		}
		finally {
			session.close();
		}
	}
	public void updateEquipmentAvailability(@Param("equipId") int equipId, @Param("isAvailable") boolean isAvailable)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("equipId", equipId);
		map.put("isAvailable", isAvailable);
		
		try {

			session.selectList("LaboratoryEquipment.updateEquipStatus", map);
		}
		finally {
			session.close();
		}
	}
	public void updateLabEquipmentStatus(Map<String, String> equipStatusLinkedHashMap)
	{
		SqlSession session = sqlSessionFactory.openSession();
		
		for (Map.Entry<String, String> entry : equipStatusLinkedHashMap.entrySet()) {
			String strValue = entry.getValue();
			LaboratoryEquipment lab = new LaboratoryEquipment();
			lab.setEquipmentCode(entry.getKey());
			/*if (strValue.equals("Y"))
				lab.setEquipmentAvailable(true);
			else
				lab.setEquipmentAvailable(false);*/
			lab.setEquipmentAvailable(entry.getValue().equals("Y"));
			//System.out.println(lab.isAvailable());
			session.update("LaboratoryEquipment.updateLabEquipStatus",  lab);
			session.commit();
			
		}
		
	}
	public boolean getEquipmentStatus(@Param("equipId") int equipId)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("equipId", equipId);
		
		try {

			List<LaboratoryEquipment> list = session.selectList("LaboratoryEquipment.EquipStatus", map);
			return list.get(0).isAvailable();
		}
		finally {
			session.close();
		}
	}
}
