/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reporting.model.ReportRequisitionJason;

/**
 * @author MMwebaze
 *
 */
public class ReportRequisitionJasonDao {
	
	private SqlSessionFactory sqlSessionFactory;
	//SqlSession updateSession;// = sqlSessionFactory.openSession();
	
	public ReportRequisitionJasonDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisitionJason> getCreatedArvData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		
		SqlSession session = sqlSessionFactory.openSession();
		
		try
		{
			return session.selectList("ReportRequisitionJason.submittedArvRandR", map);
		}
		finally
		{
			session.close();
		}
	}
	/*private void changeStatusOfRandR(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programCode)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		if (programCode == 1)// ARV
			updateSession.selectList("ReportRequisitionJason.changeArvStatus", map);
		else if (programCode == 2)// EMLIP
			updateSession.selectList("ReportRequisitionJason.changeEmlipStatus", map);
		else if (programCode == 3)// HIV
			updateSession.selectList("ReportRequisitionJason.changeHivStatus", map);
		else if (programCode == 4)//LAB
			updateSession.selectList("ReportRequisitionJason.changeLabStatus", map);
	}
	public List<ReportRequisitionJason> jasonEmlipData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programCode)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		updateSession = sqlSessionFactory.openSession();

		try {
			changeStatusOfRandR(fromDate, toDate, programCode);
			List<ReportRequisitionJason> list = updateSession.selectList("ReportRequisitionJason.emlip", map);
			return list;
		} finally {
			updateSession.close();
		}
	}
	public List<ReportRequisitionJason> jasonArvData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programCode)
	{
		//SqlSession session = sqlSessionFactory.openSession();
		updateSession = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		try {
			changeStatusOfRandR(fromDate, toDate, programCode);
			List<ReportRequisitionJason> list = updateSession.selectList("ReportRequisitionJason.arv", map);
			
			return list;
		} finally {
			updateSession.close();
		}
	}
	public List<ReportRequisitionJason> jasonHivData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programCode)
	{
		//SqlSession session = sqlSessionFactory.openSession();
		updateSession = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		
		try {
			changeStatusOfRandR(fromDate, toDate, programCode);
			List<ReportRequisitionJason> list = updateSession.selectList("ReportRequisitionJason.hiv", map);
			return list;
		} finally {
			updateSession.close();
		}
	}
	public List<ReportRequisitionJason> jasonLabData(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programCode)
	{
		//SqlSession session = sqlSessionFactory.openSession();
		updateSession = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		
		try {
			changeStatusOfRandR(fromDate, toDate, programCode);
			List<ReportRequisitionJason> list = updateSession.selectList("ReportRequisitionJason.lab");
			return list;
		} finally {
			updateSession.close();
		}
	}
	public boolean isReportSubmitted(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, int programId)
	{
		SqlSession session = sqlSessionFactory.openSession();
		List<ReportRequisition> list = null;
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {
			if (programId == 1)//ARV
			{
				list = session.selectList("ReportRequisition.check_arv_report_submitted", map);

				if (list.size() == 0)
					return false;
				else
					return true;
			}
			else if(programId == 2)//EMLIP
			{
				list = session.selectList("ReportRequisition.check_emlip_report_submitted", map);

				if (list.size() == 0)
					return false;
				else
					return true;
			}
			else if (programId == 3)//HIV
			{
				list = session.selectList("ReportRequisition.check_hiv_report_submitted", map);

				if (list.size() == 0)
					return false;
				else
					return true;
			}
			else //LAB
			{
				list = session.selectList("ReportRequisition.check_lab_report_submitted", map);

				if (list.size() == 0)
					return false;
				else
					return true;
			}
		}
		finally {
			session.close();
		}	
	}
*/}
