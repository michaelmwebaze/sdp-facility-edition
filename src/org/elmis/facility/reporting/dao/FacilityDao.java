package org.elmis.facility.reporting.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Facility;

public class FacilityDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public FacilityDao(){
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Facility> selectAll()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Facility> list = session.selectList("Facility.getAll");
			return list;
		} finally {
			session.close();
		}
	}
}
