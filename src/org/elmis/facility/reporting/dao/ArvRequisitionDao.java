package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.CalendarUtil;

public class ArvRequisitionDao {

	private SqlSessionFactory sqlSessionFactory;
	private AmcDao amcDao = new AmcDao();

	public ArvRequisitionDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getArvDispensaryIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String remarks, boolean isEmergency)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		if (isEmergency)
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
		else
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
		try
		{
			deleteArvRandRData(toDate, session);
			List<ReportRequisition> issuesList = new ArrayList<>();
			List<ReportRequisition> dispensedList = session.selectList("ArvRequisition.dispensed", map);
			for (ReportRequisition r: dispensedList)
			{
				String productCode = r.getProductCode();
				map.put("productCode", productCode);
				System.out.println(r.getQtyDispensed()+" dispensed");
				int y;
				int z = amcDao.amc(productCode);
				if (z == 0)
					y = 1;
				else
					y = 3;
				double amc = (z + r.getQtyDispensed())/y;
				r.setAmc(amc);
				int maxQty = ((int)amc) * 3;
				r.setMaxQty(maxQty);

				Integer qtyReceived = session.selectOne("ArvRequisition.qtyReceived", map);
				Integer phyCount = session.selectOne("ArvRequisition.phyCount", map);
				
				Integer beginBal = session.selectOne("ArvRequisition.beginBalance", map);
				//System.out.println("Begin balance "+beginBal);
				Integer adjustments = session.selectOne("LabRequisition.adjustments", map);
				if (r.getReportPeriodFrm() == null)
					r.setReportPeriodFrm(fromDate);
				if (r.getReportPeriodTo() == null)
					r.setReportPeriodTo(toDate);
				if (qtyReceived == null){
					r.setTotalReceived(0);	
				}
				else{
					r.setTotalReceived(qtyReceived);
				}
				if (phyCount == null){
					//System.out.println(phyCount+" Phys Count");
					r.setPhyCount(0);
					r.setOrderQty(maxQty - 0);
				}
				else{
					//System.out.println(phyCount+" Physical Equip");
					r.setPhyCount(phyCount);
					r.setOrderQty(maxQty - phyCount);
				}
				if (beginBal == null){
					//System.out.println(beginBal+" Big Bal");
					r.setBeginningBal(0);	
				}
				else{
					//System.out.println(beginBal);
					r.setBeginningBal(beginBal);
				}
				if (adjustments == null){
					//System.out.println(adjustments+" Adjus");
					r.setLossAdjustment(0);	
				}
				else{
					//System.out.println(adjustments);
					r.setLossAdjustment(adjustments);
				}
				issuesList.add(r);
				session.insert("ArvRequisition.insertArvrandr",  r);
			}

			session.commit();
			return issuesList;
		}
		finally
		{
			session.close();
		}
	}
	private void deleteArvRandRData( Date toDate, SqlSession session)
	{
		session.selectList("ArvRequisition.delarvdata", toDate);
		session.commit();
	}
}
