package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.LaboratoryTestInformation;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.CalendarUtil;

public class LaboratoryTestInformationDao {

	private SqlSessionFactory sqlSessionFactory;
	//private CalendarUtil calenderUtil = new CalendarUtil();
	
	public LaboratoryTestInformationDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	
	public List<LaboratoryTestInformation> getLabEquipmentInfoReport(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate)
	{
		Map<String, Date> map = new HashMap<>();
		
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<LaboratoryTestInformation> list = session.selectList("LaboratoryTestInformation.getReport", map);
			return list;
		} finally {
			session.close();
		}
	}
	public void saveLabEquipmentInfoData(LaboratoryTestInformation labTestInfo)
	{
		 SqlSession session = sqlSessionFactory.openSession();
		 
	        try {
	            session.insert("LaboratoryTestInformation.insert", labTestInfo);
	            session.commit();
	        } finally {
	            session.close();
	        }
	}
	public boolean checkSubmission(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap <String, Date>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		try {			
			List<LaboratoryTestInformation> list = session.selectList("LaboratoryTestInformation.checkSubmission", map);
			if (list.size() != 0)
				return false;
		} finally {
			session.close();
		}
		return true;
	}
	
	public static void main(String[] arg)
	{
		String frmDate = "01/08/2013";
		String toDate = "31/08/2013";
		CalendarUtil calenderUtil = new CalendarUtil();
		LaboratoryTestInformationDao dao = new LaboratoryTestInformationDao();
		if (dao.checkSubmission(calenderUtil.getSqlDate(calenderUtil.changeDateFormat("01/08/2013")), calenderUtil.getSqlDate(calenderUtil.changeDateFormat("31/08/2013"))))
			JOptionPane.showMessageDialog(null, "<html><h2>Equipment Information Report for the<br> period "+frmDate+" to "+toDate+" <br>has already been submitted</h2></html>");
	}

}
