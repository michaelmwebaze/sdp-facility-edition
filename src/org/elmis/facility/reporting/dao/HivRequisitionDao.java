package org.elmis.facility.reporting.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.CalendarUtil;

public class HivRequisitionDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public HivRequisitionDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getHivDispensaryIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String remarks, boolean isEmergency)
	{
		//SqlSession session = sqlSessionFactory.openSession();
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		if (isEmergency)
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
		else
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
		
		try
		{
			List<ReportRequisition> issuesList = new ArrayList<>();
			List<ReportRequisition> screeningList = session.selectList("HivRequisition.dispensed_screening", map);
			List<ReportRequisition> tiebreakerList = session.selectList("HivRequisition.dispensed_tiebreaker", map);
			List<ReportRequisition> confirmatoryList = session.selectList("HivRequisition.dispensed_confirmatory", map);
			
			for (ReportRequisition r : screeningList)
				issuesList.add(r);
			for (ReportRequisition r : tiebreakerList)
				issuesList.add(r);
			for (ReportRequisition r : confirmatoryList)
				issuesList.add(r);
			
			for (ReportRequisition r : issuesList)
			{
				String productCode = r.getProductCode();
				System.out.println("P code "+productCode);
				System.out.println(toDate);
				System.out.println("Total received "+r.getTotalReceived());
				map.put("productCode", productCode);
				
				Integer physicalCount = session.selectOne("HivRequisition.physicalcount", map);
				Integer beginBalance = session.selectOne("HivRequisition.beginbalance", map);
				
				if (physicalCount == null)
					physicalCount = 0;
				else
					r.setPhyCount(physicalCount);
				
				if (beginBalance == null)
					beginBalance = 0;
				else
					r.setBeginningBal(beginBalance);
			}
			session.commit();
			return issuesList;
		}
		finally
		{
			session.close();
		}
	}
}
