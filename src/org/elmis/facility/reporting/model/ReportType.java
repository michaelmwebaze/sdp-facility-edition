package org.elmis.facility.reporting.model;

public class ReportType {
	
	private int reportTypeId;
	private String reportTypeName;
	
	public int getReportTypeId() {
		return reportTypeId;
	}
	public void setReportTypeId(int reportTypeId) {
		this.reportTypeId = reportTypeId;
	}
	public String getReportTypeName() {
		return reportTypeName;
	}
	public void setReportTypeName(String reportTypeName) {
		this.reportTypeName = reportTypeName;
	}
	@Override
	public String toString() {
		return reportTypeName;
	}
}
