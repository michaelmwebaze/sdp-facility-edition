package org.elmis.facility.reporting.model;

public class LaboratoryEquipment {
	
	private int testId;
	private String testName;
	private boolean isAvailable;
	private int testCount;
	private String equipmentCode;
	private boolean equipmentAvailable;
	
	public boolean isEquipmentAvailable() {
		return equipmentAvailable;
	}
	public void setEquipmentAvailable(boolean equipmentAvailable) {
		this.equipmentAvailable = equipmentAvailable;
	}
	public int getTestCount() {
		return testCount;
	}
	public void setTestCount(int testCount) {
		this.testCount = testCount;
	}
	
	public String getEquipmentCode() {
		return equipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}
	public int getTestId() {
		return testId;
	}
	public void setTestId(int testId) {
		this.testId = testId;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	
	public boolean isAvailable() {
		return isAvailable;
	}
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	@Override
	public String toString() {
		return testName;
	}
}
