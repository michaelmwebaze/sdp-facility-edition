package org.elmis.facility.reporting.model;

public class ReportTracker {
	
	private int id;
	private int lastMonthSubmitted;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLastMonthSubmitted() {
		return lastMonthSubmitted;
	}
	public void setLastMonthSubmitted(int lastMonthSubmitted) {
		this.lastMonthSubmitted = lastMonthSubmitted;
	}
}
