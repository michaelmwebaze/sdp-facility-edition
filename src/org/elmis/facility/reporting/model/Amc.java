/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.model;

/**
 * @author MMwebaze
 *
 */
public class Amc {
	
	private int id;
	private int qtyIssued;
	private String productCode;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQtyIssued() {
		return qtyIssued;
	}
	public void setQtyIssued(int qtyIssued) {
		this.qtyIssued = qtyIssued;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

}
