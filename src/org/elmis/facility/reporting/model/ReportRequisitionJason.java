/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.model;

/**
 * @author MMwebaze
 *
 */
public class ReportRequisitionJason {
	
	@Override
	public String toString() {
		return "ReportRequisitionJason [productName=" + productName + ", unit="
				+ unit + ", beginningBal=" + beginningBal + ", totalReceived="
				+ totalReceived + ", qtyDispensed=" + qtyDispensed
				+ ", lossAdjustment=" + lossAdjustment + ", phyCount="
				+ phyCount + ", productCode=" + productCode + ", amc=" + amc
				+ ", productCategory=" + productCategory + ", typeTest="
				+ typeTest + ", daysStockedOut=" + daysStockedOut
				+ ", programid=" + programid + ", quantityRequested="
				+ quantityRequested + "]";
	}
	private String productName;
	private String unit;
	private int beginningBal;
	private int totalReceived;
	private int qtyDispensed;
	private int lossAdjustment;
	private int phyCount;
	private String productCode;
	private int amc;
	private String productCategory;
	private String typeTest;
	private int daysStockedOut;
	private int programid;
	private int quantityRequested;
	public int getQuantityRequested() {
		return quantityRequested;
	}
	public void setQuantityRequested(int quantityRequested) {
		this.quantityRequested = quantityRequested;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getBeginningBal() {
		return beginningBal;
	}
	public void setBeginningBal(int beginningBal) {
		this.beginningBal = beginningBal;
	}
	public int getTotalReceived() {
		return totalReceived;
	}
	public void setTotalReceived(int totalReceived) {
		this.totalReceived = totalReceived;
	}
	public int getQtyDispensed() {
		return qtyDispensed;
	}
	public void setQtyDispensed(int qtyDispensed) {
		this.qtyDispensed = qtyDispensed;
	}
	public int getLossAdjustment() {
		return lossAdjustment;
	}
	public void setLossAdjustment(int lossAdjustment) {
		this.lossAdjustment = lossAdjustment;
	}
	public int getPhyCount() {
		return phyCount;
	}
	public void setPhyCount(int phyCount) {
		this.phyCount = phyCount;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getAmc() {
		return amc;
	}
	public void setAmc(int amc) {
		this.amc = amc;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public String getTypeTest() {
		return typeTest;
	}
	public void setTypeTest(String typeTest) {
		this.typeTest = typeTest;
	}
	public int getDaysStockedOut() {
		return daysStockedOut;
	}
	public void setDaysStockedOut(int daysStockedOut) {
		this.daysStockedOut = daysStockedOut;
	}
	public int getProgramid() {
		return programid;
	}
	public void setProgramid(int programid) {
		this.programid = programid;
	}

}
