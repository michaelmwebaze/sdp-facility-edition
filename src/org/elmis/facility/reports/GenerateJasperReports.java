/**
 * 
 */
package org.elmis.facility.reports;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JRViewer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;

import org.elmis.facility.tools.JasperViewer;

/**
 * @author JBanda
 *
 */
public class GenerateJasperReports {

	

	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	/**
	 * 
	 */
	public GenerateJasperReports() {
		// TODO Auto-generated constructor stub
	}
	
public void createStockControlCard(Products products ){
		
		SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard	scc = new StockControlCard();
		List sccList = new ArrayList();
		try {

			
			
			sccList = session.selectList("selectSCCByProductCode",products.getCode());
			
			
		} finally {
			session.close();
		}
		
		
		
		try {

			
			
			

			AppJFrame.glassPane.activate(null);
			
			parameterMap.put("packsize", products.getPacksize());
			parameterMap.put("primary_name", products.getPrimaryname());
			parameterMap.put("facility_name", System.getProperty("facility_name"));
			parameterMap.put("facility_code",System.getProperty("facility_code"));
			parameterMap.put("district", (System.getProperty("district")));
			print = JasperFillManager.fillReport("Reports/stockcontrolcard.jasper",
					parameterMap, new JRBeanCollectionDataSource(sccList));

			new JasperViewer(print);
			
					
		JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}
	
		

}
}
