/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reports.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author mmwebaze
 *
 */
public class PropertyLoader {

	private Properties prop = new Properties();
	
	public PropertyLoader()
	{
		try
		{
			prop.load(new FileInputStream("Reports/report.properties"));
			//maxStockLevel = prop.getProperty("");
		}
		catch(IOException e)
		{
			System.out.println(e.getLocalizedMessage());
		}
	}
	public int getStockLevelProp(int programCode)
	{
		if (programCode == 1)//ARV
			return Integer.parseInt(prop.getProperty("arv.stocklevel"));
		else if (programCode == 2)//HIV
			return Integer.parseInt(prop.getProperty("hiv.stocklavel"));
		else if (programCode == 3)//LAB
			return Integer.parseInt(prop.getProperty("lab.stocklevel"));
			else
				return Integer.parseInt(prop.getProperty("emlip.stocklevel"));				
	}
	
	public double getEmergencyOrderPointProp()
	{
		return Double.parseDouble(prop.getProperty("facility.orderpoint"));
	}
	/**
	 * @return
	 */
	public String getFacilityName() {
		
		return prop.getProperty("facility.name");
	}
	public String getProvince()
	{
		return prop.getProperty("province");
	}
	public String getDistrict()
	{
		return prop.getProperty("district");
	}
}
