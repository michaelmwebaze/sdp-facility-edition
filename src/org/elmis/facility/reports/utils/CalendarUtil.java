package org.elmis.facility.reports.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import javax.swing.JOptionPane;

import org.elmis.facility.reporting.dao.ReportTrackerDao;
import org.elmis.facility.reporting.model.ReportTracker;

public class CalendarUtil {

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
	private String deadLineDate = "04";

	public String setDeadLineDate()
	{
		return deadLineDate+"/"+getCurrentMonth()+"/"+getCurrentYear();
	}
	public java.sql.Date  getSqlDate(String date)
	{
		java.sql.Date datum = null;
		Date formattedDate;

		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			formattedDate = formatter.parse(date);
			return datum = new java.sql.Date(formattedDate.getTime());
		} catch (ParseException e) {
			System.out.println("getSQLDate "+e.getMessage());
		}

		return null;
	}
	public void getReportingMonth()
	{
		ReportTrackerDao reportMonthDao = new ReportTrackerDao();
		Iterator<ReportTracker> it = reportMonthDao.selectAll().iterator();


		while(it.hasNext())
			getValue(it.next().getLastMonthSubmitted());
	}
	public String[] getYears()
	{
		Calendar cal = Calendar.getInstance();

		int currentYear = cal.get(Calendar.YEAR);

		int yrs = currentYear - 2;

		String[] yrArray = new String[(currentYear - yrs)];

		for(int i = 0; i< yrArray.length; i++)
			yrArray[i] = (currentYear - i)+"";
		return yrArray;
	}
	public String getFirstDayOfReportingMonth()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		return sdf.format(cal.getTime());
	}
	public String getFirstDayOfCurrentMonth()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		return sdf.format(cal.getTime());
	}
	public String getSubmissionDeadLine()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 0);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		cal.add(Calendar.DATE, 3);
		return sdf.format(cal.getTime());
	}
	public String getCurrentDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date todayDate = new Date();

		return dateFormat.format(todayDate);	
	}

	public int getCurrentMonth()
	{
		Calendar cal = Calendar.getInstance();
		return cal.get(cal.MONTH);
	}

	public int getCurrentYear()
	{
		Calendar cal = Calendar.getInstance();
		return cal.get(cal.YEAR);
	}

	public String getLastDayOfReportingMonth()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));	

		return sdf.format(cal.getTime());
	}
	public String changeDateFormat(String date)
	{
		Date formattedDate;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			formattedDate = sdf.parse(date);
			return formatter.format(formattedDate);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}

		return null;
	}
	public Date fromStringToDate(String date)
	{
		Date formattedDate;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			formattedDate = formatter.parse(date);
			return formattedDate;
		} catch (ParseException e) {
			System.out.println(">>> "+e.getMessage());
		}

		return null;
	}
	public int getValue(String month)
	{
		int i = 0;
		switch(month)
		{
		case "January": i = 0;
		break;
		case "February": i = 1;
		break;
		case "March": i = 2;
		break;
		case "April": i = 3;
		break;
		case "May": i = 4;
		break;
		case "June": i = 5;
		break;
		case "July": i = 6;
		break;
		case "August": i = 7;
		break;
		case "September": i = 8;
		break;
		case "October": i = 9;
		break;
		case "November": i = 10;
		break;
		case "December": i = 11;
		break;
		}
		System.out.println(""+i);
		return i;
	}

	public String getValue(int month)
	{
		String mth = null;

		switch (month) {
		case 0 :
			mth = "January";
			break;
		case 1 :
			mth = "February";
			break;
		case 2 :
			mth = "March";
			break;
		case 3 :
			mth = "April";
			break;
		case 4 :
			mth = "May";
			break;
		case 5 :
			mth = "June";
			break;
		case 6 :
			mth = "July";
			break;
		case 7 :
			mth = "August";
			break;
		case 8 :
			mth = "September";
			break;
		case 9 :
			mth = "October";
			break;
		case 10:
			mth = "November";
			break;
		case 11 :
			mth = "December";
			break;
		default:
			break;
		}
		return mth;
	}
	public boolean compareWithDeadlineDate() throws ParseException
	{
		/*String currentDate = getCurrentDate();
		DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
		Date dateCurrent = sdf.parse(currentDate);
		Date deadLine = sdf.parse(getSubmissionDeadLine());
		if(dateCurrent.compareTo(deadLine) < 0)
		{
			//System.out.println(dateCurrent+" IS THE DEADLINE FOR SUBMISSION");
			return true;
		}

		else if(dateCurrent.compareTo(deadLine) == 0)
		{
			System.out.println(dateCurrent+" IS DEADLINE DAY. SUBMIT REPORTS AND REQUISITIONS AS SOON POSSIBLE ");
			return true;
		}
		else //if(dateCurrent.compareTo(deadLine) > 0)
		{
			if (JOptionPane
					.showConfirmDialog(
							null,
							"<html><h2>PAST DEADLINE. FOR NORMAL R&R WAIT TILL 1 - 4 OF NEXT MONTH. <br><font color=red>ONLY EMERGENCY R & R's CAN BE SUBMITTED </font>. WOULD YOU LIKE TO CREATE THE EMERGENCY R & R?</h2></html>", "WARNING",
									JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			//System.out.println(dateCurrent+" IS PAST DEADLINE DATE OF "+deadLineDate+" FOR SUBMISSION OF NORMAL REPORT. ONLY EMERGENCY R & R's CAN BE SUBMITTED");
			return true;
			}
			else
				return false;
			JOptionPane.showConfirmDialog(null, "PAST DEADLINE. ONLY EMERGENCY R & R CAN BE SUBMITTED", "INFORMATION", JOptionPane.CANCEL_OPTION);
			return false;
		}*/
		return true;

	}
	public String getPreviousMonth(int howManyMonthsBack)
	{
		int x = 0 - howManyMonthsBack; 
		Calendar beginRange = Calendar.getInstance();
		beginRange.add(Calendar.MONTH, x);
		beginRange.set(Calendar.DATE, 1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		beginRange.getActualMinimum(Calendar.DAY_OF_MONTH);
		return formatter.format(beginRange.getTime());
	}
	public String getPreviousLastDayOfMonth(int howManyMonthsBack)
	{
		int x = 0 - howManyMonthsBack; 
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, x-1);
		cal.set(Calendar.DATE, 0);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		return formatter.format(cal.getTime());
	}
	public java.sql.Date getLastDayOfPreviousMonth()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -2);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		return getSqlDate(formatter.format(lastDateOfPreviousMonth.getTime()));
	}
	public java.sql.Date getLastDayOfPreviousMonthEmergency()
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		return getSqlDate(formatter.format(lastDateOfPreviousMonth.getTime()));
	}
	public static void main(String[] arfs) throws ParseException
	{
		CalendarUtil calU = new CalendarUtil();
		int x = 0;
		/*for (int x = 0; x < 2; x++)
		{*/
			System.out.println(x);
			System.out.println(calU.getPreviousLastDayOfMonth(x));
		//}

	}
}
