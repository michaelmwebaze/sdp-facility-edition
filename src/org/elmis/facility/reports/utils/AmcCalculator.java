/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reports.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.elmis.facility.reporting.dao.ReportRequisitionDao;
import org.elmis.facility.reporting.model.ReportRequisition;


/**
 * AmcCalculator.java
 * Purpose: Used to calculate the AMC of a product
 * @author Michael Mwebaze
 * @version 1.0
 */
public class AmcCalculator {
	
	private int maxStockLevel;
	private CalendarUtil calendarUtil;
	private PropertyLoader prop = new PropertyLoader();
	/**
	 * Constructor that initializes dates
	 */
	public AmcCalculator()
	{	
		calendarUtil = new CalendarUtil();
	}
	/**
	 * 
	 * @param programCode program area code e.g 
	 * @param endRange end date of the period in which products were issued
	 * @param productCode product code whose amc is to be calculated
	 * @return a list of product quantities that were issued up to 
	 * endRange 
	 */
	public List<ReportRequisition> calculateAmc(int programCode, String endRange, String productCode)
	{
		maxStockLevel = prop.getStockLevelProp(programCode) - 1;
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);//8
	    //System.out.println("Month is: "+ cal.get(Calendar.MONTH));
	    cal.add(Calendar.MONTH, -maxStockLevel);//6
	    //System.out.println("Month is: "+ cal.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		ReportRequisitionDao dao = new ReportRequisitionDao();
		return dao.getIssues(calendarUtil.getSqlDate(formatter.format(cal.getTime())), calendarUtil.getSqlDate(endRange), productCode);
	}
}
