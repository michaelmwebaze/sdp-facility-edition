/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.facility.dashboard.util;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Date;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.elmis.facility.json.GenerateJason;
import org.elmis.facility.json.beans.ReportBean;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JRViewer;

/**
 *
 * @author MMwebaze
 */
public class JRViewJd extends javax.swing.JDialog {
	private JPanel bottomPanel = new JPanel();
	private JButton pdfExportBtn = new JButton("Export to PDF");
	private JButton closeBtn = new JButton("Close");
	private JButton submitBtn = new JButton("Submit R & R");
	//private JButton xcelExportBtn = new JButton("Export to EXCEL");
	private String reportFolder;
	private String pdfName;
	private Date frmDate;
	private Date toDate;

    /**
     * Creates new form JRViewJd
     */
    public JRViewJd(java.awt.Frame parent, boolean modal, int reportStatus, JRViewer jrViewer, String pdfName, JasperPrint pdfPrint, String reportFolder, Date frmDate, Date toDate) {
        super(parent, modal);
        this.frmDate = frmDate;
        this.toDate = toDate;
        //this.pdfName = pdfName;
        this.reportFolder = reportFolder;
        Toolkit toolkit =  Toolkit.getDefaultToolkit ();
        Dimension dim = toolkit.getScreenSize();
        jrViewer.setPreferredSize(new Dimension(dim.width, dim.height - 100));
        
        //this.add(jrViewer);
        //pack();
        String str = pdfName;
		String sep = "/";
		str = str.replace("\\", sep);
		pdfName = str;
		this.pdfName = pdfName;
        setTitle(str);
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				submitReport(evt);
			}
		});
        closeBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				closeReport(evt);
			}
		});
        pdfExportBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exportReportPDF(evt);
			}
		});
       /* xcelExportBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exportReportXcel(evt);
			}
		});*/
        bottomPanel.add(closeBtn);
        //bottomPanel.add(xcelExportBtn);
        bottomPanel.add(pdfExportBtn);
        if (reportStatus == 1)
        bottomPanel.add(submitBtn);
        add(jrViewer,  BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.PAGE_END);
        pack();
        //initComponents();
        
        setLocationRelativeTo(parent);
    }
    private void closeReport(ActionEvent evt) {
		dispose();
		
	}
	private void submitReport(java.awt.event.ActionEvent evt)
    {
		System.out.println("Starting to submit R and R....");
		ReportBean reportBean = new ReportBean();
		/*reportBean.setFacilityId(1917);
		reportBean.setProgramId(1);
		reportBean.setUserId(68);
		reportBean.setPeriodId(6);
		reportBean.setEmergency(false);
		*/
		reportBean.setAgentCode("904038");
		reportBean.setProgramCode("em");
		reportBean.setApproverName("wbomett");
		new GenerateJason().generateArvJasonString(frmDate, toDate, reportBean);
		System.out.println("Finished submitting R and R....");
    	/*JOptionPane.showMessageDialog(this,
				"<html><h2>UNABLE TO CONNECT TO ELMIS CENTRAL DATABASE <br></h2></html>",	"INFORMATION",JOptionPane.WARNING_MESSAGE);*/
    }
    private void exportReportPDF(java.awt.event.ActionEvent evt)
    {
    	try
    	{
    	JFileChooser fileChooser = new JFileChooser(new File(reportFolder));
    	fileChooser.setDialogTitle(pdfName.replace('/', '\\'));   
    	File f = new File(new File(pdfName.replace('/', '\\')).getCanonicalPath());
    	fileChooser.setSelectedFile(f);
    	int userSelection = fileChooser.showSaveDialog(this);
    	
    	//fileChooser.showOpenDialog(null);
    	if (userSelection == JFileChooser.APPROVE_OPTION) {
    		
    	    File fileToSave = fileChooser.getSelectedFile();
    	   
    	    //System.out.println("Save as file: " + fileToSave.getAbsolutePath());
    	    
    	}
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getLocalizedMessage());
    	}
    	/*System.out.println("Export to pdf...");
    	try {
			JasperExportManager.exportReportToPdfFile(pdfPrint,pdfName);
			org.elmis.facility.reports.utils.RenderPdf.renderPdf(pdfName);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    }
    private void exportReportXcel(java.awt.event.ActionEvent evt)
    {
    	//RenderPdf.renderXcel(xlsxName, sourceFileName);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    //@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  /*  private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }*/// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
