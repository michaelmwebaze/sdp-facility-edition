/*
 * EquipAvailabilityJd.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.dashboard.reporting;

import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;

import org.elmis.facility.reporting.dao.LaboratoryEquipmentDao;
import org.elmis.facility.reporting.model.LaboratoryEquipment;

import com.sun.org.apache.bcel.internal.generic.Select;

/**
 *
 * @author  __USER__
 */
public class EquipAvailabilityJd extends javax.swing.JDialog {

	private LaboratoryEquipmentDao labDao = new LaboratoryEquipmentDao();
	private String[] equipName;
	private List<String> equipmentList = new ArrayList<>();
	private int selectedValue;

	/** Creates new form EquipAvailabilityJd */
	public EquipAvailabilityJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);

		List<LaboratoryEquipment> listEquip = labDao.getAllLabEquipment();

		for (LaboratoryEquipment labEquip : listEquip)
			equipmentList.add(labEquip.getTestName().trim());

		equipName = equipmentList.toArray(new String[equipmentList.size()]);
		initComponents();
		if (labDao.getEquipmentStatus(1))
			yesRadioButton.setSelected(true);
		else
			noRadioButton.setSelected(true);
		buttonGroup1.add(yesRadioButton);
		buttonGroup1.add(noRadioButton);
		setLocationRelativeTo(parent);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		buttonGroup1 = new javax.swing.ButtonGroup();
		yesRadioButton = new javax.swing.JRadioButton();
		noRadioButton = new javax.swing.JRadioButton();
		equipmentComboBox = new javax.swing.JComboBox();
		saveSettingsButton = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Change Equipment Availability");
		setResizable(false);

		yesRadioButton.setText("Yes");

		noRadioButton.setText("No");

		equipmentComboBox.setModel(new javax.swing.DefaultComboBoxModel(
				equipName));
		equipmentComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				equipmentItemStateChanged(evt);
			}
		});

		saveSettingsButton.setText("Save setting");
		saveSettingsButton
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						saveSettingsButtonActionPerformed(evt);
					}
				});

		jLabel1.setText("Select Equipment:");

		jLabel2.setText("Equipment availability:");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(
														jLabel2,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(jLabel1))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(
														saveSettingsButton)
												.addGroup(
														layout.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																false)
																.addComponent(
																		equipmentComboBox,
																		javax.swing.GroupLayout.Alignment.LEADING,
																		0,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE)
																.addGroup(
																		javax.swing.GroupLayout.Alignment.LEADING,
																		layout.createSequentialGroup()
																				.addComponent(
																						yesRadioButton)
																				.addPreferredGap(
																						javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																				.addComponent(
																						noRadioButton))))
								.addGap(227, 227, 227)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(93, 93, 93)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel1)
												.addComponent(
														equipmentComboBox,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(26, 26, 26)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(yesRadioButton)
												.addComponent(noRadioButton))
								.addGap(47, 47, 47)
								.addComponent(saveSettingsButton,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										40,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(47, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	//GEN-END:initComponents

	private void saveSettingsButtonActionPerformed(
			java.awt.event.ActionEvent evt) {		
		selectedValue = equipmentComboBox.getSelectedIndex() + 1;
		System.out.println("Saved "+selectedValue);
		if (yesRadioButton.isSelected())
		{
			labDao.updateEquipmentAvailability(selectedValue, true);
			System.out.println("YES");
		}
		else
		{
			labDao.updateEquipmentAvailability(selectedValue, false);
			System.out.println("NO");
		}
		dispose();
	}

	//GEN-END:initComponents

	private void equipmentItemStateChanged(java.awt.event.ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			if (labDao.getEquipmentStatus(equipmentComboBox.getSelectedIndex() + 1))
				yesRadioButton.setSelected(true);
			else
				noRadioButton.setSelected(true);
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				EquipAvailabilityJd dialog = new EquipAvailabilityJd(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JComboBox equipmentComboBox;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JRadioButton noRadioButton;
	private javax.swing.JButton saveSettingsButton;
	private javax.swing.JRadioButton yesRadioButton;
	// End of variables declaration//GEN-END:variables
}