/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.facility.dashboard.reporting;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.reporting.model.LaboratoryEquipment;

/**
 *
 * @author mmwebaze
 */
public class MachineConfigurationTableModel extends AbstractTableModel{

	private final static String[] header = {"CODE", "MACHINE", "STATUS"};             
	private List<LaboratoryEquipment> labEquipList;

	public MachineConfigurationTableModel(List<LaboratoryEquipment> labEquipList)
	{
		this.labEquipList = labEquipList;
	}
	@Override
	public int getRowCount() {
		return labEquipList.size();
	}

	@Override
	public int getColumnCount() {
		return header.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = null;
		LaboratoryEquipment lab = labEquipList.get (rowIndex);
		if (columnIndex == 0)
			return value = lab.getEquipmentCode().trim();
		else if (columnIndex == 1)   
			return value = lab.getTestName().trim();
		else
		{
			if (lab.isAvailable())
				return "Y";
			else
				return "N";
		}
	}
	@Override
	public String getColumnName(int col)
	{
		return header[col];
	}
	@Override
	public void setValueAt(Object value, int row, int col) {
		String valueStr = value.toString().toUpperCase();
		LaboratoryEquipment lab = labEquipList.get(row);
		if (valueStr.equals("N"))
		lab.setAvailable(false);
		else
			lab.setAvailable(true);
		   
		fireTableCellUpdated(row, col);
	}  
	@Override
	public boolean isCellEditable(int row, int column)
	{
		if (column == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
