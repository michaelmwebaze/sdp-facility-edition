/*
 * ReportAndRequisition.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.dashboard.reporting;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.elmis.facility.connections.DatabaseSwitcher;
import org.elmis.facility.reporting.dao.ProgramDao;
import org.elmis.facility.reporting.dao.ReportRequisitionDao;
import org.elmis.facility.reporting.dao.StorePhysicalCountDao;
import org.elmis.facility.reporting.generator.ReportGenerator;
import org.elmis.facility.reporting.model.Program;
import org.elmis.facility.reporting.model.StorePhysicalCount;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.search.reports.RandRSearchJd;

/**
 * ReportAndRequisition.java
 * Purpose: Used for generation of Reports & Requisitions
 * @author Michael Mwebaze
 * @version 1.0
 */
public class ReportAndRequisition extends javax.swing.JDialog {

	private String frmDate; //reporting period begin date
	private String toDate;  //reporting period end date
	private int programSelector;  //Each program has a predefined number e.g ARV is 0, EMLIP=1, HIV=2, LAB = 3
	private boolean dbAvailable = DatabaseSwitcher.switchToPostgres();
	private StorePhysicalCountDao spcDao = new StorePhysicalCountDao();
	private JProgressBar progressBar = new JProgressBar(0, 100);

	/** Creates new form ReportAndRequisition */
	public ReportAndRequisition(java.awt.Frame parent, boolean modal, String userId) {
		super(parent, modal);
		this.parent = parent;
		frmDate = calendarUtil.getFirstDayOfReportingMonth();
		toDate = calendarUtil.getLastDayOfReportingMonth();
		add(progressBar);
		progressBar.setBounds(260, 540, 280, 20);
		progressBar.setVisible(false);
		
		/*if(dbAvailable)//checks for products whose physical counts have not been carried out and creates a PDF report
		{
			this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			List<StorePhysicalCount> listPhyCount = spcDao.checkForPhysicalCount(calendarUtil.getSqlDate(calendarUtil.changeDateFormat(frmDate)), calendarUtil.getSqlDate(calendarUtil.changeDateFormat(toDate)));

			if (listPhyCount.size() != 0 )
			{
				JOptionPane.showMessageDialog(this,
						"<html>PHYSICAL COUNT FOR SOME PRODUCTS HAS NOT BEEN CARRIED OUT.<br> <font color=red>SEE GENERATED REPORT</font></html>",	"PHYSICAL COUNT",JOptionPane.WARNING_MESSAGE);

				new JasperReporGenerator().generatePhysicalCountReport(listPhyCount, frmDate, toDate);
				//dispose();
			}
			this.setCursor(Cursor.getDefaultCursor());
		}
		else
		{
			JOptionPane.showMessageDialog(this,
					"<html>UNABLE TO GENERATE PHYSICAL COUNT REPORT.<br> <font color=red>CENTRAL DATABASE NO AVAILABLE.</font></html>",	"DATABASE CONNECTION ERROR",JOptionPane.WARNING_MESSAGE);
		}*/
		//else{
		if (dbAvailable)// gets the avail programs from the database which are eventually inserted into the Combobox for selection
		{
			Iterator<Program> programs = programDao.getPrograms().iterator();

			while (programs.hasNext())
				programsList.add(programs.next().getProgramName());
		}
		else
		{
			JOptionPane.showMessageDialog(this,
					"<html>UNABLE POPULATE PROGRAMS.<br> <font color=red>CENTRAL DATABASE NO AVAILABLE.</font></html>",	"DATABASE CONNECTION ERROR",JOptionPane.WARNING_MESSAGE);
		}

		reportTypeArray = programsList.toArray(new String[programsList.size()]);
		initComponents();
		getContentPane().setBackground(new java.awt.Color(102, 102, 102));
		remarkTextArea.setBackground(Color.white);
		remarkTextArea.setLineWrap(true);
		remarkTextArea.setWrapStyleWord(true);
		setLocationRelativeTo(parent);
		fromDateTextField.setText(frmDate);
		toDateTextField.setText(toDate);
		//}
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		fromDateTextField = new javax.swing.JTextField();
		toDateTextField = new javax.swing.JTextField();
		emergencyOrderCheckBox = new javax.swing.JCheckBox();
		jLabel2 = new javax.swing.JLabel();
		jLabel1 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		reportTypeComboBox = new javax.swing.JComboBox();
		cancelButton = new javax.swing.JButton();
		viewReportButton = new javax.swing.JButton();
		submitReportButton = new javax.swing.JButton();
		jLabel4 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		remarkTextArea = new javax.swing.JTextArea();
		jLabel5 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("View & save Requests and Requisitions");

		fromDateTextField.setEditable(false);

		toDateTextField.setEditable(false);

		//emergencyOrderCheckBox.setFont(new java.awt.Font("Ebrima", 1, 12));
		//emergencyOrderCheckBox.setForeground(new java.awt.Color(255, 0, 0));
		emergencyOrderCheckBox.setText("<html><b><h3>Emergency order</h3><b></html>");
		emergencyOrderCheckBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				emergencyOrderCheckBoxItemStateChanged(evt);
			}
		});

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setText("To:");

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel1.setText("Reporting Period from:");

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setText("Select report type:");

		reportTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(reportTypeArray));
		reportTypeComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				reportTypeComboBoxItemStateChanged(evt);
			}
		});

		cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Cancel.png"))); // NOI18N
		cancelButton.setText("Cancel");
		cancelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelButtonActionPerformed(evt);
			}
		});

		viewReportButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		viewReportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/View report.png"))); // NOI18N
		viewReportButton.setText("View report");
		viewReportButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					viewReportButtonActionPerformed(evt);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		submitReportButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		submitReportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Save icon.png"))); // NOI18N
		submitReportButton.setText("Search submitted R and Rs");
		submitReportButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				submitReportButtonActionPerformed(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setText("Remarks:");

		remarkTextArea.setColumns(20);
		remarkTextArea.setRows(5);
		jScrollPane1.setViewportView(remarkTextArea);

		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/reports requisition.png"))); // NOI18N

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
												.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
														.addComponent(jLabel1)
														.addGap(18, 18, 18)
														.addComponent(fromDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addGap(18, 18, 18)
														.addComponent(jLabel2)
														.addGap(11, 11, 11))
														.addComponent(reportTypeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(jScrollPane1))
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
														.addComponent(toDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addGap(31, 31, 31)
														.addComponent(emergencyOrderCheckBox))
														.addGroup(layout.createSequentialGroup()
																.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(jLabel4)
																		.addComponent(jLabel3))
																		.addGap(0, 529, Short.MAX_VALUE))
																		.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
																				.addGap(0, 245, Short.MAX_VALUE)
																				.addComponent(cancelButton)
																				.addGap(18, 18, 18)
																				.addComponent(viewReportButton)
																				.addGap(18, 18, 18)
																				.addComponent(submitReportButton)))
																				.addContainerGap())
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGap(36, 36, 36)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(jLabel1)
								.addComponent(jLabel2)
								.addComponent(toDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(fromDateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addComponent(emergencyOrderCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(61, 61, 61)
								.addComponent(jLabel3)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(reportTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(33, 33, 33)
								.addComponent(jLabel4)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(submitReportButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(viewReportButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(68, 68, 68))
										.addGroup(layout.createSequentialGroup()
												.addGap(28, 28, 28)
												.addComponent(jLabel5)
												.addContainerGap(31, Short.MAX_VALUE))
				);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents
	/**
	 * Handles R & R submission events
	 * @param evt
	 */
	private void submitReportButtonActionPerformed(
			java.awt.event.ActionEvent evt) {
		new RandRSearchJd(null, true).setVisible(true);

		/*if (programSelector == 0)// ARV SUBMIT DATA
		{
			int programCode = 1;
			reportBean.setProgramId(programCode);

			jasonGen.generateArvJasonString(calendarUtil
					.getSqlDate(calendarUtil.changeDateFormat(frmDate)),
					calendarUtil.getSqlDate(calendarUtil
							.changeDateFormat(toDate)), reportBean);
		} else if (programSelector == 3)// LAB SUBMIT DATA
		{
			int programCode = 4;

			reportBean.setProgramId(programCode);

			jasonGen.generateLabJasonString(calendarUtil
					.getSqlDate(calendarUtil.changeDateFormat(frmDate)),
					calendarUtil.getSqlDate(calendarUtil
							.changeDateFormat(toDate)), reportBean);
		} else if (programSelector == 2)//HIV SUBMIT DATA
		{
			int programCode = 3;
			reportBean.setProgramId(programCode);

			jasonGen.generateHivJasonString(calendarUtil
					.getSqlDate(calendarUtil.changeDateFormat(frmDate)),
					calendarUtil.getSqlDate(calendarUtil
							.changeDateFormat(toDate)), reportBean);
			System.out.println("HIV");
		} else if (programSelector == 1)// EMLIP SUBMIT DATA
		{
			int programCode = 2;
			reportBean.setProgramId(programCode);

			jasonGen.generateEmlipJasonString(calendarUtil
					.getSqlDate(calendarUtil.changeDateFormat(frmDate)),
					calendarUtil.getSqlDate(calendarUtil
							.changeDateFormat(toDate)), reportBean);
		}*/
		//viewReportButton.setEnabled(true);
	}
	/**
	 * Handles the Event that is created as a result of selecting the close button
	 * @param evt
	 */
	private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
		dispose();
	}
	/**
	 * Handles events that a generated as a result of switching from one Program Area to another.
	 * Then sets the program number which determines which Program Area R & R is generated or 
	 * submitted.  
	 * @param evt
	 */
	private void reportTypeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {

		int index = reportTypeComboBox.getSelectedIndex();
		programSelector = index;
	}
	/**
	 * Handles event that is generated as a result of clicking the View Button. Once the View
	 * Button is clicked, an R & R report in PDF is created for the User to preview before
	 * they can submit it to the Central eLMIS. 
	 * @param evt
	 * @throws ParseException
	 */
	private void viewReportButtonActionPerformed(java.awt.event.ActionEvent evt)
			throws ParseException {
		//if (isNonEmergency && )
		//JasperReporGenerator jasperGen = new JasperReporGenerator();
		if (isNonEmergency)//controls submission on non-emergency reports
		{
			if (calendarUtil.compareWithDeadlineDate())//checks if deadline date has been exceeded 
			{
				final String normalOrder = "NO";

				int selected = reportTypeComboBox.getSelectedIndex(); //stores index when an item is selected
				if (selected == 0)//ARV SECTION
				{

					class ReportWorker extends SwingWorker<String, Object> {
						protected String doInBackground() {
							progressBar.setVisible(true);
							progressBar.setIndeterminate(true);
							CalendarUtil cal = new CalendarUtil();
							List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkArvProductPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())));

							if (spcList.isEmpty())
							{
								new ReportGenerator().generateArvDrugsReport(fromDateTextField.getText(),
										toDateTextField.getText(), normalOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, false);
							}
							else
							{
								int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
								if (ack == JOptionPane.YES_OPTION) {
									new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
											toDateTextField.getText());
								}
							}
							return "Done.";
						}

						protected void done() {
							progressBar.setVisible(false);
						}
					}

					new ReportWorker().execute();
				}
				else if (selected == 1)//EMLIP SECTION
				{
					if (checkLineItemDao.isReportCreated(
							calendarUtil.getSqlDate(calendarUtil
									.changeDateFormat(frmDate)), calendarUtil
									.getSqlDate(calendarUtil
											.changeDateFormat(toDate)), 1))
					{
						class ReportWorker extends SwingWorker<String, Object> {
							protected String doInBackground() {
								progressBar.setVisible(true);
								progressBar.setIndeterminate(true);
								CalendarUtil cal = new CalendarUtil();
								List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM");

								if (spcList.isEmpty())
								{
									new ReportGenerator().generateEmlipReport(fromDateTextField.getText(),
								toDateTextField.getText(), normalOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, false);
								}
								else
								{
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
												toDateTextField.getText());
									}
								}
								return "Done.";
							}

							protected void done() {
								progressBar.setVisible(false);
							}
						}

						new ReportWorker().execute();
					}
					else 
					{
						JOptionPane.showMessageDialog(this,
								"<html><h2>The EMLIP Report & Requisition for the<br> period<font color=red> "
										+ frmDate
										+ " to "
										+ toDate
										+ " </font> has already been created. <br></h2></html>",	"INFORMATION",JOptionPane.WARNING_MESSAGE);
					}
				} 
				else if (selected == 2)//HIV TESTS SECTION
				{
					if (checkLineItemDao.isReportCreated(
							calendarUtil.getSqlDate(calendarUtil
									.changeDateFormat(frmDate)), calendarUtil
									.getSqlDate(calendarUtil
											.changeDateFormat(toDate)), 3))
					{
						class ReportWorker extends SwingWorker<String, Object> {
							protected String doInBackground() {
								progressBar.setVisible(true);
								progressBar.setIndeterminate(true);
								CalendarUtil cal = new CalendarUtil();
								
								List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "HIV");

								if (spcList.isEmpty())
								{
									new ReportGenerator().generateHivReport(fromDateTextField.getText(),
										toDateTextField.getText(), normalOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, false);
								}
								else
								{
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
												toDateTextField.getText());
									}
								}
								return "Done.";
							}

							protected void done() {
								progressBar.setVisible(false);
							}
						}

						new ReportWorker().execute();
					}
					else {
						JOptionPane.showMessageDialog(this,
								"<html><h2>The HIV Report & Requisition for the<br> period<font color=red> "
										+ frmDate
										+ " to "
										+ toDate
										+ " </font> has already been created. <br> </h2></html>",	"INFORMATION",JOptionPane.WARNING_MESSAGE);
					}
				} 
				else if (selected == 3)//LAB USAGE SECTION
				{
					if (checkLineItemDao.isReportCreated(
							calendarUtil.getSqlDate(calendarUtil
									.changeDateFormat(frmDate)), calendarUtil
									.getSqlDate(calendarUtil
											.changeDateFormat(toDate)), 4))
					{
						class ReportWorker extends SwingWorker<String, Object> {
							protected String doInBackground() {
								progressBar.setVisible(true);
								progressBar.setIndeterminate(true);
								CalendarUtil cal = new CalendarUtil();
								List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB");

								if (spcList.isEmpty())
								{
									new ReportGenerator().generateLabUsageReport(fromDateTextField.getText(),
										toDateTextField.getText(), normalOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, false);
								}
								else
								{
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
												toDateTextField.getText());
									}
								}
								return "Done.";
							}

							protected void done() {
								progressBar.setVisible(false);
							}
						}

						new ReportWorker().execute();
					}
					else 
					{
						JOptionPane.showMessageDialog(this,
								"<html><h2>The LAB Report & Requisition for the<br> period<font color=red> "
										+ frmDate
										+ " to "
										+ toDate
										+ " </font> has already been created. <br></h2></html>",	"INFORMATION",JOptionPane.WARNING_MESSAGE);
					}
				} 
				else // TB SECTION
				{

				}
				remarkTextArea.setText("");
			}
			//viewReportButton.setEnabled(false);
		} 
		else //controls submission of emergency reports
		{
			final String emergencyOrder = "YES";

			int selected = reportTypeComboBox.getSelectedIndex(); //stores index when an item is selected
			if (selected == 0)//ARV SECTION
			{
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
						List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkArvProductPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())));

						if (spcList.isEmpty())
						{
							new ReportGenerator().generateArvDrugsReport(fromDateTextField.getText(),
									toDateTextField.getText(), emergencyOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, true);
						}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
										toDateTextField.getText());
							}
						}
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();

			} else if (selected == 1)//EMLIP SECTION
			{
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
						List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM");

						if (spcList.isEmpty())
						{
							new ReportGenerator().generateEmlipReport(fromDateTextField.getText(),
						toDateTextField.getText(), emergencyOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, true);
						}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
										toDateTextField.getText());
							}
						}
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();
			} else if (selected == 2)//HIV TESTS SECTION
			{
				this.setCursor(Cursor
						.getPredefinedCursor(Cursor.WAIT_CURSOR));

				this.setCursor(Cursor.getDefaultCursor());
			} 
			else if (selected == 3)//LAB USAGE SECTION
			{
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
						List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB");

						if (spcList.isEmpty())
						{
							new ReportGenerator().generateLabUsageReport(fromDateTextField.getText(),
								toDateTextField.getText(), emergencyOrder, remarkTextArea.getText().toUpperCase(), ReportAndRequisition.this, true);
						}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
										toDateTextField.getText());
							}
						}
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();
			} 
			else // TB SECTION
			{

			}
			remarkTextArea.setText("");
		}
	}
	/**
	 * Handles event generated as a result of checking the Emergency Order
	 * Check Box. Once checked, the report submitted will be an Emergency
	 * Order R & R
	 * @param evt
	 */
	private void emergencyOrderCheckBoxItemStateChanged(
			java.awt.event.ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			fromDateTextField.setText("");
			fromDateTextField.setText(calendarUtil.getFirstDayOfCurrentMonth());
			toDateTextField.setText("");
			toDateTextField.setText(calendarUtil.getCurrentDate());
			isNonEmergency = false;
			emergencyOrderCheckBox.setText("<html><b><h2><font color=red>Emergency order</font></h2><b></html>");
		} else {
			fromDateTextField.setText("");
			fromDateTextField.setText(calendarUtil
					.getFirstDayOfReportingMonth());
			toDateTextField.setText(calendarUtil.getLastDayOfReportingMonth());
			isNonEmergency = true;
			emergencyOrderCheckBox.setText("<html><b><h2>Emergency order</h2><b></html>");
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ReportAndRequisition dialog = new ReportAndRequisition(new javax.swing.JFrame(), true, "msolomon");
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton cancelButton;
	private javax.swing.JCheckBox emergencyOrderCheckBox;
	private javax.swing.JTextField fromDateTextField;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextArea remarkTextArea;
	private javax.swing.JComboBox reportTypeComboBox;
	private javax.swing.JButton submitReportButton;
	private javax.swing.JTextField toDateTextField;
	private javax.swing.JButton viewReportButton;
	// End of variables declaration//GEN-END:variables
	private boolean isNonEmergency = true; //this variable determines whether a non-emergency report or emergency report will be submitted
	private ProgramDao programDao = new ProgramDao();
	private List<String> programsList = new ArrayList<>();
	private String[] reportTypeArray;
	private Frame parent;
	private CalendarUtil calendarUtil = new CalendarUtil();
	private ReportRequisitionDao checkLineItemDao = new ReportRequisitionDao();
}