/*
 * EquipmentInformationJd.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.dashboard.reporting;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.reporting.dao.LaboratoryEquipmentDao;
import org.elmis.facility.reporting.dao.LaboratoryTestInformationDao;
import org.elmis.facility.reporting.generator.ReportGenerator;
import org.elmis.facility.reporting.model.LaboratoryEquipment;
import org.elmis.facility.reporting.model.LaboratoryTestInformation;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.reports.utils.TableColumnAligner;

/**
 *
 * @author  __USER__
 */
public class EquipmentInformationJd extends javax.swing.JDialog {

	/** Creates new form EquipmentInformationJd */
	/**
	 * @param parent
	 * @param modal
	 */
	public EquipmentInformationJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		currentYear = calendarUtil.getCurrentYear();
		frmDate = calendarUtil.getFirstDayOfReportingMonth();
		toDate = calendarUtil.getLastDayOfReportingMonth();

		Iterator<LaboratoryEquipment> equipIt = labDao.getLabEquipmentUsed()
				.iterator();

		while (equipIt.hasNext()) {
			LaboratoryEquipment labEquip = equipIt.next();
			
			equipMentTestCount.put(labEquip.getTestId(),
					labEquip.getTestCount());
			
			for (Map.Entry<Integer, Integer> entry : equipMentTestCount.entrySet()) {
				
				equipmentTestcountValues.add(entry.getValue());
				
			}
			testNameList.add(labEquip.toString());
		}
		initComponents();
		                  
		fromTextField.setText(frmDate);
		toTextField.setText(toDate);

		for (int i = 0; i < testNameList.size(); i++) {
			tableModel.addRow(new String[testNameList.size()]);

			jTable1.getModel().setValueAt(testNameList.get(i), i, 0);
		}

		comboLoader();
		
		colAligner1.centreAlignColumn(jTable1, 1);
		colAligner2.centreAlignColumn(jTable1, 2);
		
		jTable1.getModel().addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent e) {
				if (jTable1.getCellEditor() == null) {

				} else {

					if (jTable1.getSelectedColumn() == 1) {
						String str = ""
								+ jTable1.getValueAt(jTable1.getSelectedRow(),
										jTable1.getSelectedColumn());
						if (str.equalsIgnoreCase("YES"))
							statusLists.add("Y");
						else
							statusLists.add("N");

					} else if (jTable1.getSelectedColumn() == 2) {
						try {
							Integer i = Integer.parseInt(jTable1.getValueAt(
									jTable1.getSelectedRow(),
									jTable1.getSelectedColumn()).toString());
							Integer idEquipment = jTable1.getSelectedRow() + 1;
							
							idsHashset.add(idEquipment);
							countList.add(i);
						} catch (NumberFormatException nfe) {
							JOptionPane.showConfirmDialog(null,
									"Please enter numbers only", "INFORMATION",
									JOptionPane.CANCEL_OPTION);
							/*jTable1.getModel().setValueAt("", jTable1.getSelectedRow(),
									jTable1.getSelectedColumn());*/
						}
					}
				}

			}
		});

		jTable1.setFillsViewportHeight(true);
		jTable1.getColumnModel().getColumn(1).setPreferredWidth(10);
		jTable1.getTableHeader().setReorderingAllowed(false);
		jTable1.getTableHeader().setPreferredSize(new Dimension(30, 30));
		jTable1.setRowHeight(35);

		setLocationRelativeTo(parent);
	}

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        viewButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        fromLabel = new javax.swing.JLabel();
        toLabel = new javax.swing.JLabel();
        fromTextField = new javax.swing.JTextField();
        toTextField = new javax.swing.JTextField();
        emergencyOrderCheckBox = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("MONTHLY TEST NUMBERS");
        setResizable(false);

        jLabel1.setText("<html><b><h3>MONTHLY TEST NUMBERS AND EQUIPMENT INFORMATION</h3></b></html>");

        jTable1.setModel(tableModel);
        jScrollPane1.setViewportView(jTable1);

        viewButton.setText("CREATE REPORT");
        viewButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewButtonActionPerformed(evt);
            }
        });

        cancelButton.setText("CANCEL");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        fromLabel.setText("From:");

        toLabel.setText("to:");

        fromTextField.setEditable(false);

        toTextField.setEditable(false);

        emergencyOrderCheckBox.setText("Emergency Order");

        jLabel2.setText("Image here");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(586, 586, 586)
                        .addComponent(emergencyOrderCheckBox))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(fromLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fromTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(toLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(toTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(34, 34, 34)
                                    .addComponent(viewButton))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 687, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(fromLabel)
                            .addComponent(toLabel)
                            .addComponent(fromTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(toTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emergencyOrderCheckBox))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(viewButton, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(38, 38, 38))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void viewButtonActionPerformed(java.awt.event.ActionEvent evt) {
		if (checkTestInfoSubmission
				.checkSubmission(calenderUtil.getSqlDate(calenderUtil
						.changeDateFormat(frmDate)), calenderUtil
						.getSqlDate(calenderUtil.changeDateFormat(toDate)))) {
			if (JOptionPane
					.showConfirmDialog(
							this,
							"<html><h2>Are you sure you want to submit Equipment Information Report for the<br> period<font color=red> "
									+ frmDate
									+ " to "
									+ toDate
									+ " </font>?</h2></html>", "WARNING",
							JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				Iterator<Integer> equipIdIterator = idsHashset.iterator();

				while (equipIdIterator.hasNext())
					testIds.add(equipIdIterator.next());


				if (idsHashset.size() == statusLists.size()

				&& statusLists.size() == countList.size()
						&& countList.size() == idsHashset.size()
						&& statusLists.size() != 0) {

					for (int i = 0; i < idsHashset.size(); i++) {
						int testCount = countList.get(i) - equipmentTestcountValues.get(i);
						int equipId = testIds.get(i);
						LaboratoryTestInformation labTest = new LaboratoryTestInformation();
						labTest.setTestId(equipId);
						labTest.setMachineTestCount(countList.get(i));
						labTest.setMonthTotal(testCount);
						labTest.setEquipFunction(statusLists.get(i));
						labTest.setBeginDate(calendarUtil
								.getSqlDate(calendarUtil
										.changeDateFormat(frmDate)));
						labTest.setEndDate(calendarUtil.getSqlDate(calendarUtil
								.changeDateFormat(toDate)));
						new LaboratoryTestInformationDao()
								.saveLabEquipmentInfoData(labTest);
						labDao.updateTestCount(countList.get(i), equipId);
						viewButton.setEnabled(false);
					}
					JOptionPane.showMessageDialog(this, "DATA HAS BEEN SAVED", "",JOptionPane.INFORMATION_MESSAGE);
					reportGen.generateLabEquipInfoReport(frmDate, toDate, this);
					statusLists.clear();
					testIds.clear();
					countList.clear();
					idsHashset.clear();
					
					dispose();

				} /*else {
					JOptionPane
							.showMessageDialog(
									this,
									"<html>ERROR IN INPUT CONFIRM MACHINE TEST COUNT BY <br>PRESSING <font color=red>ENTER</font></html>",
									"", JOptionPane.ERROR_MESSAGE);
					}*/
			} else {

			}
		} else
			JOptionPane
					.showMessageDialog(
							this,
							"<html><h2>Equipment Information Report for the<br> period<font color=red> "
									+ frmDate
									+ " to "
									+ toDate
									+ " </font><br>has already been submitted</h2></html>");
	}

	private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
		dispose();
	}

	//Function responsible for setting Y or N in the drop down with the form
	private void comboLoader() {
		try {
			TableColumn statusColumn = jTable1.getColumnModel().getColumn(1);
			statusColumn.setPreferredWidth(10);
			JComboBox<String> comboBox = new JComboBox();
			comboBox.setPreferredSize(new Dimension(30, 30));
			comboBox.removeAllItems();

			try {
				comboBox.addItem("YES");
				comboBox.addItem("NO");
				//comboBox.addItem("P");

			} catch (NullPointerException e) {
			} catch (Exception e) {
				e.printStackTrace();
			}
			statusColumn.setCellEditor(new DefaultCellEditor(comboBox));
		} catch (Exception e) {
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				EquipmentInformationJd dialog = new EquipmentInformationJd(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelButton;
    private javax.swing.JCheckBox emergencyOrderCheckBox;
    private javax.swing.JLabel fromLabel;
    private javax.swing.JTextField fromTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel toLabel;
    private javax.swing.JTextField toTextField;
    private javax.swing.JButton viewButton;
    // End of variables declaration//GEN-END:variables
	private String[][] data = null;
	private String[] columnNames = { "<html><b>TEST</b></html>",
			"<html><b>EQUIPMENT FUNCTIONING</b></html>",
			"<html><b>MACHINE TEST COUNT</b></html>" };
	private DefaultTableModel tableModel = new DefaultTableModel(data,
			columnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
			return column > 0;
		}
	};
	private LaboratoryEquipmentDao labDao = new LaboratoryEquipmentDao();
	private List<String> testNameList = new ArrayList<>();
	//private JasperReporGenerator jsReportGen = new JasperReporGenerator();
	private ArrayList<Integer> testIds = new ArrayList<>();
	private Set<Integer> idsHashset = new HashSet<>();
	private ArrayList<String> statusLists = new ArrayList<>();
	private ArrayList<Integer> countList = new ArrayList<>();
	private CalendarUtil calendarUtil = new CalendarUtil();
	private int currentYear;
	private String frmDate;
	private String toDate;
	private ReportGenerator reportGen = new ReportGenerator();
	private Map<Integer, Integer> equipMentTestCount = new LinkedHashMap<>();
	private LaboratoryTestInformationDao checkTestInfoSubmission = new LaboratoryTestInformationDao();
	private CalendarUtil calenderUtil = new CalendarUtil();
	private List<Integer> equipmentTestcountValues = new ArrayList<>();
	private TableColumnAligner colAligner1 = new TableColumnAligner();
	private TableColumnAligner colAligner2 = new TableColumnAligner();
}