package org.elmis.facility.dashboard.reporting;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.elmis.facility.reporting.dao.AmcDao;
import org.elmis.facility.reporting.dao.LaboratoryTestInformationDao;
import org.elmis.facility.reporting.dao.ReportLineItemDao;
import org.elmis.facility.reporting.dao.ReportRequisitionDao;
import org.elmis.facility.reporting.dao.ReportingPeriodRemarkDao;
import org.elmis.facility.reporting.dao.StorePhysicalCountDao;
import org.elmis.facility.reporting.model.LaboratoryTestInformation;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reporting.model.ReportingPeriodRemark;
import org.elmis.facility.reporting.model.StorePhysicalCount;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.reports.utils.PropertyLoader;
import org.elmis.facility.reports.utils.RenderPdf;
import org.elmis.facility.reports.utils.ReportStorageManager;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
/**
 * 
 * @author Michael Mwebaze Kitobe
 * This class is used to generate the different program reports with-in jasper and also generate the PDF
 *
 */
public class JasperReporGenerator {
	private String facilityName;
	private Map<String, Object> parameters = new HashMap<>();
	private CalendarUtil calenderUtil = new CalendarUtil();
	private PropertyLoader prop = new PropertyLoader();
	private AmcDao amcDao = new AmcDao();
	private ReportStorageManager storageManager = new ReportStorageManager();
	private ReportLineItemDao reportLineItemDao = new ReportLineItemDao();
	private StorePhysicalCountDao spcDao = new StorePhysicalCountDao();
	private Date prevDate;
	private ReportingPeriodRemarkDao reportRemarkDao = new ReportingPeriodRemarkDao();

	public JasperReporGenerator()
	{
		prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
		if (!storageManager.isFolderAvailable())
			storageManager.createReportFolder();

		//FacilityDao daoFacility = new FacilityDao();

		facilityName = prop.getFacilityName();
		parameters.put("facility",facilityName);
	}
	//Used to generate ARV DRUGS REPORT
	public void generateArvDrugsReport(String frmDate, String toDate, String normalOrder, String remarks)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportRequisitionDao reportDao = new ReportRequisitionDao();
		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(1));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		JasperDesign arvDesign = null;
		JasperReport arvRpt = null;
		JasperPrint arvPrnt = null;

		List<ReportRequisition> dispenaryIssues = reportDao.getArvDispensaryIssues(dateFrm, dateTo);

		List<ReportRequisition> storeIssues = reportDao.getArvStoreIssues(dateFrm, dateTo);

		for (ReportRequisition r : dispenaryIssues)
		{
			for (ReportRequisition q : storeIssues)
			{	
				if(r.getProductCode().equals(q.getProductCode())){
					prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
					int beginBal = spcDao.getpreviousPhysicalCount(prevDate, r.getProductCode());
					if (beginBal == 0)
						r.setBeginningBal(q.getTotalReceived());
					else
						r.setBeginningBal(beginBal);
					r.setTotalReceived(q.getTotalReceived());
					r.setLossAdjustment(q.getLossAdjustment());
					r.setPhyCount(q.getPhyCount());
				}
			}

			r.setReportPeriodFrm(dateFrm);
			r.setReportPeriodTo(dateTo);

			reportDao.insertArv(r);

			double amc = amcDao.calculateArvAmc(r.getProductCode());
			int maxQty = (int)amc * 3;
			int phyCount = r.getPhyCount();
			int orderQty = phyCount - maxQty;

			r.setAmc(amc);
			r.setMaxQty(maxQty);
			r.setOrderQty(orderQty);

			reportDao.updateArvRequisitionParams((int)amc, maxQty, orderQty, dateFrm, dateTo, r.getProductCode());
		}

		reportRemarkDao.insertRemark(reportRemark);
		JRBeanCollectionDataSource arvDataSrc = new JRBeanCollectionDataSource(dispenaryIssues);

		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R ARV.pdf";

		try
		{
			arvDesign = JRXmlLoader.load("./Reports/ARV_MASTER_REPORT.jrxml");
			arvRpt = JasperCompileManager.compileReport(arvDesign);
			arvPrnt  = JasperFillManager.fillReport(arvRpt, parameters, arvDataSrc);
			JasperExportManager.exportReportToPdfFile(arvPrnt,pdfName);
			new RenderPdf().renderPdf(pdfName);	
		}
		catch(JRException e)
		{
			JOptionPane.showMessageDialog(null,
					"<html><h2>ERROR GENERATING ARV Report & Requisition for the period<br> <font color=red> "
									+ frmDate
									+ " to "
									+ toDate
									+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
			System.out.println("ARV DATA PROCESSING ERROR:"+e.getMessage());
		}
	}

	//USED TO GENERATE HIV TESTS REPORT
	public void generateHivTestsReport(String frmDate, String toDate, String normalOrder, String remarks)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportRequisitionDao dao = new ReportRequisitionDao();
		List<ReportRequisition> hivData = dao.getHivData(dateFrm, dateTo);

		for (ReportRequisition r: hivData)
		{
			prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
			int beginBal = spcDao.getpreviousPhysicalCount(prevDate, r.getProductCode());
			if (beginBal == 0)
				r.setBeginningBal(r.getTotalReceived());
			else
				r.setBeginningBal(beginBal);
			r.setReportPeriodFrm(dateFrm);
			r.setReportPeriodTo(dateTo);
			dao.insertHiv(r);
		}

		List<ReportRequisition> hivIssues = reportLineItemDao.getHivData(dateFrm, dateTo);

		for (ReportRequisition r: hivIssues)
		{
			double amc = amcDao.calculateEmlipAmc(r.getProductCode().trim());
			int maxQty = (int)amc * 3;
			int phyCount = r.getPhyCount();
			int orderQty = maxQty - phyCount;
			r.setAmc(amc);
			r.setOrderQty(orderQty);
			r.setMaxQty(maxQty);
			dao.updateHivRequisitionParams((int)amc, maxQty, orderQty, dateFrm, dateTo, r.getProductCode());
		}
		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(2));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		JasperDesign hivDesign = null;
		JasperReport hivRpt = null;
		JasperPrint hivPrnt = null;

		JRBeanCollectionDataSource hivDataSrc = new JRBeanCollectionDataSource(hivIssues);
		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R HIV TESTS.pdf";
		try
		{
			hivDesign = JRXmlLoader.load("./Reports/HIV_MASTER_REPORT.jrxml");
			hivRpt = JasperCompileManager.compileReport(hivDesign);
			hivPrnt  = JasperFillManager.fillReport(hivRpt, parameters, hivDataSrc);
			JasperExportManager.exportReportToPdfFile(hivPrnt,pdfName);
			new RenderPdf().renderPdf(pdfName);	
		}
		catch(JRException e)
		{
			JOptionPane.showMessageDialog(null,
					"<html><h2>ERROR GENERATING HIV Report & Requisition for the period<br> <font color=red> "
									+ frmDate
									+ " to "
									+ toDate
									+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
			System.out.println("HIV REPORT PROCESSING ERROR:"+e.getMessage());
		}
	}
	//USED TO GENERATE EMLIP REPORT
	public void generateEmlipReport(String frmDate, String toDate, String normalOrder, String remarks)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportRequisitionDao dao = new ReportRequisitionDao();
		List<ReportRequisition> emlipList = dao.getEmlipData(dateFrm, dateTo);

		for (ReportRequisition r: emlipList)
		{
			prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
			int beginBal = spcDao.getpreviousPhysicalCount(prevDate, r.getProductCode());
			if (beginBal == 0)
				r.setBeginningBal(r.getTotalReceived());
			else
				r.setBeginningBal(beginBal);
			r.setReportPeriodFrm(dateFrm);
			r.setReportPeriodTo(dateTo);
			dao.insertEmplip(r);
		}
		List<ReportRequisition> emlipIssues = reportLineItemDao.getEmlipData(dateFrm, dateTo);

		for (ReportRequisition r: emlipIssues)
		{
			double amc = amcDao.calculateEmlipAmc(r.getProductCode().trim());
			int maxQty = (int)amc * 3;
			int phyCount = r.getPhyCount();
			int orderQty = maxQty - phyCount;
			r.setAmc(amc);
			r.setOrderQty(orderQty);
			r.setMaxQty(maxQty);
			dao.updateEmlipRequisitionParams((int)amc, maxQty, orderQty, dateFrm, dateTo, r.getProductCode());
		}
		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(4));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		JasperDesign emlipDesign = null;
		JasperReport emlipRpt = null;
		JasperPrint emlipPrnt = null;

		JRBeanCollectionDataSource emlipDataSrc = new JRBeanCollectionDataSource(emlipIssues);
		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R EMLIP.pdf";

		try
		{
			emlipDesign = JRXmlLoader.load("./Reports/EMLIP_REPORT.jrxml");
			emlipRpt = JasperCompileManager.compileReport(emlipDesign);
			emlipPrnt  = JasperFillManager.fillReport(emlipRpt, parameters, emlipDataSrc);
			JasperExportManager.exportReportToPdfFile(emlipPrnt,pdfName);
			new RenderPdf().renderPdf(pdfName);	
		}
		catch(JRException e)
		{
			JOptionPane.showMessageDialog(null,
					"<html><h2>ERROR GENERATING EMLIP Report & Requisition for the period<br> <font color=red> "
									+ frmDate
									+ " to "
									+ toDate
									+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
			System.out.println("EMLIP ERROR:"+e.getMessage());
		}
	}
	//USED TO GENERATE LAB USAGE REPORT
	public void generateLabUsageReport(String frmDate, String toDate, String normalOrder, String remarks)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportRequisitionDao labDataDao = new ReportRequisitionDao();
		List<ReportRequisition> labList = labDataDao.getLabData(dateFrm, dateTo);

		for (ReportRequisition r: labList)
		{
			prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
			int beginBal = spcDao.getpreviousPhysicalCount(prevDate, r.getProductCode());
			if (beginBal == 0)
				r.setBeginningBal(r.getTotalReceived());
			else
				r.setBeginningBal(beginBal);
			r.setReportPeriodFrm(dateFrm);
			r.setReportPeriodTo(dateTo);
			labDataDao.insertLab(r);
		}
		List<ReportRequisition> labIssues = reportLineItemDao.getLabData(dateFrm, dateTo);

		for (ReportRequisition r: labIssues)
		{
			double amc = amcDao.calculateLabAmc(r.getProductCode());
			//System.out.println(r.getProductCode().trim());
			int maxQty = (int)amc * 3;
			int phyCount = r.getPhyCount();
			int orderQty = maxQty - phyCount;
			r.setAmc(amc);
			r.setOrderQty(orderQty);
			r.setMaxQty(maxQty);
			labDataDao.updateLabRequisitionParams((int)amc, maxQty, orderQty, dateFrm, dateTo, r.getProductCode());
		}
		JasperReport masterReport, equipReport;
		JasperPrint masterPrint;
		JasperDesign masterDesign, equipDesign;

		LaboratoryTestInformationDao labTestDao = new LaboratoryTestInformationDao();
		List<LaboratoryTestInformation> listLabTestInfo = labTestDao.getLabEquipmentInfoReport(dateFrm, dateTo);


		JRBeanCollectionDataSource masterDataSrc = new JRBeanCollectionDataSource(labIssues);
		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" LAB_USAGE_REPORT.pdf";

		parameters.put("equipInfo", listLabTestInfo);
		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  prop.getStockLevelProp(3));
		parameters.put("emergencyOrderPoint", prop.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		try {
			equipDesign = JRXmlLoader.load("./Reports/EQUIPMENT_INFO_REPORT.jrxml");
			equipReport = JasperCompileManager.compileReport(equipDesign);

			masterDesign = JRXmlLoader.load("./Reports/LAB_MASTER_REPORT.jrxml");		    
			masterReport = JasperCompileManager.compileReport(masterDesign);

			masterPrint  = JasperFillManager.fillReport(masterReport, parameters, masterDataSrc);

			JasperExportManager.exportReportToPdfFile(masterPrint,pdfName);

			RenderPdf renderPdf  = new RenderPdf();
			renderPdf.renderPdf(pdfName);
		}
		catch (JRException e)
		{
			JOptionPane.showMessageDialog(null,
					"<html><h2>ERROR GENERATING LAB USAGE FOR THE PERIOD<br> <font color=red> "
									+ frmDate
									+ " to "
									+ toDate
									+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
			System.out.println("---"+e.getMessage());
		}
	}

	public void generateLabEquipInfoReport(String frmDate, String toDate)
	{
		LaboratoryTestInformationDao labTestDao = new LaboratoryTestInformationDao();
		List<LaboratoryTestInformation> listLabTestInfo = labTestDao.getLabEquipmentInfoReport(calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate)), calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate)));

		JasperDesign labTestDesign = null;
		JasperReport labTestRpt = null;
		JasperPrint labTestPrnt = null;

		JRBeanCollectionDataSource labTestDataSrc = new JRBeanCollectionDataSource(listLabTestInfo);
		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" EQUIPMENT_INFO_REPORT.pdf";

		try
		{
			labTestDesign = JRXmlLoader.load("./Reports/EQUIPMENT_INFO_REPORT.jrxml");
			labTestRpt = JasperCompileManager.compileReport(labTestDesign);
			labTestPrnt  = JasperFillManager.fillReport(labTestRpt, parameters, labTestDataSrc);
			JasperExportManager.exportReportToPdfFile(labTestPrnt,pdfName);
			new RenderPdf().renderPdf(pdfName);	
		}
		catch(JRException e)
		{
			System.out.println("EQUIPMENT_INFO_REPORT ERROR:"+e.getMessage());
		}
	}
	public void generatePhysicalCountReport(List<StorePhysicalCount> listPhyCount, String frmDate, String toDate)
	{
		JasperDesign labTestDesign = null;
		JasperReport labTestRpt = null;
		JasperPrint labTestPrnt = null;

		JRBeanCollectionDataSource labTestDataSrc = new JRBeanCollectionDataSource(listPhyCount);
		String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" PHYSICAL_COUNT.pdf";

		try
		{
			labTestDesign = JRXmlLoader.load("./Reports/physical_count.jrxml");
			labTestRpt = JasperCompileManager.compileReport(labTestDesign);
			labTestPrnt  = JasperFillManager.fillReport(labTestRpt, parameters, labTestDataSrc);
			JasperExportManager.exportReportToPdfFile(labTestPrnt,pdfName);
			new RenderPdf().renderPdf(pdfName);	
		}
		catch(JRException e)
		{
			System.out.println("PHYSICAL COUNT REPORT ERROR:"+e.getMessage());
		}
	}
}