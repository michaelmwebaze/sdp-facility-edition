/*
 * ReportsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.elmis.facility.dashboard.reporting.ReportAndRequisition;
import org.elmis.forms.reports.consumptiontrend.SelectProductsJD;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

/**
 * 
 * @author __USER__
 */
public class ReportsJP extends javax.swing.JPanel {

	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	private static java.util.Date startDate;
	private static java.util.Date endDate;

	/** Creates new form ReportsJP */
	public ReportsJP() {
		initComponents();

		jLabel4.setVisible(true);
		jLabel5.setVisible(false);
		
		stockTrendsJL2.setVisible(true);
		//arvpatientregimenJL.setVisible(false);
		//jLabel2.setVisible(false);

		//weeklyRptJL.setVisible(false);
		//staffproductivityJL.setVisible(false);

		// productivityJBtn.setVisible(false);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		stockTrendsJL = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jPanel3 = new javax.swing.JPanel();
		stockTrendsJL2 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				infoBox("Under Constructiion", "Report under construction");
			}
		});
		jPanel4 = new javax.swing.JPanel();
		arvpatientregimenJL = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Stock Reports",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		stockTrendsJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		stockTrendsJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock trends icon.png"))); // NOI18N
		stockTrendsJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Stock Trends",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		stockTrendsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				stockTrendsJLMouseClicked(evt);
			}
		});

		jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock out report icon.png"))); // NOI18N
		jLabel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Stockout Report",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												stockTrendsJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												106,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel2).addContainerGap(
												22, Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout.createSequentialGroup().addComponent(jLabel2)
						.addContainerGap()).addGroup(
				jPanel1Layout.createSequentialGroup().addComponent(
						stockTrendsJL, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGap(123, 123, 123)));

		jPanel3.setBackground(new java.awt.Color(102, 102, 102));
		jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Reports & Requisitions",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		stockTrendsJL2.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS view r&r.png"))); // NOI18N
		stockTrendsJL2.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "View R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		stockTrendsJL2.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				createRandrDoubleMouseClicked(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS archived r&r.png"))); // NOI18N
		jLabel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Archived R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout
				.setHorizontalGroup(jPanel3Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel3Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(stockTrendsJL2)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel4).addContainerGap(
												22, Short.MAX_VALUE)));
		jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel3Layout.createSequentialGroup().addGroup(
						jPanel3Layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(stockTrendsJL2).addComponent(
										jLabel4)).addContainerGap(29,
						Short.MAX_VALUE)));

		jPanel4.setBackground(new java.awt.Color(102, 102, 102));
		jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Reports ",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		arvpatientregimenJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		arvpatientregimenJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS arv regiment.png"))); // NOI18N
		arvpatientregimenJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "ARV Regimen",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		arvpatientregimenJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				arvpatientregimenJLMouseClicked(evt);
			}
		});

		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS archived r&r.png"))); // NOI18N
		jLabel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Archived R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout
				.setHorizontalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												arvpatientregimenJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												111,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(jLabel5).addContainerGap(
												13, Short.MAX_VALUE)));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel4Layout.createSequentialGroup().addGroup(
						jPanel4Layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jLabel5).addComponent(
										arvpatientregimenJL)).addContainerGap(
						29, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												jPanel3,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jPanel4,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { jPanel1, jPanel3, jPanel4 });

		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																jPanel3,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING,
																				false)
																		.addComponent(
																				jPanel4,
																				javax.swing.GroupLayout.Alignment.LEADING,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				jPanel1,
																				javax.swing.GroupLayout.Alignment.LEADING,
																				0,
																				169,
																				Short.MAX_VALUE)))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { jPanel1, jPanel3, jPanel4 });

	}// </editor-fold>
	//GEN-END:initComponents

	public static void infoBox(String infoMessage, String location)
    {
        JOptionPane.showMessageDialog(null, infoMessage, "Information: " + location, JOptionPane.INFORMATION_MESSAGE);
    }
	
	@SuppressWarnings("unused")
	private void arvpatientregimenJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		infoBox("Under Constructiion", "Report under construction");

	}

	private void createRandrDoubleMouseClicked(java.awt.event.MouseEvent evt) {
		AppJFrame.glassPane.activate(null);
		ReportAndRequisition dialog = new ReportAndRequisition(
				javax.swing.JOptionPane.getFrameForComponent(this), true,
				"msolomon");
		dialog.pack();
		dialog.setVisible(true);
		AppJFrame.glassPane.deactivate();
	}

	private void stockTrendsJL1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
	}

	private void stockTrendsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//	new DateIntervalJD(javax.swing.JOptionPane.getFrameForComponent(this),
		//		true);
		//startDate = new DateRange().getStartDate();
		//	endDate = new DateRange().getEndDate();

		/*	JFreeChart chartIn = new TimeSeriesChart().createChart("Consumption Trend for Acyclovir, tablet 400mg");
			
			AppJFrame.glassPane.activate(null);

			new TimeSeriesJD(javax.swing.JOptionPane.getFrameForComponent(this),
					true, chartIn);
			
			AppJFrame.glassPane.deactivate();*/
		AppJFrame.glassPane.activate(null);
		new SelectProductsJD(
				javax.swing.JOptionPane.getFrameForComponent(this), true);
		AppJFrame.glassPane.deactivate();

	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel arvpatientregimenJL;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JLabel stockTrendsJL;
	private javax.swing.JLabel stockTrendsJL2;
	// End of variables declaration//GEN-END:variables

}