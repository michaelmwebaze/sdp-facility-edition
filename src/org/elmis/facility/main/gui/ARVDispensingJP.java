/*
 * StartJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;

import org.elmis.facility.controllers.facilityarvdispensingsessionsmappercalls;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.reports.utils.ReportStorageManager;
import org.elmis.forms.stores.arv_dispensing.arvdispensingProductsPhysicalCountJD;
import org.elmis.forms.stores.arv_dispensing.arvdispensingproductAdjustmentsJD;
import org.elmis.forms.stores.arv_dispensing.copyReceivingJD;
import org.elmis.forms.stores.arv_dispensing.DAR_report.RenderPdf;

/**
 * 
 * @author __USER__
 */
public class ARVDispensingJP extends javax.swing.JPanel implements
		ActionListener {

	//private static Map parameterMap = new HashMap();
	//private JasperPrint print;
	private static java.util.Date startDate;
	private static java.util.Date endDate;

	private static Map parameterMap = new HashMap();

	facilityarvdispensingsessionsmappercalls callmapper;
	List<Elmis_Dar_Transactions> arvfixeddarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvalldardoseproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumsingledosedarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumliquidpowderdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumcotrimoxazoledarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsingledosedarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvliquid_powderdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvcotridarproductsList = new LinkedList();
	//private Logger logger = Logger.getLogger(this.getClass());
	private JasperPrint print;
	private ReportStorageManager reportstoragemanager;
	

	/** Creates new form StartJP */
	public ARVDispensingJP(List<String> userRightsList) {
		initComponents();

		// organiserJL.setVisible(false);

		for (String userRights : userRightsList) {

			//hiv Testing
			/*if (this.hivTestingJL.getName().equalsIgnoreCase(userRights.trim())) {
					hivTestingJL.setVisible(true);
				}
			 */
		}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		dispenseJL = new javax.swing.JLabel();
		viewARVdarJL = new javax.swing.JLabel();
		ARVdarJL = new javax.swing.JLabel();
		dispenseJL1 = new javax.swing.JLabel();
		physicalCountJL = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		dispenseJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		dispenseJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/income_reports_55.png"))); // NOI18N
		dispenseJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Dispensing"));
		dispenseJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				//dispenseJLMouseClicked(evt);
			}
		});

		viewARVdarJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view dar icon.png"))); // NOI18N
		viewARVdarJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "View DAR",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewARVdarJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewARVdarJLMouseClicked(evt);
			}
		});

		ARVdarJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		ARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS losses&adjust.png"))); // NOI18N
		ARVdarJL.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Adjustments",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		ARVdarJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ARVdarJLMouseClicked(evt);
			}
		});

		dispenseJL1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		dispenseJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS Dispensing icon.png"))); // NOI18N
		dispenseJL1.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Dispensing",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		dispenseJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				dispenseJL1MouseClicked(evt);
			}
		});

		physicalCountJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		physicalCountJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS Physical count.png"))); // NOI18N
		physicalCountJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Physical Count",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		physicalCountJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		physicalCountJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addGap(
																				456,
																				456,
																				456)
																		.addComponent(
																				dispenseJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				96,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				dispenseJL1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				96,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				ARVdarJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				100,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				viewARVdarJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				110,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				physicalCountJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				113,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(124, Short.MAX_VALUE)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												dispenseJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												0,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																false)
														.addComponent(
																physicalCountJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																viewARVdarJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																ARVdarJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																dispenseJL1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addContainerGap(218, Short.MAX_VALUE)));
	}// </editor-fold>
	//GEN-END:initComponents

	private void physicalCountJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling
		AppJFrame.glassPane.activate(null);
		new arvdispensingProductsPhysicalCountJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	public static Date toDate(java.sql.Timestamp timestamp) {
		long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		return new Date(millisec);

	}

	private void viewARVdarJLMouseClicked(java.awt.event.MouseEvent evt)
			 {
		// TODO add your handling code here:
		// TODO add your handling code here:
		//elmis_dar_transactions =  new Elmis_Dar_Transactions();

		//create a timestamp 

		Timestamp productdeliverdate = null;
		String oldDateString;
		String mydate = new java.util.Date().toString();
		String newDateString;
		final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
		//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
		final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
		oldDateString = mydate;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		java.util.Date d;
		d = null;
		try {
			d = sdf.parse(oldDateString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
		productdeliverdate = Timestamp.valueOf(newDateString);

		//end create time stamp

		Date Startdate = new Date();
		Date Enddate = new Date();
		if (productdeliverdate != null) {

			//convert to date value 	
			Date convertdate = toDate(productdeliverdate);
			//date split up methods
			Calendar cal = Calendar.getInstance();
			cal.setTime(convertdate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if (!(day == 1)) {
				day = 1;
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.DATE, day);
				cal.set(Calendar.YEAR, year);
				Startdate = cal.getTime();
			}
			Enddate = toDate(productdeliverdate);
		}

		callmapper = new facilityarvdispensingsessionsmappercalls();

		arvfixeddarproductsList = callmapper.dogetcurrentARVdar(Startdate,
				Enddate);
		arvsingledosedarproductsList = callmapper.dosingledoseARVdar(Startdate,
				Enddate);
		arvalldardoseproductsList = callmapper.dogetcurrentAlldoseARVdar(
				Startdate, Enddate);
		this.arvliquid_powderdarproductsList = callmapper
				.doliquidpowderdoseARVdar(Startdate, Enddate);
		this.arvcotridarproductsList = callmapper.docotrimoxazoledoseARVdar(
				Startdate, Enddate);
		arvsumdarproductsList = callmapper
				.doselectsumDARarv(Startdate, Enddate);
		this.arvsumsingledosedarproductsList = callmapper
				.doselectsumsingledoseDARarv(Startdate, Enddate);
		this.arvsumcotrimoxazoledarproductsList = callmapper
				.doselectsumCotimoxazoleDARarv(Startdate, Enddate);
		this.arvsumliquidpowderdarproductsList = callmapper
				.doselectsumliquidpowderDARarv(Startdate, Enddate);

		System.out.println("I am size..." + arvfixeddarproductsList.size());

		System.out.println(new java.io.File("").getAbsolutePath());

		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			int totalmonthdispensed = 0;
			int sumtotal = 0;
			int sumliquidpowder = 0;
			int sumsingledose = 0;
			int sumcotrimoxazole = 0;
			Timestamp thisdate = null;
			String thisartnumber = null;
			String thisproductname = null;
			Integer thisdispensedbottle = 0;

			Timestamp thisliquiddate = null;
			String thisliquidartnumber = null;
			String thisliquidproductname = null;
			Integer thisliquiddispensedbottle = 0;

			for (Elmis_Dar_Transactions e : arvsumdarproductsList) {
				if (!(e == null)) {
					sumtotal = e.getTotaldispensed();
				}
			}
			for (Elmis_Dar_Transactions e : arvsumsingledosedarproductsList) {
				if (!(e == null)) {
					sumsingledose = e.getTotaldispensed();
				}
			}
			for (Elmis_Dar_Transactions e : arvsumcotrimoxazoledarproductsList) {
				if (!(e == null)) {
					sumcotrimoxazole = e.getTotaldispensed();
				}

			}
			for (Elmis_Dar_Transactions e : arvsumliquidpowderdarproductsList) {

				if (!(e == null)) {
					sumliquidpowder = e.getTotaldispensed();
				}

			}

			for (Elmis_Dar_Transactions e : arvsingledosedarproductsList) {
				thisdate = e.getTransaction_date();
				thisartnumber = e.getArt_number();
				thisproductname = e.getPrimaryname();
				thisdispensedbottle = e.getNo_equiv_bottles();

			}

			for (Elmis_Dar_Transactions e : arvliquid_powderdarproductsList) {

				thisliquiddate = e.getTransaction_date();
				thisliquidartnumber = e.getArt_number();
				thisliquidproductname = e.getPrimaryname();
				thisliquiddispensedbottle = e.getNo_equiv_bottles();

			}

			//load report location
			String masterfile = "./Reports/ARVdarreport.jrxml";
			String subfile = "./Reports/ARVdarsingledosereport.jrxml";

			totalmonthdispensed = sumtotal + sumsingledose + sumcotrimoxazole
					+ sumliquidpowder;
			//parameters.put("printdetail3",arvsingledosedarproductsList.size()) ;
			parameters.put("totaldispensed", totalmonthdispensed);
			//parameters.put("ARVdarsingledose", arvsingledosedarproductsList);

			//Single dose parameters 
			/*parameters.put("dispensedate", thisdate);
			parameters.put("patientnumber",thisartnumber);
			parameters.put("arvproductname", thisproductname);
			parameters.put("dispensedbottles", thisdispensedbottle);*/
			parameters.put("dispenserInitial", AppJFrame.userLoggedIn);

			//Liquid powder parameters
			/*	parameters.put("liquidpowderdate", thisliquiddate);
				parameters.put("liquidpowderpatientnumber",thisliquidartnumber);
				parameters.put("liquidpowderproductname", thisliquidproductname);
				parameters.put("liquidpowderdispensedbottle", thisliquiddispensedbottle);*/

			@SuppressWarnings("unused")
			JasperReport rptsingledose, rptmaster;// = null;
			@SuppressWarnings("unused")
			JasperPrint prtsingledose, prtmaster;// = null;
			@SuppressWarnings("unused")
			JasperDesign subReportsingledose, master;// = null;
			JRBeanCollectionDataSource masterbeanColDataSource = new JRBeanCollectionDataSource(
					arvalldardoseproductsList);
			@SuppressWarnings("unused")
			JRBeanCollectionDataSource singledosebeanColDataSource = new JRBeanCollectionDataSource(
					arvsingledosedarproductsList);
			master = JRXmlLoader.load("./Reports/ARVdarreport.jrxml");
			//subReportsingledose = JRXmlLoader.load("C:\\Users\\mkausa\\Documents\\programmers\\developerclone_workspace\\elmis-facility (withAllAdmin)\\ARVdarsingledosereport.jrxml");
			rptmaster = JasperCompileManager.compileReport(master);
			// rptsingledose = JasperCompileManager.compileReport(subReportsingledose);	
			prtmaster = JasperFillManager.fillReport(rptmaster, parameters,
					masterbeanColDataSource);
			//prtsingledose = JasperFillManager.fillReport(rptsingledose, parameters, singledosebeanColDataSource);

			reportstoragemanager = new ReportStorageManager();
			String pdfpath = null;
			if (reportstoragemanager.isFolderAvailable() == true) {
				System.out.println("found folder for pdf");
				/*pdfpath = reportstoragemanager.getReportFolder();
				JasperExportManager.exportReportToPdfFile(prtmaster, pdfpath
						+ "/" + "arvdarreport.pdf");
				reportstoragemanager.getReportFolder();
				RenderPdf renderPdf = new RenderPdf();
				renderPdf.renderPdf(pdfpath + "/" + "arvdarreport.pdf");*/
				JFrame frame = new JFrame("Report");
				frame.getContentPane().setPreferredSize(new Dimension(1200, 700));
				frame.getContentPane().add(new JRViewer(prtmaster));
				frame.pack();
				frame.setVisible(true);
			

			}

			/*JasperExportManager.exportReportToPdfFile(prtmaster,
					"C:\\Users\\mkausa\\Desktop\\arvdarreport.pdf");*/

			//	RenderPdf renderPdf = new RenderPdf();

			//	renderPdf.renderPdf("C:\\Users\\mkausa\\Desktop\\arvdarreport.pdf");

		} catch (JRException ex) {
			System.out.println("---" + ex.getLocalizedMessage());
		}

	}

	private void ARVdarJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		AppJFrame.glassPane.activate(null);
		new arvdispensingproductAdjustmentsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();

	}

	private void dispenseJL1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		AppJFrame.glassPane.activate(null);
		new copyReceivingJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);

		AppJFrame.glassPane.deactivate();

	}

	/********* Action Listener Coding Starts *************/
	// this action Listener for phone book
	//
	public void actionPerformed(ActionEvent evt) {

		AppJFrame.glassPane.activate(null);
		//new ContactAdminJD(javax.swing.JOptionPane.getFrameForComponent(this),

		AppJFrame.glassPane.deactivate();

	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel ARVdarJL;
	private javax.swing.JLabel dispenseJL;
	private javax.swing.JLabel dispenseJL1;
	private javax.swing.JLabel physicalCountJL;
	private javax.swing.JLabel viewARVdarJL;
	// End of variables declaration//GEN-END:variables

}