/*
 * AdminJPanel.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.util.List;

import org.elmis.facility.system.SystemSettingJD;
import org.elmis.forms.admin.dispensingpoints.DispensingPointAdminJD;
import org.elmis.forms.users.UsersAdminTabJD;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Color;

/**
 * 
 * @author __USER__
 */
public class AdminJPanel extends javax.swing.JPanel {

	List<String> userRightsList;
	private String userRights;

	/** Creates new form AdminJPanel */
	public AdminJPanel(List<String> userRightsList) {

		this.userRightsList = userRightsList;
		initComponents();

		usersAdminJL.setVisible(false);
		companySettingsJL.setVisible(false);
		databaseSettingsJL.setVisible(false);

		for (String userRights : userRightsList) {

			//Manage users
			if (this.usersAdminJL.getName().equalsIgnoreCase(userRights.trim())) {
				usersAdminJL.setVisible(true);
			}

			//Manage facility

			if (this.companySettingsJL.getName().equalsIgnoreCase(
					userRights.trim())) {
				companySettingsJL.setVisible(true);
			}

			//Manage Database
			if (this.databaseSettingsJL.getName().equalsIgnoreCase(
					userRights.trim())) {
				databaseSettingsJL.setVisible(true);
			}
		}
		
		
		
		
		//disable functions if working offline 
	if(System.getProperty("dburl").equalsIgnoreCase("jdbc:hsqldb:hsql://localhost:5000/elmis_facility")){
		databaseSettingsJL.setEnabled(false);
		companySettingsJL.setEnabled(false);
		usersAdminJL.setEnabled(false);
		DispensingPointsJL.setEnabled(false);
		
		DispensingPointsJL.enableInputMethods(false);
		
		//DispensingPointsJL.removeMouseListener(java.awt.event.MouseEvent evt);
		//
	//	((Component) event.getSource()).removeMouseListener(this);
		
	}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		companySettingsJL = new javax.swing.JLabel();
		usersAdminJL = new javax.swing.JLabel();
		databaseSettingsJL = new javax.swing.JLabel();
		DispensingPointsJL = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());
		setForeground(new java.awt.Color(255, 255, 255));

		companySettingsJL.setBackground(new java.awt.Color(102, 102, 102));
		companySettingsJL.setForeground(new java.awt.Color(102, 102, 102));
		companySettingsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		companySettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/eLMIS facility icon.png"))); // NOI18N
		companySettingsJL.setText(" ");
		companySettingsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Facility",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		companySettingsJL.setName("MANAGE_FACILITY");
		companySettingsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseExited(evt);
			}
		});

		usersAdminJL.setBackground(new java.awt.Color(102, 102, 102));
		usersAdminJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/User Admin icon.png"))); // NOI18N
		usersAdminJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "User Admin",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		usersAdminJL.setName("MANAGE_USER");
		usersAdminJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseExited(evt);
			}
		});

		databaseSettingsJL.setBackground(new java.awt.Color(102, 102, 102));
		databaseSettingsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		databaseSettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/eLMIS database icon.png"))); // NOI18N
		databaseSettingsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Database Settings",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		databaseSettingsJL.setName("MANAGE_DATABASE");
		databaseSettingsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseExited(evt);
			}
		});

		DispensingPointsJL.setBackground(new java.awt.Color(102, 102, 102));
		DispensingPointsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		DispensingPointsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Dispensing point.png"))); // NOI18N
		DispensingPointsJL.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), null), "Service Point", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
		DispensingPointsJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		DispensingPointsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				DispensingPointsJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				DispensingPointsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				DispensingPointsJLMouseExited(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												companySettingsJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												133,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												usersAdminJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												133,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												databaseSettingsJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												133,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(18, 18, 18)
										.addComponent(
												DispensingPointsJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												133,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(111, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addGroup(
						layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(DispensingPointsJL).addComponent(
										databaseSettingsJL).addComponent(
										usersAdminJL).addComponent(
										companySettingsJL)).addContainerGap(
						142, Short.MAX_VALUE)));
	}// </editor-fold>
	//GEN-END:initComponents

	private void DispensingPointsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		

		DispensingPointsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Dispensing point.png")));
		
		
	}

	private void DispensingPointsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		DispensingPointsJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMISdispensing icon big.png")));
	}

	private void databaseSettingsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		databaseSettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS database icon.png")));
	}

	private void databaseSettingsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		databaseSettingsJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS database icon big.png")));
	}

	private void usersAdminJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/User Admin icon.png")));
	}

	private void usersAdminJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS user admin icon big.png")));
	}

	private void companySettingsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		companySettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/eLMIS facility icon.png")));
	}

	private void companySettingsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		companySettingsJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS facility icon big.png")));
	}

	private void DispensingPointsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		
				if(!DispensingPointsJL.isEnabled()){
					
				//if button is disabled because the system is running offline 
					//do nothing 	
					
				}else{
					
					
		AppJFrame.glassPane.activate(null);
		new DispensingPointAdminJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);
		AppJFrame.glassPane.deactivate();
		
				}
	}

	private void databaseSettingsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		if(!databaseSettingsJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
				//do nothing 	
				
			}else{

		AppJFrame.glassPane.activate(null);

		new NetworkSettingsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
		
			}

	}

	private void usersAdminJLMouseClicked(java.awt.event.MouseEvent evt) {
		
		
		if(!usersAdminJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
				//do nothing 	
				
			}else{

		AppJFrame.glassPane.activate(null);

		new UsersAdminTabJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);
		AppJFrame.glassPane.deactivate();
		
			}
	}

	private void companySettingsJLMouseClicked(java.awt.event.MouseEvent evt) {
		
		if(!companySettingsJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
				//do nothing 	
				
			}else{
		AppJFrame.glassPane.activate(null);

		new SystemSettingJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);

		AppJFrame.glassPane.deactivate();
		
			}
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel DispensingPointsJL;
	private javax.swing.JLabel companySettingsJL;
	private javax.swing.JLabel databaseSettingsJL;
	private javax.swing.JLabel usersAdminJL;
	// End of variables declaration//GEN-END:variables

}