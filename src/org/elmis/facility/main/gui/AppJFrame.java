/*
` * AppJFrame.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JInternalFrame;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.dashboard_elmis.DashboardJD;
import org.elmis.facility.domain.model.Role_rights;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.network.EmbeddedServer;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.network.NetworkConnectFailJD;
import org.elmis.facility.network.NetworkMode;
import org.elmis.facility.setup.NewClientSetupJD;
import org.elmis.facility.system.SystemSettingJD;
import org.elmis.facility.tools.Login;
import org.elmis.facility.tools.MyDisabledGlassPane;
import org.elmis.facility.tools.NetworkProperties;
import org.elmis.forms.admin.dispensingpoints.SelectLocationJD;

import com.oribicom.tools.publicMethods;

/**
 * 
 * @author __USER__
 */
public class AppJFrame extends javax.swing.JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	public static javax.swing.JDesktopPane desktop;
	public static String userName = "";
	public static String password = "";
	public static String accessLevel;
	public static String userLoggedIn;
	//public static String userDisplayName;
	public static String FrameTitle = "";
	public static String eventFrom = "";

	public static String userid = "";

	private static java.awt.Image appIcon;

	public static MyDisabledGlassPane glassPane = new MyDisabledGlassPane();

	public static Properties prop;
	private FileInputStream fis;

	public static String dbdriver;
	public static String dbhost;
	public static String dbport;
	public static String dbname;
	public static String dbuser;
	public static String dbpassword;
	public static String clientType;
	public static String dburl;
	public static String dp_type;

	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	// list of rights
	private List<String> userRightsList;
	private List<String> rightsGroupList;

	public static Frame frame;
	private users user;

	/** Creates new form AppJFrame */
	public AppJFrame() {

		// read properties file

		prop = new Properties(System.getProperties());
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		dbdriver = prop.getProperty("dbdriver");

		dbhost = prop.getProperty("dbhost");

		dbport = prop.getProperty("dbport");
		dbname = prop.getProperty("dbname");
		dbuser = prop.getProperty("dbuser");
		dbpassword = prop.getProperty("dbpassword");
		clientType = prop.getProperty("clientType");//
		dp_type = prop.getProperty("dp_type");

		System.setProperty("dburl", "jdbc:postgresql://"
				+ System.getProperty("dbhost") + ":"
				+ System.getProperty("dbport") + "/"
				+ System.getProperty("dbname"));

		// check if this is the first time the application is being run
		// after a new installation
		if (clientType.equalsIgnoreCase("null")) {

			new NewClientSetupJD(javax.swing.JOptionPane
					.getFrameForComponent(this), true);

			if (System.getProperty("facilitySetup").equalsIgnoreCase("no")) {

				javax.swing.JOptionPane
						.showMessageDialog(
								this,
								"This facility has not yet been configured "
										+ "you need to \nconfigure the facility before you can proceed");
				new SystemSettingJD(javax.swing.JOptionPane
						.getFrameForComponent(this), true);

			}

			// runStartup();
			new NetworkProperties().writeToPropertiesFile();

		}

		// check database connection
		Connection conn = NetworkMode.getConn();

		// if no connection to db found
		if (conn == null) {

			// if client is a satellite or stand alone
			if (clientType.equalsIgnoreCase("satellite")
					|| clientType.equalsIgnoreCase("standalone")) {
				// start embedded database
				new EmbeddedServer().startup();

				// start application
				runStartup();

			}
			// if client is a networked client
			else if (clientType.equalsIgnoreCase("networked")) {

				// prompt user that not connection to db found ..
				int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
						"Unable to connect to Server database\n\n"
								+ "Yes - to work offline\n\n"
								+ "No -to try and connect to database",
						"database connection erro", 1);
				// yes work off-line
				if (confirm == 0) {
					// set db to work local

					// start embedded server
					new EmbeddedServer().startup();

					// start application
					runStartup();

				}
				// no check network connection
				else if (confirm == 1) {
					// bring up the network setting JD
					new NetworkConnectFailJD(javax.swing.JOptionPane
							.getFrameForComponent(this), true);

					// check database connection
					Connection connection = NetworkMode.getConn();

					if (connection == null) {

						// prompt user that not connection to db found ..
						int confirmm = javax.swing.JOptionPane
								.showConfirmDialog(
										this,
										"Unable to connect to Server database\n\n"
												+ "Yes - to work offline\n\n"
												+ "No -to try and connect to database",
										"database connection erro", 1);
						// yes work off-line
						if (confirmm == 0) {

							// start embedded server
							new EmbeddedServer().startup();

							// set the URL Property
							System.setProperty("dburl",
											"jdbc:hsqldb:hsql://localhost:5000/elmis_facility");

							// start application
							runStartup();

						}

					} else {

						System.setProperty("dburl", "jdbc:postgresql://"
								+ dbhost + ":" + dbport + "/" + dbname);

						// start application
						runStartup();

					}

				}

			}

		} // else if connection is found
		else {

			// if client is a satellite or stand alone
			if (clientType.equalsIgnoreCase("satellite")
					|| clientType.equalsIgnoreCase("standalone")) {
				// start embedded database
				new EmbeddedServer().startup();

				// set the URL Property
				System.setProperty("dburl",
						"jdbc:hsqldb:hsql://localhost:5000/elmis_facility");

				// start application
				runStartup();

			} else {// if client is networked

				System.setProperty("dburl", "jdbc:postgresql://" + dbhost + ":"
						+ dbport + "/" + dbname);
				// start application
				runStartup();
			}

			// TODO
			// start thread to sync db
		}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		buttonGroup1 = new javax.swing.ButtonGroup();
		jPanel4 = new javax.swing.JPanel();
		jToolBar1 = new javax.swing.JToolBar();
		jLabel4 = new javax.swing.JLabel();
		userNameJL = new javax.swing.JLabel();
		jSeparator1 = new javax.swing.JToolBar.Separator();
		jSeparator4 = new javax.swing.JToolBar.Separator();
		jSeparator6 = new javax.swing.JToolBar.Separator();
		jSeparator7 = new javax.swing.JToolBar.Separator();
		jSeparator8 = new javax.swing.JToolBar.Separator();
		jSeparator9 = new javax.swing.JToolBar.Separator();
		jSeparator10 = new javax.swing.JToolBar.Separator();
		jSeparator11 = new javax.swing.JToolBar.Separator();
		jSeparator12 = new javax.swing.JToolBar.Separator();
		jSeparator3 = new javax.swing.JToolBar.Separator();
		jSeparator15 = new javax.swing.JToolBar.Separator();
		connectionStatusJL = new javax.swing.JLabel();
		jSeparator13 = new javax.swing.JToolBar.Separator();
		jSeparator5 = new javax.swing.JToolBar.Separator();
		jSeparator16 = new javax.swing.JToolBar.Separator();
		jSeparator17 = new javax.swing.JToolBar.Separator();
		jSeparator18 = new javax.swing.JToolBar.Separator();
		jSeparator19 = new javax.swing.JToolBar.Separator();
		jSeparator20 = new javax.swing.JToolBar.Separator();
		jSeparator21 = new javax.swing.JToolBar.Separator();
		jSeparator22 = new javax.swing.JToolBar.Separator();
		jSeparator23 = new javax.swing.JToolBar.Separator();
		jSeparator24 = new javax.swing.JToolBar.Separator();
		dashboardButton = new javax.swing.JButton();
		jSeparator26 = new javax.swing.JToolBar.Separator();
		jSeparator27 = new javax.swing.JToolBar.Separator();
		jSeparator28 = new javax.swing.JToolBar.Separator();
		jSeparator29 = new javax.swing.JToolBar.Separator();
		jSeparator30 = new javax.swing.JToolBar.Separator();
		jSeparator25 = new javax.swing.JToolBar.Separator();
		locationJL = new javax.swing.JLabel();
		jSeparator32 = new javax.swing.JToolBar.Separator();
		jSeparator33 = new javax.swing.JToolBar.Separator();
		jSeparator31 = new javax.swing.JToolBar.Separator();
		jSeparator34 = new javax.swing.JToolBar.Separator();
		jSeparator35 = new javax.swing.JToolBar.Separator();
		changeLocationJBtn = new javax.swing.JButton();
		menuBar = new javax.swing.JMenuBar();
		fileMenu = new javax.swing.JMenu();
		exitMenuItem = new javax.swing.JMenuItem();
		RptJMenu = new javax.swing.JMenu();
		myTimeTodayJMI = new javax.swing.JMenuItem();
		allStaffTodayJMI = new javax.swing.JMenuItem();
		jSeparator2 = new javax.swing.JSeparator();
		expStatementJMI = new javax.swing.JMenuItem();
		statusRptJMI = new javax.swing.JMenuItem();
		helpMenu = new javax.swing.JMenu();
		aboutMenuItem = new javax.swing.JMenuItem();
		userGuideJMI = new javax.swing.JMenuItem();
		jProgressBar1 = new javax.swing.JProgressBar();
		jDesktopPane1 = new javax.swing.JDesktopPane();
		aboutJIF = new javax.swing.JInternalFrame();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel20 = new javax.swing.JLabel();
		jLabel21 = new javax.swing.JLabel();
		loginJIF = new javax.swing.JInternalFrame();
		jPanel13 = new javax.swing.JPanel();
		jLabel9 = new javax.swing.JLabel();
		userNameJTF = new javax.swing.JTextField();
		jLabel10 = new javax.swing.JLabel();
		passwordJPF = new javax.swing.JPasswordField();
		jLabel5 = new javax.swing.JLabel();
		loginJBtn = new javax.swing.JButton();
		forgotPasswordJL = new javax.swing.JLabel();
		truckJL = new javax.swing.JLabel();
		menuBar1 = new javax.swing.JMenuBar();
		fileMenu1 = new javax.swing.JMenu();
		exitMenuItem1 = new javax.swing.JMenuItem();
		helpMenu1 = new javax.swing.JMenu();
		aboutMenuItem1 = new javax.swing.JMenuItem();
		userGuideJMI1 = new javax.swing.JMenuItem();
		jSeparator14 = new javax.swing.JSeparator();
		jMenuItem1 = new javax.swing.JMenuItem();

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 100,
				Short.MAX_VALUE));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 100,
				Short.MAX_VALUE));

		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setTitle("eLMIS Facility");
		setIconImage(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage());
		setLocationByPlatform(true);
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				formWindowClosing(evt);
			}
		});

		jToolBar1.setBackground(new java.awt.Color(102, 102, 102));
		jToolBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jToolBar1.setFloatable(false);
		jToolBar1.setRollover(true);
		jToolBar1.setFont(new java.awt.Font("Tahoma", 1, 8));

		jLabel4.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS contact info small.png"))); // NOI18N
		jLabel4.setText("Current User :");
		jToolBar1.add(jLabel4);

		userNameJL.setFont(new java.awt.Font("Tahoma", 1, 11));
		jToolBar1.add(userNameJL);
		jToolBar1.add(jSeparator1);
		jToolBar1.add(jSeparator4);
		jToolBar1.add(jSeparator6);
		jToolBar1.add(jSeparator7);
		jToolBar1.add(jSeparator8);
		jToolBar1.add(jSeparator9);
		jToolBar1.add(jSeparator10);
		jToolBar1.add(jSeparator11);
		jToolBar1.add(jSeparator12);
		jToolBar1.add(jSeparator3);
		jToolBar1.add(jSeparator15);

		connectionStatusJL.setFont(new java.awt.Font("Gulim", 0, 11));
		connectionStatusJL.setForeground(new java.awt.Color(255, 255, 255));
		connectionStatusJL.setText("Disconnected from server");
		connectionStatusJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				connectionStatusJLMouseClicked(evt);
			}
		});
		jToolBar1.add(connectionStatusJL);
		jToolBar1.add(jSeparator13);
		jToolBar1.add(jSeparator5);
		jToolBar1.add(jSeparator16);
		jToolBar1.add(jSeparator17);
		jToolBar1.add(jSeparator18);
		jToolBar1.add(jSeparator19);
		jToolBar1.add(jSeparator20);
		jToolBar1.add(jSeparator21);
		jToolBar1.add(jSeparator22);
		jToolBar1.add(jSeparator23);
		jToolBar1.add(jSeparator24);

		dashboardButton.setBackground(new java.awt.Color(102, 102, 102));
		dashboardButton.setFont(new java.awt.Font("Gulim", 0, 11));
		dashboardButton.setForeground(new java.awt.Color(255, 255, 255));
		dashboardButton.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS dashboard small icon.png"))); // NOI18N
		dashboardButton.setText("Dashboard");
		dashboardButton.setFocusable(false);
		dashboardButton
				.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		dashboardButton
				.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		dashboardButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dashboardButtonActionPerformed(evt);
			}
		});
		jToolBar1.add(dashboardButton);
		jToolBar1.add(jSeparator26);
		jToolBar1.add(jSeparator27);
		jToolBar1.add(jSeparator28);
		jToolBar1.add(jSeparator29);
		jToolBar1.add(jSeparator30);
		jToolBar1.add(jSeparator25);

		locationJL.setFont(new java.awt.Font("Gulim", 0, 11));
		locationJL.setForeground(new java.awt.Color(255, 255, 255));
		locationJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS branch icon.png"))); // NOI18N
		locationJL.setText("Locatio:");
		jToolBar1.add(locationJL);
		jToolBar1.add(jSeparator32);
		jToolBar1.add(jSeparator33);
		jToolBar1.add(jSeparator31);
		jToolBar1.add(jSeparator34);
		jToolBar1.add(jSeparator35);

		changeLocationJBtn.setBackground(new java.awt.Color(102, 102, 102));
		changeLocationJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS refresh small icon.png"))); // NOI18N
		changeLocationJBtn.setToolTipText("Change Location");
		changeLocationJBtn.setFocusable(false);
		changeLocationJBtn
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		changeLocationJBtn
				.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
		changeLocationJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						changeLocationJBtnActionPerformed(evt);
					}
				});
		jToolBar1.add(changeLocationJBtn);

		fileMenu.setText("File");

		exitMenuItem.setText("Log-out");
		exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exitMenuItemActionPerformed(evt);
			}
		});
		fileMenu.add(exitMenuItem);

		menuBar.add(fileMenu);

		RptJMenu.setText("Reports");

		myTimeTodayJMI.setText("My Time Today");
		RptJMenu.add(myTimeTodayJMI);

		allStaffTodayJMI.setText("All Staff Today");
		RptJMenu.add(allStaffTodayJMI);
		RptJMenu.add(jSeparator2);

		expStatementJMI.setText("Expense Statement");
		RptJMenu.add(expStatementJMI);

		statusRptJMI.setText("Status Report");
		RptJMenu.add(statusRptJMI);

		menuBar.add(RptJMenu);

		helpMenu.setText("Help");

		aboutMenuItem.setText("About");
		aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				aboutMenuItemActionPerformed(evt);
			}
		});
		helpMenu.add(aboutMenuItem);

		userGuideJMI.setText("User Guide");
		userGuideJMI.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				userGuideJMIActionPerformed(evt);
			}
		});
		helpMenu.add(userGuideJMI);

		menuBar.add(helpMenu);

		jDesktopPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		aboutJIF.setClosable(true);
		aboutJIF
				.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
		aboutJIF.setTitle("About");

		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel1.setText("LegalManager 2.0");

		jLabel2.setText("www.yourlegalmanager.com");

		jLabel3.setText("Phone 251396 / 0977-68-4444");

		jLabel20.setText("email: info@yourlegalmanager.com");

		jLabel21.setText("Foxdale Court, Zambezi Rd. Roma Lusaka");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel2,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																214,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jLabel1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																184,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jLabel3,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																169,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jLabel20,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																188,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jLabel21,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																251,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(29, Short.MAX_VALUE)));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(jLabel1)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel2)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel3)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel20)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel21)
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		javax.swing.GroupLayout aboutJIFLayout = new javax.swing.GroupLayout(
				aboutJIF.getContentPane());
		aboutJIF.getContentPane().setLayout(aboutJIFLayout);
		aboutJIFLayout.setHorizontalGroup(aboutJIFLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				aboutJIFLayout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel1,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addContainerGap()));
		aboutJIFLayout.setVerticalGroup(aboutJIFLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				aboutJIFLayout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel1,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addContainerGap()));

		aboutJIF.setBounds(50, 50, 320, 150);
		jDesktopPane1.add(aboutJIF, javax.swing.JLayeredPane.DEFAULT_LAYER);

		loginJIF.setBorder(null);
		loginJIF.setTitle("Logistics Management Information Systems");
		loginJIF.setVisible(true);

		jPanel13.setBackground(new java.awt.Color(255, 255, 255));
		jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel9.setFont(new java.awt.Font("Gulim", 1, 11));
		jLabel9.setText("Username");

		userNameJTF.setFont(new java.awt.Font("Tahoma", 0, 14));

		jLabel10.setFont(new java.awt.Font("Gulim", 1, 11));
		jLabel10.setText("Password");

		passwordJPF.setFont(new java.awt.Font("Tahoma", 0, 14));

		jLabel5.setBackground(new java.awt.Color(255, 255, 255));
		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS Login screen2.png"))); // NOI18N

		loginJBtn.setBackground(new java.awt.Color(255, 255, 255));
		loginJBtn.setFont(new java.awt.Font("Gulim", 1, 12));
		loginJBtn.setText("Logon");
		loginJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				loginJBtnActionPerformed(evt);
			}
		});

		forgotPasswordJL.setFont(new java.awt.Font("Gulim", 1, 11));
		forgotPasswordJL.setForeground(new java.awt.Color(102, 102, 102));
		forgotPasswordJL.setText("Forgot password?");
		forgotPasswordJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				forgotPasswordJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(
				jPanel13);
		jPanel13.setLayout(jPanel13Layout);
		jPanel13Layout
				.setHorizontalGroup(jPanel13Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel13Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(jLabel5)
										.addGap(72, 72, 72)
										.addGroup(
												jPanel13Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																forgotPasswordJL)
														.addComponent(
																loginJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																113,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																passwordJPF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																240,
																Short.MAX_VALUE)
														.addComponent(jLabel10)
														.addComponent(jLabel9)
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																240,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(125, 125, 125)));
		jPanel13Layout
				.setVerticalGroup(jPanel13Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel13Layout
										.createSequentialGroup()
										.addGroup(
												jPanel13Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(jLabel5)
														.addGroup(
																jPanel13Layout
																		.createSequentialGroup()
																		.addGap(
																				72,
																				72,
																				72)
																		.addComponent(
																				jLabel9)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				userNameJTF,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				39,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jLabel10)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				passwordJPF,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				38,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				loginJBtn,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				36,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				forgotPasswordJL)))
										.addContainerGap(41, Short.MAX_VALUE)));

		jPanel13Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { passwordJPF, userNameJTF });

		javax.swing.GroupLayout loginJIFLayout = new javax.swing.GroupLayout(
				loginJIF.getContentPane());
		loginJIF.getContentPane().setLayout(loginJIFLayout);
		loginJIFLayout.setHorizontalGroup(loginJIFLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel13, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		loginJIFLayout.setVerticalGroup(loginJIFLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		loginJIF.setBounds(530, 30, 700, 400);
		jDesktopPane1.add(loginJIF, javax.swing.JLayeredPane.DEFAULT_LAYER);
		truckJL.setBounds(680, 410, -1, 100);
		jDesktopPane1.add(truckJL, javax.swing.JLayeredPane.DEFAULT_LAYER);

		fileMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/file small.png"))); // NOI18N
		fileMenu1.setText("File");
		fileMenu1.setFont(new java.awt.Font("Ebrima", 1, 12));

		exitMenuItem1.setFont(new java.awt.Font("Gulim", 0, 11));
		exitMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS delete role small.png"))); // NOI18N
		exitMenuItem1.setText("Log-out");
		exitMenuItem1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				exitMenuItemActionPerformed(evt);
			}
		});
		fileMenu1.add(exitMenuItem1);

		menuBar1.add(fileMenu1);

		helpMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS dispensing point small.png"))); // NOI18N
		helpMenu1.setText("Help");
		helpMenu1.setFont(new java.awt.Font("Ebrima", 1, 12));

		aboutMenuItem1.setFont(new java.awt.Font("Ebrima", 0, 12));
		aboutMenuItem1.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS dashboard small icon.png"))); // NOI18N
		aboutMenuItem1.setText("About");
		aboutMenuItem1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				aboutMenuItemActionPerformed(evt);
			}
		});
		helpMenu1.add(aboutMenuItem1);

		userGuideJMI1.setFont(new java.awt.Font("Ebrima", 0, 12));
		userGuideJMI1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Reset password.png"))); // NOI18N
		userGuideJMI1.setText("User Guide");
		userGuideJMI1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				userGuideJMIActionPerformed(evt);
			}
		});
		helpMenu1.add(userGuideJMI1);
		helpMenu1.add(jSeparator14);

		jMenuItem1.setFont(new java.awt.Font("Ebrima", 0, 12));
		jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/online small.png"))); // NOI18N
		jMenuItem1.setText("Online Help");
		jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jMenuItem1ActionPerformed(evt);
			}
		});
		helpMenu1.add(jMenuItem1);

		menuBar1.add(helpMenu1);

		setJMenuBar(menuBar1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 1147,
				Short.MAX_VALUE).addGroup(
				layout.createSequentialGroup().addGap(10, 10, 10).addComponent(
						jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE,
						1137, Short.MAX_VALUE)).addGroup(
				layout.createSequentialGroup().addGap(437, 437, 437)
						.addComponent(jProgressBar1,
								javax.swing.GroupLayout.PREFERRED_SIZE, 113,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(597, Short.MAX_VALUE)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												jToolBar1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												25,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jDesktopPane1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												567, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jProgressBar1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void changeLocationJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		AppJFrame.glassPane.activate(null);
		new SelectLocationJD(
				javax.swing.JOptionPane.getFrameForComponent(null), true);

		AppJFrame.glassPane.deactivate();
	}

	private void dashboardButtonActionPerformed(java.awt.event.ActionEvent evt) {

		AppJFrame.glassPane.activate(null);

		javax.swing.JDialog dashboard = new DashboardJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);
		dashboard.setLocationRelativeTo(null);

		dashboard.setVisible(true);
		AppJFrame.glassPane.deactivate();
	}

	private void forgotPasswordJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		javax.swing.JOptionPane
				.showMessageDialog(this,
						"Please see your local system administrstor to reset your password.");
	}

	private void connectionStatusJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		// new AppIconsJIF(this.jDesktopPane1,this.user, this.userRightsList);

	}

	private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {

		try {
			URI url = new URI("http://www.oribicom.com");

			java.awt.Desktop.getDesktop().browse(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// static JFrame frame;

	// main
	private void userGuideJMIActionPerformed(java.awt.event.ActionEvent evt) {
	}

	private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
		// GEN-FIRST:event_exitMenuItemActionPerformed

		// log off
		// int ok = publicMethods
		// .confirmDialog("Are you sure you want to log out ?");

		int ok = javax.swing.JOptionPane.showInternalConfirmDialog(
				this.desktop, "Are you sure you want to log out ?");

		if (ok == 0) {

			javax.swing.JInternalFrame object[] = AppJFrame.desktop
					.getAllFrames();

			if (object.length > 5) {

				javax.swing.JOptionPane
						.showInternalMessageDialog(AppJFrame.desktop,
								"Please close all Application Windows\nbefore you can log-off");
			} else {

				userNameJL.setText("");
				//AppJFrame.userDisplayName = null;
				disableAll();

				// remove AppJFrame on logout
				this.jDesktopPane1.removeAll();
				this.jDesktopPane1.repaint();

				AppJFrame.desktop.setBackground(Color.gray);
				//this.glassPane.activate(null);

				jDesktopPane1.add(loginJIF,
						javax.swing.JLayeredPane.DEFAULT_LAYER);
				centerJIF(loginJIF);

				//loginJIF.setVisible(true);

				passwordJPF.setText("");

				jToolBar1.setVisible(false);

			}

		}

	}

	private void formWindowClosing(java.awt.event.WindowEvent evt) {

		AppJFrame.glassPane.activate(null);

		// check if the timer is running
		javax.swing.JInternalFrame frames[] = AppJFrame.desktop.getAllFrames();

		int ok = javax.swing.JOptionPane.showConfirmDialog(this,
				"Are you sure you want to exit application", "Quit", 2, 1);

		for (int i = 0; i < frames.length; i++) {
			// Get internal frame's title
			String title = frames[i].getTitle();

			if (title.equalsIgnoreCase("Title example")) {
				i = frames.length;
				javax.swing.JOptionPane.showMessageDialog(this,
						"..... before you can exit ");

			} else {

				AppJFrame.glassPane.activate(null);

				if (ok == 0) {

					this.dispose();

					System.exit(0);
				} else {

					AppJFrame.glassPane.deactivate();
				}
			}

		}

		AppJFrame.glassPane.deactivate();
	}

	private void loginJBtnActionPerformed(java.awt.event.ActionEvent evt) {

		// TODO add your handling code here:
		userName = userNameJTF.getText().replaceAll("'", "`").trim();

		password = new String(passwordJPF.getPassword());

		login(userName, password);

	}

	private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		AppJFrame.glassPane.activate(null);

		new AboutJDialog(javax.swing.JOptionPane.getFrameForComponent(this),
				true);
		AppJFrame.glassPane.deactivate();

		//aboutJIF.setVisible(true);
	}

	// populate table

	protected boolean isLoaded(String FormTitle) {

		javax.swing.JInternalFrame Form[] = AppJFrame.desktop.getAllFrames();
		for (int i = 0; i < Form.length; i++) {
			if (Form[i].getTitle().equalsIgnoreCase(FormTitle)) {
				Form[i].show();
				try {
					Form[i].setIcon(false);
					Form[i].setSelected(true);
				} catch (PropertyVetoException e) {
				}
				return true;
			}
		}
		return false;

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		//Logger myLogger = Logger.getLogger("Test");
		//myLogger.isInfoEnabled();

		//Appender appender = new Appender();
		//myLogger.addAppender(newAppender)

		try {

			javax.swing.UIManager
					.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");

		} catch (Exception ex) {
			ex.printStackTrace();

		}
		// LiquidLookAndFeel.setToolbarFlattedButtons(true);
		// or if you want to use Apple's Panther window decoration
		com.birosoft.liquid.LiquidLookAndFeel.setLiquidDecorations(true,
				"panther");
		// com.birosoft.liquid.LiquidLookAndFeel.setLiquidDecorations(false,"stipples"
		// );
		com.birosoft.liquid.LiquidLookAndFeel.setStipples(false);
		com.birosoft.liquid.LiquidLookAndFeel.setPanelTransparency(false);
		com.birosoft.liquid.LiquidLookAndFeel.setShowTableGrids(true);
		com.birosoft.liquid.LiquidLookAndFeel.setToolbarFlattedButtons(true);
		com.birosoft.liquid.LiquidLookAndFeel.setDefaultRowBackgroundMode(true);

		com.jidesoft.utils.Lm.verifyLicense("Joe Banda", "OpenLMIS",
				"ETAcp2S4Q.dTxOWhjQ1aoCDjZ:Owgrn2");

		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				frame = new AppJFrame();
				frame.setVisible(true);
			}
		});

	}

	private void login(String userName, String password) {

		// 

		this.userName = userName;
		this.password = password;

		// do user user Authentication in Login.java
		this.user = new Login().doLogin(userName, password);

		if (this.user != null) {

			// get user role given user id
			this.userRightsList = new LinkedList();

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				// Role role = session.selectOne("selectByRoles", 1);

				List<Role_rights> roleRightList = session.selectList(
						"selectRightByUserId", this.user.getId());

				for (Role_rights roleRights : roleRightList) {

					// System.out.println("Role "+ roleRights.getRoleid());
					// System.out.print(" "+ roleRights.getRightname());
					userRightsList.add(roleRights.getRightname());

				}

			} finally {
				session.close();
			}

			if (!user.getFirstname().equalsIgnoreCase("")) {

				this.userNameJL.setText(user.getFirstname() + " "
						+ user.getLastname());

				userLoggedIn = user.getFirstname() + " " + user.getLastname();

				loginJIF.setVisible(false);

				jToolBar1.setVisible(true);

				AppJFrame.desktop.setBackground(Color.white);

				// create appicons JPanel

				new AppIconsJIF(this.jDesktopPane1, this.user,
						this.userRightsList);

				//show the dashboard

				AppJFrame.glassPane.activate(null);

				//javax.swing.JOptionPane.showMessageDialog(this, System.getProperty("dbhost"));

				javax.swing.JDialog dashboard = new DashboardJD(
						javax.swing.JOptionPane.getFrameForComponent(this),
						true);
				//	dashboard.setLocationRelativeTo(null);

				dashboard.setVisible(true);
				AppJFrame.glassPane.deactivate();

				//end show dash board

			}

		} else {

			javax.swing.JOptionPane.showInternalMessageDialog(jDesktopPane1,
					"***Invalid User Name or Password***", "Log-in error", 2);

			// clear the saved password
			passwordJPF.setText("");

		}

	}

	public void disableAll() {

		fileMenu.setEnabled(false);
		helpMenu.setEnabled(false);

		fileMenu.setEnabled(false);

		helpMenu.setEnabled(false);

	}

	private void runStartup() {

		initComponents();

		//this.loginJIF.setContentPane(new loginJPanel());

		// show connection status
		if (System.getProperty("dbdriver").equalsIgnoreCase(
				"org.postgresql.Driver")) {

			connectionStatusJL.setText("Connected to server");
			connectionStatusJL.setIcon(new javax.swing.ImageIcon(getClass()
					.getResource("/images/greenled.png")));
		}

		// set the glassPane to disable background
		glassPane = new MyDisabledGlassPane();
		JRootPane root = SwingUtilities.getRootPane(this);
		root.setGlassPane(glassPane);
		// end set glassPane

		desktop = jDesktopPane1;

		// dont show the tool bar
		jToolBar1.setVisible(false);

		// appJP.setVisible(false);

		this.setSize(screen.width, screen.height - 40);
		this.setLocationRelativeTo(null);

		//read and display the work location

		if (System.getProperty("dp_name") != null) {

			locationJL.setText("Location : " + System.getProperty("dp_name"));
		}

		truckJL.setLocation(this.desktop.getWidth() - this.truckJL.getWidth(),
				this.desktop.getHeight() - this.truckJL.getHeight());

		double sceenX = this.desktop.getWidth();
		double sceenY = this.desktop.getHeight();

		System.setProperty("screenx", Double.toString(sceenX));
		System.setProperty("screeny", Double.toString(sceenY));

		centerJIF(this.loginJIF);

		AppJFrame.desktop.setBackground(Color.gray);
		AppJFrame.desktop.setSelectedFrame(this.loginJIF);
		this.userNameJTF.requestFocus();

		this.loginJIF.setFrameIcon(new javax.swing.ImageIcon(getClass()
				.getResource("")));

		this.loginJIF.getRootPane().setDefaultButton(loginJBtn);

		appIcon = new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage();

	}

	public class AnimationListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

		}

	}

	public void centerJIF(JInternalFrame jif) {
		//Dimension desktopSize = this.desktop.getSize();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension jInternalFrameSize = jif.getSize();
		//int width = (publicMethods.screenYbig() + jInternalFrameSize.width) / 1;
		//int height = (publicMethods.screenXbig() + jInternalFrameSize.height) / 1;

		//int width = (desktopSize.width - jInternalFrameSize.width) / 2;
		//int height = (desktopSize.height - jInternalFrameSize.height) /2;

		int width = (screen.width - jInternalFrameSize.width) / 2;
		int height = ((screen.height - jInternalFrameSize.height) / 2)
				- (jInternalFrameSize.height / 4);

		//int width =( (screen.width/2) - (jInternalFrameSize.width /2))* 2;
		//int height = ( (screen.height/2) - (jInternalFrameSize.height /2))* 2;
		jif.setLocation(width, height);
		jif.setVisible(true);
	}

	public static void moveToFront(final JInternalFrame fr) {
		if (fr != null) {
			processOnSwingEventThread(new Runnable() {
				public void run() {
					fr.moveToFront();
					fr.setVisible(true);
					try {
						fr.setSelected(true);
						if (fr.isIcon()) {
							fr.setIcon(false);
						}
						fr.setSelected(true);
					} catch (PropertyVetoException ex) {

					}
					fr.requestFocus();
				}
			});
		}

	}

	public static void processOnSwingEventThread(Runnable todo) {
		processOnSwingEventThread(todo, false);
	}

	public static void processOnSwingEventThread(Runnable todo, boolean wait) {
		if (todo == null) {
			throw new IllegalArgumentException("Runnable == null");
		}

		if (wait) {
			if (SwingUtilities.isEventDispatchThread()) {
				todo.run();
			} else {
				try {
					SwingUtilities.invokeAndWait(todo);
				} catch (Exception ex) {
					throw new RuntimeException(ex);
				}
			}
		} else {
			if (SwingUtilities.isEventDispatchThread()) {
				todo.run();
			} else {
				SwingUtilities.invokeLater(todo);
			}
		}
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JMenu RptJMenu;
	private javax.swing.JInternalFrame aboutJIF;
	private javax.swing.JMenuItem aboutMenuItem;
	private javax.swing.JMenuItem aboutMenuItem1;
	private javax.swing.JMenuItem allStaffTodayJMI;
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JButton changeLocationJBtn;
	private javax.swing.JLabel connectionStatusJL;
	private javax.swing.JButton dashboardButton;
	private javax.swing.JMenuItem exitMenuItem;
	private javax.swing.JMenuItem exitMenuItem1;
	private javax.swing.JMenuItem expStatementJMI;
	private javax.swing.JMenu fileMenu;
	private javax.swing.JMenu fileMenu1;
	private javax.swing.JLabel forgotPasswordJL;
	private javax.swing.JMenu helpMenu;
	private javax.swing.JMenu helpMenu1;
	private javax.swing.JDesktopPane jDesktopPane1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel20;
	private javax.swing.JLabel jLabel21;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JMenuItem jMenuItem1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel13;
	private javax.swing.JPanel jPanel4;
	private static javax.swing.JProgressBar jProgressBar1;
	private javax.swing.JToolBar.Separator jSeparator1;
	private javax.swing.JToolBar.Separator jSeparator10;
	private javax.swing.JToolBar.Separator jSeparator11;
	private javax.swing.JToolBar.Separator jSeparator12;
	private javax.swing.JToolBar.Separator jSeparator13;
	private javax.swing.JSeparator jSeparator14;
	private javax.swing.JToolBar.Separator jSeparator15;
	private javax.swing.JToolBar.Separator jSeparator16;
	private javax.swing.JToolBar.Separator jSeparator17;
	private javax.swing.JToolBar.Separator jSeparator18;
	private javax.swing.JToolBar.Separator jSeparator19;
	private javax.swing.JSeparator jSeparator2;
	private javax.swing.JToolBar.Separator jSeparator20;
	private javax.swing.JToolBar.Separator jSeparator21;
	private javax.swing.JToolBar.Separator jSeparator22;
	private javax.swing.JToolBar.Separator jSeparator23;
	private javax.swing.JToolBar.Separator jSeparator24;
	private javax.swing.JToolBar.Separator jSeparator25;
	private javax.swing.JToolBar.Separator jSeparator26;
	private javax.swing.JToolBar.Separator jSeparator27;
	private javax.swing.JToolBar.Separator jSeparator28;
	private javax.swing.JToolBar.Separator jSeparator29;
	private javax.swing.JToolBar.Separator jSeparator3;
	private javax.swing.JToolBar.Separator jSeparator30;
	private javax.swing.JToolBar.Separator jSeparator31;
	private javax.swing.JToolBar.Separator jSeparator32;
	private javax.swing.JToolBar.Separator jSeparator33;
	private javax.swing.JToolBar.Separator jSeparator34;
	private javax.swing.JToolBar.Separator jSeparator35;
	private javax.swing.JToolBar.Separator jSeparator4;
	private javax.swing.JToolBar.Separator jSeparator5;
	private javax.swing.JToolBar.Separator jSeparator6;
	private javax.swing.JToolBar.Separator jSeparator7;
	private javax.swing.JToolBar.Separator jSeparator8;
	private javax.swing.JToolBar.Separator jSeparator9;
	private javax.swing.JToolBar jToolBar1;
	private javax.swing.JLabel locationJL;
	private javax.swing.JButton loginJBtn;
	private javax.swing.JInternalFrame loginJIF;
	private javax.swing.JMenuBar menuBar;
	private javax.swing.JMenuBar menuBar1;
	private javax.swing.JMenuItem myTimeTodayJMI;
	private javax.swing.JPasswordField passwordJPF;
	private javax.swing.JMenuItem statusRptJMI;
	private javax.swing.JLabel truckJL;
	private javax.swing.JMenuItem userGuideJMI;
	private javax.swing.JMenuItem userGuideJMI1;
	private javax.swing.JLabel userNameJL;
	private javax.swing.JTextField userNameJTF;
	// End of variables declaration//GEN-END:variables

}