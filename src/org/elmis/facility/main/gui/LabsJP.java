/*
 * ReportsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.elmis.facility.dashboard.reporting.EquipAvailabilityJd;
import org.elmis.facility.dashboard.reporting.EquipmentInformationJd;
import org.elmis.facility.dashboard.reporting.MachineConfigurationJD;

/**
 * 
 * @author __USER__
 */
public class LabsJP extends javax.swing.JPanel {

	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	/** Creates new form ReportsJP */
	public LabsJP() {
		initComponents();
		
		equipInfoLabel.setVisible(true);
		equipInfoLabel1.setVisible(true);
		//weeklyRptJL.setVisible(false);
		//staffproductivityJL.setVisible(false);

		// productivityJBtn.setVisible(false);

		//equipInfoLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(arg0, arg1, arg2, arg3, arg4, arg5)));
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel3 = new javax.swing.JPanel();
		equipInfoLabel = new javax.swing.JLabel();
		jPanel4 = new javax.swing.JPanel();
		equipInfoLabel1 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jPanel3.setBackground(new java.awt.Color(102, 102, 102));
		jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Monthly Test Numbers",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		equipInfoLabel.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS view r&r.png"))); // NOI18N
		equipInfoLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Test Numbers",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		equipInfoLabel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				equipInfoLabelMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel3Layout.createSequentialGroup().addContainerGap()
						.addComponent(equipInfoLabel,
								javax.swing.GroupLayout.PREFERRED_SIZE, 128,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(85, Short.MAX_VALUE)));
		jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel3Layout.createSequentialGroup().addComponent(
						equipInfoLabel).addContainerGap(69, Short.MAX_VALUE)));

		jPanel4.setBackground(new java.awt.Color(102, 102, 102));
		jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Facility Equipment",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		jPanel4.setFont(new java.awt.Font("Ebrima", 1, 12));

		equipInfoLabel1.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS view r&r.png"))); // NOI18N
		equipInfoLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Equipment Available",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		equipInfoLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				equipAvailabilityMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel4Layout.createSequentialGroup().addContainerGap()
						.addComponent(equipInfoLabel1,
								javax.swing.GroupLayout.PREFERRED_SIZE, 129,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(22, Short.MAX_VALUE)));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel4Layout.createSequentialGroup().addComponent(
						equipInfoLabel1).addContainerGap(69, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel3,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jPanel4,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addContainerGap()));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																false)
														.addComponent(
																jPanel4,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																jPanel3,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
	}// </editor-fold>
	//GEN-END:initComponents

	private void equipAvailabilityMouseClicked(java.awt.event.MouseEvent evt) {
		AppJFrame.glassPane.activate(null);
		/*EquipAvailabilityJd equipAvail = new EquipAvailabilityJd(
				javax.swing.JOptionPane.getFrameForComponent(this), true);*/
		MachineConfigurationJD machineConfig = new MachineConfigurationJD(javax.swing.JOptionPane.getFrameForComponent(this), true);
		machineConfig.pack();
		machineConfig.setVisible(true);
		AppJFrame.glassPane.deactivate();
	}

	private void equipInfoLabelMouseClicked(java.awt.event.MouseEvent evt) {
		AppJFrame.glassPane.activate(null);
		EquipmentInformationJd equipInfo = new EquipmentInformationJd(
				javax.swing.JOptionPane.getFrameForComponent(this), true);
		equipInfo.pack();
		equipInfo.setVisible(true);
		AppJFrame.glassPane.deactivate();
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel equipInfoLabel;
	private javax.swing.JLabel equipInfoLabel1;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	// End of variables declaration//GEN-END:variables

}