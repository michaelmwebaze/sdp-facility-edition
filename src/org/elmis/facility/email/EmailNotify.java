package org.elmis.facility.email;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.oribicom.tools.NetworkMode;

public class EmailNotify implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String feenote;
	private String invoice;
	private String quote;
	private String payment;
	private String newClient;
	private String newFile;
	private String fileClosed;
	private String clientDeleted;
	private String fileDeleted;
	private String newStaff;
	private String staffDeleted;
	private String staffAccessLevel;
	private String weeklyReport;
	private String monthlyReport;
	private String emailAddress;
	// private String[] mailingList ;
	private static List<String> mailingList;

	private EmailNotify emailNotify;

	// Constructors

	/** default constructor */
	public EmailNotify() {
	}

	/** full constructor */
	public EmailNotify(String id, String feenote, String invoice, String quote,
			String payment, String newClient, String newFile,
			String fileClosed, String clientDeleted, String fileDeleted,
			String newStaff, String staffDeleted, String staffAccessLevel,
			String weeklyReport, String monthlyReport, String emailAddress) {
		this.id = id;
		this.feenote = feenote;
		this.invoice = invoice;
		this.quote = quote;
		this.payment = payment;
		this.newClient = newClient;
		this.newFile = newFile;
		this.fileClosed = fileClosed;
		this.clientDeleted = clientDeleted;
		this.fileDeleted = fileDeleted;
		this.newStaff = newStaff;
		this.staffDeleted = staffDeleted;
		this.staffAccessLevel = staffAccessLevel;
		this.weeklyReport = weeklyReport;
		this.monthlyReport = monthlyReport;
		this.emailAddress = emailAddress;
	}

	// Property accessors

	//@Column(name = "id", length = 40)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//@Column(name = "feenote", length = 5)
	public String getFeenote() {
		return this.feenote;
	}

	public void setFeenote(String feenote) {
		this.feenote = feenote;
	}

	//@Column(name = "invoice", length = 5)
	public String getInvoice() {
		return this.invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	//@Column(name = "quote", length = 5)
	public String getQuote() {
		return this.quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	//@Column(name = "payment", length = 5)
	public String getPayment() {
		return this.payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	//@Column(name = "new_client", length = 5)
	public String getNewClient() {
		return this.newClient;
	}

	public void setNewClient(String newClient) {
		this.newClient = newClient;
	}

	//@Column(name = "new_file", length = 5)
	public String getNewFile() {
		return this.newFile;
	}

	public void setNewFile(String newFile) {
		this.newFile = newFile;
	}

	//@Column(name = "file_closed", length = 5)
	public String getFileClosed() {
		return this.fileClosed;
	}

	public void setFileClosed(String fileClosed) {
		this.fileClosed = fileClosed;
	}

	//@Column(name = "client_deleted", length = 5)
	public String getClientDeleted() {
		return this.clientDeleted;
	}

	public void setClientDeleted(String clientDeleted) {
		this.clientDeleted = clientDeleted;
	}

	//@Column(name = "file_deleted", length = 5)
	public String getFileDeleted() {
		return this.fileDeleted;
	}

	public void setFileDeleted(String fileDeleted) {
		this.fileDeleted = fileDeleted;
	}

	//@Column(name = "new_staff", length = 5)
	public String getNewStaff() {
		return this.newStaff;
	}

	public void setNewStaff(String newStaff) {
		this.newStaff = newStaff;
	}

	//@Column(name = "staff_deleted", length = 5)
	public String getStaffDeleted() {
		return this.staffDeleted;
	}

	public void setStaffDeleted(String staffDeleted) {
		this.staffDeleted = staffDeleted;
	}

	//@Column(name = "staff_access_level", length = 5)
	public String getStaffAccessLevel() {
		return this.staffAccessLevel;
	}

	public void setStaffAccessLevel(String staffAccessLevel) {
		this.staffAccessLevel = staffAccessLevel;
	}

	//@Column(name = "weekly_report", length = 5)
	public String getWeeklyReport() {
		return this.weeklyReport;
	}

	public void setWeeklyReport(String weeklyReport) {
		this.weeklyReport = weeklyReport;
	}

	//@Column(name = "monthly_report", length = 5)
	public String getMonthlyReport() {
		return this.monthlyReport;
	}

	public void setMonthlyReport(String monthlyReport) {
		this.monthlyReport = monthlyReport;
	}

	//@Column(name = "email_address", length = 300)
	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean saveSetting(EmailNotify emailNotify) {
		String sql = "insert into email_notify(" + "id," + "feenote,"
				+ "invoice ," + "quote ," + "payment," + "new_client,"
				+ "new_file," + "file_closed," + "client_deleted,"
				+ "file_deleted," + "new_staff," + "staff_deleted,"
				+ "staff_access_level," + "weekly_report," + "monthly_report,"
				+ "email_address" + ")" + "values( '"
				+ emailNotify.getId()
				+ "','"
				+ emailNotify.getFeenote()
				+ "','"
				+ emailNotify.getInvoice()
				+ "','"
				+ emailNotify.getQuote()
				+ "','"
				+ emailNotify.getPayment()
				+ "','"
				+ emailNotify.getNewClient()
				+ "','"
				+ emailNotify.getNewFile()
				+ "','"
				+ emailNotify.getFileClosed()
				+ "','"
				+ emailNotify.getClientDeleted()
				+ "','"
				+ emailNotify.getFileDeleted()
				+ "','"
				+ emailNotify.getNewStaff()
				+ "','"
				+ emailNotify.getStaffDeleted()
				+ "','"
				+ emailNotify.getStaffAccessLevel()
				+ "','"
				+ emailNotify.getWeeklyReport()
				+ "','"
				+ emailNotify.getMonthlyReport()
				+ "','"
				+ emailNotify.getEmailAddress() + "')";

		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {

			NetworkMode.c.createStatement().executeUpdate(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	public EmailNotify getSetting(String userId) {

		// EmailNotify emailNotify;// = new EmailNotify();

		String query = "select * from email_notify where id = '" + userId + "'";

		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {
			ResultSet rs = NetworkMode.c.createStatement().executeQuery(query);

			while (rs.next()) {

				emailNotify = new EmailNotify();

				emailNotify.setId(rs.getString("id"));
				emailNotify.setFeenote(rs.getString("feenote"));
				emailNotify.setInvoice(rs.getString("invoice"));
				emailNotify.setQuote(rs.getString("quote"));
				emailNotify.setPayment(rs.getString("payment"));
				emailNotify.setNewClient(rs.getString("new_client"));
				emailNotify.setNewFile(rs.getString("new_file"));
				emailNotify.setFileClosed(rs.getString("file_closed"));
				emailNotify.setClientDeleted(rs.getString("client_deleted"));
				emailNotify.setFileDeleted(rs.getString("file_deleted"));
				emailNotify.setNewStaff(rs.getString("new_staff"));
				emailNotify.setStaffDeleted(rs.getString("staff_deleted"));
				emailNotify.setStaffAccessLevel(rs
						.getString("staff_access_level"));
				emailNotify.setWeeklyReport(rs.getString("weekly_report"));
				emailNotify.setMonthlyReport(rs.getString("monthly_report"));
				emailNotify.setEmailAddress(rs.getString("email_address"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emailNotify;
	}

	public boolean updateSetting(EmailNotify emailNotify) {
		String updateSql = "update email_notify set feenote ='"
				+ emailNotify.getFeenote() + "'," + "invoice= '"
				+ emailNotify.getInvoice() + "'," + "quote= '"
				+ emailNotify.getQuote() + "'," + "payment= '"
				+ emailNotify.getPayment() + "'," + "new_client= '"
				+ emailNotify.getNewClient() + "'," + "new_file= '"
				+ emailNotify.getNewFile() + "'," + "file_closed= '"
				+ emailNotify.getFileClosed() + "'," + "client_deleted= '"
				+ emailNotify.getClientDeleted() + "'," + "file_deleted= '"
				+ emailNotify.getFileDeleted() + "'," + "new_staff= '"
				+ emailNotify.getNewStaff() + "'," + "staff_access_level= '"
				+ emailNotify.getStaffAccessLevel() + "'," + "weekly_report= '"
				+ emailNotify.getWeeklyReport() + "'," + "monthly_report= '"
				+ emailNotify.getMonthlyReport() + "'," + "staff_deleted= '"
				+ emailNotify.getStaffDeleted() + "'" + " where id = '"
				+ emailNotify.getId() + "'";
			

		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {

			NetworkMode.c.createStatement().executeUpdate(updateSql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	// get email mailing list

	public List getMailingList(String query) {

		// EmailNotify emailNotify;// = new EmailNotify();

		// String query = "select * from email_notify where id = '"+ userId+"'";

		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {
			ResultSet rs = NetworkMode.c.createStatement().executeQuery(query);

			/*
			 * int size =0; if (rs != null) { rs.beforeFirst(); rs.last(); size
			 * = rs.getRow(); }
			 * 
			 * mailingList = new String[size] ;
			 */
			mailingList = new LinkedList();
			while (rs.next()) {

				emailNotify = new EmailNotify();

				emailNotify.setId(rs.getString("id"));
				emailNotify.setFeenote(rs.getString("feenote"));
				emailNotify.setInvoice(rs.getString("invoice"));
				emailNotify.setQuote(rs.getString("quote"));
				emailNotify.setPayment(rs.getString("payment"));
				emailNotify.setNewClient(rs.getString("new_client"));
				emailNotify.setNewFile(rs.getString("new_file"));
				emailNotify.setFileClosed(rs.getString("file_closed"));
				emailNotify.setClientDeleted(rs.getString("client_deleted"));
				emailNotify.setFileDeleted(rs.getString("file_deleted"));
				emailNotify.setNewStaff(rs.getString("new_staff"));
				emailNotify.setStaffDeleted(rs.getString("staff_deleted"));
				emailNotify.setStaffAccessLevel(rs
						.getString("staff_access_level"));
				emailNotify.setWeeklyReport(rs.getString("weekly_report"));
				emailNotify.setMonthlyReport(rs.getString("monthly_report"));
				emailNotify.setEmailAddress(rs.getString("email_address"));

				mailingList.add(rs.getString("email_address"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mailingList;
	}

}