package org.elmis.facility.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

/**
 * ElmisProperties.java
 * Purpose: Loads Postgres specific property file into the system
 * @author Michael Mwebaze
 * @version 1.0
 */
public class ElmisProperties {
	/**
	 * 
	 * @return System and Postgres related properties set in the ./Programmproperties.properties file
	 */
	public static Properties getElmisProperties()
	{
		Properties props = null;
		try(FileInputStream fis = new FileInputStream("Programmproperties.properties")) {
			props = new Properties(System.getProperties());
			props.load(fis);
			System.setProperties(props);
			return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}

}
