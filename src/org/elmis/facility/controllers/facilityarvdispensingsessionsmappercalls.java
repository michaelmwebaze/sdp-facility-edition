package org.elmis.facility.controllers;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.Elmis_Dar_TransactionsMapper;
import org.elmis.facility.domain.dao.SP_SystemcalculatedproductsbalanceMapper;
import org.elmis.facility.domain.dao.artdispenseProductQtyMapper;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Operators;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Geographic_Zones;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.network.MyBatisConnectionFactory;
public class facilityarvdispensingsessionsmappercalls {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public facilityarvdispensingsessionsmappercalls(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	

	@SuppressWarnings("unchecked")
	public Facility getFacility(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			 Facility fac = session.selectOne("getFacility");
			 //String operator = fac.
			// System.out.println(fac.getCode().toString() + "FacilityCode Noted");
			return fac;
		} finally {
			session.close();
		}
	}

	
	@SuppressWarnings("unchecked")
		
	public Facility_Types selectAllwithTypes(Facility facility){

		SqlSession session = sqlSessionFactory.openSession();
		String facCode = (facility.getCode()).trim();
		try {
			Facility_Types facttypes = session.selectOne("getFacilityTypes",facCode);
			
			
			return facttypes;
		} finally {
			session.close();
		}
	}

	
	
	
	@SuppressWarnings("unchecked")
	public Facility_Operators selectAllwithOps(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Facility_Operators facOps = session.selectOne("getFacilityOps");
			
			
			return facOps;
		} finally {
			session.close();
		}
	}
	public  ProductQty
	doselectqtyarvdispense (String pcode) {
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
     ProductQty prodqtyobj = mapper.getcurrentarvproductqty(pcode);  
     return prodqtyobj;
     }	
	
	
	public  List<ProductQty>doselectProductQtydispense (String pcode) {
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
     List<ProductQty> prodqtyobj = mapper.getcurrentListproductqty(pcode);  
     return prodqtyobj;
     }	
	
	
	public  
	void doupdatearvdispenseqty (String prodcode,Integer prodqty) {
		
		try{
			System.out.println(prodcode + "qty params" + prodqty);
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updateproductqty(prodcode,prodqty); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			
		}
         
       
     }	
	
	public  
	void doupdatearvdispenseqtyless (String prodcode,Integer prodqty,String location, Integer soh) {
		
		try{
			System.out.println(prodcode + "qty params" + prodqty);
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updatedispenaryproductqtyless(prodcode,prodqty,location,soh); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			
		}
         
       
     }	
	
	
	
	
		
	//
	public  List<Elmis_Dar_Transactions>
	doselectsumDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumARVdar(startdate,enddate);  
     return listsumdar;
     }	
	
	public  List<Elmis_Dar_Transactions>
	doselectsumsingledoseDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumsingledoseARVdar(startdate,enddate);  
     return listsumdar;
     }	
	public  List<Elmis_Dar_Transactions>
	doselectsumliquidpowderDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumliquidpowderdoseARVdar(startdate,enddate);  
     return listsumdar;
     }	
	public  List<Elmis_Dar_Transactions>
	doselectsumCotimoxazoleDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumcotrimoxazoledoseARVdar(startdate,enddate);  
     return listsumdar;
     }	
	
	public  List<Elmis_Dar_Transactions>
	doselectpatientarvDispensationparameter (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listpatientarvdar = mapper.selectsumcotrimoxazoledoseARVdar(startdate,enddate);  
     return listpatientarvdar;
     }	
	
	
	public  List<Elmis_Dar_Transactions>
	doselectpatientarvDispensation (String myART) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listpatientarvdar = mapper.selectARVpatientdartransactions(myART);  
     return listpatientarvdar;
     }	
		
	
	
	
	public Elmis_Patient  selectARTnumber(String art){

        SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Elmis_Patient patientdetail = session.selectOne("selectByARTnumber",art);
						
			return patientdetail;
		} finally {
			session.close();
		}
	}
	
	public  void  insertdispensedProducts(Elmis_Dar_Transactions stock){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_Dar_TransactionsMapper.insert",stock);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	public  void  Registerpatient(Elmis_Patient patient){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_PatientMapper.insert",patient);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
			

	public  List<Elmis_Dar_Transactions>
	dogetcurrentARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdar(startdate,enddate);  
     return listdar;
     }
	
	
	
	public  List<Elmis_Dar_Transactions>
	dosingledoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdarsingledose(startdate,enddate);  
     return listdar;
     }
	
	
	public  List<Elmis_Dar_Transactions>
	doliquidpowderdoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdarliquidpowderdose(startdate,enddate);  
     return listdar;
     }
	
	public  List<Elmis_Dar_Transactions>
	docotrimoxazoledoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdarcotrimoxazoledose(startdate,enddate);  
     return listdar;
     }
	
	
	public  List<Elmis_Dar_Transactions>
	dogetcurrentAlldoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectAllARVdosedarproducts(startdate,enddate);  
     return listdar;
     }
		
	/**
	 * Returns the list of all Geo Zone name from the database.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Geographic_Zones selectAllwithGeozones(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Geographic_Zones geozone = session.selectOne("getAllGeoZones");
			
			
			return geozone;
		} finally {
			session.close();
		}
	}	
	
	
	
	
	
}
