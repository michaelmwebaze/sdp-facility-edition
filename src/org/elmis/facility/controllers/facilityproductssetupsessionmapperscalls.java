package org.elmis.facility.controllers;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.SP_Program_Facility_ApprovedProductsMapper;
import org.elmis.facility.domain.dao.SP_SystemcalculatedproductsbalanceMapper;
import org.elmis.facility.domain.dao.artdispenseProductQtyMapper;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.Store_Physical_Count;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.network.MyBatisConnectionFactory;


public class facilityproductssetupsessionmapperscalls {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public facilityproductssetupsessionmapperscalls(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
		
   	
/*	public List<VW_Program_Facility_ApprovedProducts> getFacilityApprovedProducts(){

		SqlSession session = sqlSessionFactory.openSession();
		
		//FacilitysccProductsMapper fpMapper = session.getMapper(FacilitysccProductsMapper.class);
		
		try {
			  
			List<VW_Program_Facility_ApprovedProducts> fplist = session.selectList("getcurrentFacilityApprovedProducts");
			return fplist;
		} finally {
			session.close();
		}
	}*/
	
	public List<Programs> dogetselectedprogram(int programid){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Programs> proglist = session.selectList("getselectedprogram",programid);
						
			return proglist;
		} finally {
			session.close();
		}
	}
	
	
	public List<Programs> selectAllPrograms(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Programs> proglist = session.selectList("getFacilityProgs");
			
			
			return proglist;
		} finally {
			session.close();
		}
	}
	
/********************************************************************************
 * get the product from products list 
 * 	
 */
	
	@SuppressWarnings("unchecked")
	
	public VW_Program_Facility_ApprovedProducts barcodeselectProduct(String  barcode){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			VW_Program_Facility_ApprovedProducts   barcodeProduct = session.selectOne("getByProductBarcode",barcode);
			
			
			return barcodeProduct;
		} finally {
			session.close();
		}
	}
	
	
	public Integer barcodeselectProductQty(String  code){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Integer   ProductQty = session.selectOne("org.elmis.facility.domain.dao.SP_Program_Facility_ApprovedProductsMapper.selectProductQty",code);
			
			
			return ProductQty;
		} finally {
			session.close();
		}
	}
	
	
	
	public List<VW_Program_Facility_ApprovedProducts> quickfiltersearchselectProduct(String  barcode){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<VW_Program_Facility_ApprovedProducts> barcodeProductList = session.selectList("selectProductBySearchTerm",barcode);
			
			
			return barcodeProductList;
		} finally {
			session.close();
		}
	}
	
	
/*************************************************
 * method returns the list of loses_adjustment types defined  in the system
 * Calls mybatis mapper method selectAllAdjustments
 * 	
 *************************************************/
	
	public List<Losses_Adjustments_Types> selectAllAdjustments(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Losses_Adjustments_Types> Adjustmentlist = session.selectList("selectAllAdjustments");
			
			return Adjustmentlist;
		} finally {
			session.close();
		}
	}
/***********************************************
 * Method return list of current product store room balances	
 ***********************************************/
	public  List<VW_Systemcalculatedproductsbalance>
	dogetcurrentSystemcalculatedproductbalances (String valueA,java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		SP_SystemcalculatedproductsbalanceMapper mapper = session.getMapper(SP_SystemcalculatedproductsbalanceMapper.class);
     List<VW_Systemcalculatedproductsbalance> listbal = mapper.getsystemcalculatedProductbalance(valueA,startdate,enddate);  
     return listbal;
     }
	
	public  List<VW_Systemcalculatedproductsbalance>
	dogetcurrentSystemcalculatedproductbalancesbyprogram (String valueA,String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		SP_SystemcalculatedproductsbalanceMapper mapper = session.getMapper(SP_SystemcalculatedproductsbalanceMapper.class);
     List<VW_Systemcalculatedproductsbalance> listbal = mapper.getsystemcalculatedProductbalancebyprogram(valueA,valueB);  
     return listbal;
     }
	
	
	
	
	
	public  List<VW_Systemcalculatedproductsbalance>
	dogetcurrentSystemcalculatedproductbalanceshsqldb (String valueA,String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		SP_SystemcalculatedproductsbalanceMapper mapper = session.getMapper(SP_SystemcalculatedproductsbalanceMapper.class);
     List<VW_Systemcalculatedproductsbalance> listbal = mapper.getsystemcalculatedProductbalancehsqldb(valueA,valueB);  
     return listbal;
     }
	
	
/*****************************************************
 * Method returns list of facility Approved products
 * 	
 *****************************************************/
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetcurrentFacilityApprovedProducts (String valueA, String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getcurrentFacilityApprovedProducts(valueA,valueB);  
     return listFoo;
     }
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetcurrentFacilityApprovedProductshsqldb (String valueA, String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getcurrentFacilityApprovedProductshsqldb(valueA,valueB);  
     return listFoo;
     }
	
		
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetARVcurrentFacilityApprovedProducts (String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getARVcurrentFacilityApprovedProducts(valueA);  
     return listFoo;
     }
	
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetARVcurrentFacilityApprovedProductshsqldb (String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getARVcurrentFacilityApprovedProductshsqldb(valueA);  
     return listFoo;
     }
	
	
	public  
	void doupdatedispensaryqty (String prodcode,Integer prodqty,String loc,Integer soh) {
		
		try{
			System.out.println(prodcode + "qty params" + prodqty + "" + loc);
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updatedispenaryproductqty(prodcode,prodqty,loc,soh); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			
		}
         
       
     }	
	
	public  void  InsertshipmentProducts(Shipped_Line_Items shipment){
		
       SqlSession session = sqlSessionFactory.openSession();
	
		
		try {
			session.insert("org.elmis.facility.domain.dao.Shipped_Line_ItemsMapper.insert",shipment);
			session.commit();
			System.out.println("Check perhaps we succeeded!");
			
		} finally {
			session.close();
		}
		
	}
	
	
/***********************************************************
 * Method inserts new products received into database using 
 * mybatis mappers 
 * 	
 ***********************************************************/
	
	public  void  InsertstockcontrolcardProducts(Elmis_Stock_Control_Card stock){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_Stock_Control_CardMapper.insert",stock);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
		
	
	
	/***************************************************************************
	 * inserts the physical count object into database using mybatis mappers
	 * 
	 ***************************************************************************/
	
	public  void  Insertproductsphysicalcount(Store_Physical_Count pc){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Store_Physical_CountMapper.insert",pc);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	public  void  InsertproductQuantity(ProductQty  Pqty){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.artdispenseProductQtyMapper.insert",Pqty);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	
	
	

}
