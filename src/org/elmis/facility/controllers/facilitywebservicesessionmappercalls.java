package org.elmis.facility.controllers;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.SP_Program_Facility_ApprovedProductsMapper;
import org.elmis.facility.domain.dao.SP_SystemcalculatedproductsbalanceMapper;
import org.elmis.facility.domain.dao.Dosage_UnitsMapper;
import org.elmis.facility.domain.dao.Product_categoriesMapper;
import org.elmis.facility.domain.dao.FacilityMapper;
import org.elmis.facility.domain.dao.ProgramsMapper;
import org.elmis.facility.domain.dao.Program_productsMapper;
import org.elmis.facility.domain.dao.Facility_Approved_ProductsMapper;
import org.elmis.facility.domain.dao.Processing_PeriodsMapper;
import org.elmis.facility.domain.dao.Losses_Adjustments_TypesMapper;
import org.elmis.facility.domain.dao.Facility_TypesMapper;
import org.elmis.facility.domain.dao.ProductsMapper;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.Store_Physical_Count;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.network.MyBatisConnectionFactory;

public class facilitywebservicesessionmappercalls {

	@SuppressWarnings("unused")
	private SqlSessionFactory sqlSessionFactory; 
	
	public facilitywebservicesessionmappercalls(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
		
   	
}
