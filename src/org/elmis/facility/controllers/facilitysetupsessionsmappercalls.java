package org.elmis.facility.controllers;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Operators;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Geographic_Zones;
import org.elmis.facility.network.MyBatisConnectionFactory;
public class facilitysetupsessionsmappercalls {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public facilitysetupsessionsmappercalls(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	

	@SuppressWarnings("unchecked")
	public Facility getFacility(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			 Facility fac = session.selectOne("getFacility");
			 //String operator = fac.
			// System.out.println(fac.getCode().toString() + "FacilityCode Noted");
			return fac;
		} finally {
			session.close();
		}
	}

	/**
	 * Returns the list of all Contact instances from the database.
	 * @return the list of all Contact instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public Facility_Types selectAllwithTypes(Facility facility){

		SqlSession session = sqlSessionFactory.openSession();
		String facCode = (facility.getCode()).trim();
		try {
			Facility_Types facttypes = session.selectOne("getFacilityTypes",facCode);
			
			
			return facttypes;
		} finally {
			session.close();
		}
	}
	/**
	 * Returns the list of all Contact instances from the database.
	 * @return the list of all Contact instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public Facility_Operators selectAllwithOps(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Facility_Operators facOps = session.selectOne("getFacilityOps");
			
			
			return facOps;
		} finally {
			session.close();
		}
	}
	

	/**
	 * Returns the list of all Geo Zone name from the database.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Geographic_Zones selectAllwithGeozones(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Geographic_Zones geozone = session.selectOne("getAllGeoZones");
			
			
			return geozone;
		} finally {
			session.close();
		}
	}	
	
	
	
	
	
}
