/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json.beans;

/**
 * @author MMwebaze
 *
 */
public class LabProductReportItem extends ProductReportItem{
	
	private String unit;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
