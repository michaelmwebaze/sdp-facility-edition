/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json.beans;

import java.util.List;

/**
 * @author MMwebaze
 *
 */
public class ProductReportItem {

	private String productCode;
	private int beginningBalance;
	private int quantityReceived;
	private int quantityDispensed;
	private List<LossAdjustments> lossesAndAdjustments;
	private int stockInHand;
	private int newPatientCount;
	private int quantityRequested;
	private int stockOutDays;
	private String remarks;
	public int getStockOutDays() {
		return stockOutDays;
	}
	public void setStockOutDays(int stockOutDays) {
		this.stockOutDays = stockOutDays;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	private String reasonForRequestedQuantity;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getBeginningBalance() {
		return beginningBalance;
	}
	public void setBeginningBalance(int beginningBalance) {
		this.beginningBalance = beginningBalance;
	}
	public int getQuantityReceived() {
		return quantityReceived;
	}
	public void setQuantityReceived(int quantityReceived) {
		this.quantityReceived = quantityReceived;
	}
	public int getQuantityDispensed() {
		return quantityDispensed;
	}
	public void setQuantityDispensed(int quantityDispensed) {
		this.quantityDispensed = quantityDispensed;
	}
	public List<LossAdjustments> getLossesAndAdjustments() {
		return lossesAndAdjustments;
	}
	public void setLossesAndAdjustments(List<LossAdjustments> lossesAndAdjustments) {
		this.lossesAndAdjustments = lossesAndAdjustments;
	}
	public int getStockInHand() {
		return stockInHand;
	}
	public void setStockInHand(int stockInHand) {
		this.stockInHand = stockInHand;
	}
	public int getNewPatientCount() {
		return newPatientCount;
	}
	public void setNewPatientCount(int newPatientCount) {
		this.newPatientCount = newPatientCount;
	}
	public int getQuantityRequested() {
		return quantityRequested;
	}
	public void setQuantityRequested(int quantityRequested) {
		this.quantityRequested = quantityRequested;
	}
	public String getReasonForRequestedQuantity() {
		return reasonForRequestedQuantity;
	}
	public void setReasonForRequestedQuantity(String reasonForRequestedQuantity) {
		this.reasonForRequestedQuantity = reasonForRequestedQuantity;
	}
}
