/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json.beans;

/**
 * @author MMwebaze
 *
 */
public class EmlipProductReportItem extends ProductReportItem{
	
	private int stockOutDays;
	/*private String unit;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}*/

	public int getStockOutDays() {
		return stockOutDays;
	}

	public void setStockOutDays(int stockOutDays) {
		this.stockOutDays = stockOutDays;
	}
}
