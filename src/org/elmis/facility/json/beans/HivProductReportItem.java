/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json.beans;

/**
 * @author MMwebaze
 *
 */
public class HivProductReportItem extends ProductReportItem{
	
	private String typeOfTest;

	public String getTypeOfTest() {
		return typeOfTest;
	}

	public void setTypeOfTest(String typeOfTest) {
		this.typeOfTest = typeOfTest;
	}

}
