package org.elmis.facility.json;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.elmis.facility.json.beans.LossAdjustments;
import org.elmis.facility.json.beans.ProductReportItem;
import org.elmis.facility.json.beans.ReportBean;
import org.elmis.facility.json.beans.Type;
import org.elmis.facility.reporting.dao.ReportRequisitionJasonDao;
import org.elmis.facility.reporting.model.ReportRequisitionJason;
import org.elmis.facility.webservice.BaseFactory;

public class GenerateJason {

	public void generateArvJasonString(Date fromDate, Date toDate, ReportBean reportBean)
	{
		List<ReportRequisitionJason> arvList = new ReportRequisitionJasonDao().getCreatedArvData(fromDate, toDate);
		List<ProductReportItem> products = new ArrayList<>();

		for (ReportRequisitionJason r: arvList)
		{
			//System.out.println("R & R "+r);
			ProductReportItem arvProducts = new ProductReportItem();
			arvProducts.setProductCode(r.getProductCode().trim());
			arvProducts.setBeginningBalance(r.getBeginningBal());
			arvProducts.setQuantityReceived(r.getTotalReceived());
			arvProducts.setQuantityDispensed(r.getQtyDispensed());
			List<LossAdjustments> loss = new ArrayList<>();
			LossAdjustments ls = new LossAdjustments();
			Type type = new Type();
			type.setName("TRANSFER_OUT");
			ls.setType(type );
			ls.setQuantity(2);
			loss.add(ls);
			//loss.add(r.getLossAdjustment());
			//arvProducts.setLossesAndAdjustments(loss);
			arvProducts.setNewPatientCount(1);
			arvProducts.setStockInHand(0);
			arvProducts.setStockOutDays(0);
			arvProducts.setQuantityRequested(r.getQuantityRequested());
			arvProducts.setReasonForRequestedQuantity("");
			arvProducts.setRemarks("");
			products.add(arvProducts);
			reportBean.setProducts(products);

		}
		ObjectMapper mapper = new ObjectMapper();

		try {

			// convert user object to json string, and save to a file
			//mapper.writeValue(new File("d:\\user.json"),reportBean);

			// display to console

			String json = mapper.writeValueAsString(reportBean);
			System.out.println(json);
			
			BaseFactory.uploadJSON("requisitions", "R&R", json, Long.class);
		} catch (IOException e) {

			e.printStackTrace();

		}
		catch (Exception e)
		{
			System.out.println(">>> "+e.getLocalizedMessage());
		}
	}
}
