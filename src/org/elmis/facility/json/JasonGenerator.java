/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elmis.facility.json.beans.ArvProductReportItem;
import org.elmis.facility.json.beans.HivProductReportItem;
import org.elmis.facility.json.beans.LabProductReportItem;
import org.elmis.facility.json.beans.ReportBean;
import org.elmis.facility.json.beans.EmlipProductReportItem;
import org.elmis.facility.json.beans.ProductReportItem;
import org.elmis.facility.reporting.dao.ReportRequisitionJasonDao;
import org.elmis.facility.reporting.dao.ReportingPeriodRemarkDao;
import org.elmis.facility.reporting.model.ReportRequisitionJason;
import org.elmis.facility.reporting.model.ReportingPeriodRemark;
import org.elmis.facility.webservice.BaseFactory;

/**
 * @author MMwebaze
 *This class is responsible for generation of Jason Strings to be submitted to the Central eLMIS
 */
public class JasonGenerator{
	
	public void generateHivJasonString(Date frmDate, Date toDate, ReportBean reportBean)
	{
		ReportRequisitionJasonDao hivJasonDao = new ReportRequisitionJasonDao();
		List<ReportRequisitionJason> hivData = hivJasonDao.jasonHivData(frmDate, toDate, 3);
		if (hivJasonDao.isReportSubmitted(frmDate, toDate, 3))
			JOptionPane
			.showMessageDialog(
					null,
					"<html>HIV REPORT FOR THE PERIOD <br><font color=red> "+frmDate+" TO "+toDate+" </font>  <br>HAS ALREADY BEEN SUBMITTED</html>",
					"", JOptionPane.ERROR_MESSAGE);
		else
		{
			List<ProductReportItem> products = new ArrayList<>();
			Iterator<ReportRequisitionJason> hivDataIt = hivData.iterator();

			while(hivDataIt.hasNext())
			{
				ReportRequisitionJason rrJason = hivDataIt.next();
				HivProductReportItem hivProducts = new HivProductReportItem();
				hivProducts.setProductCode(rrJason.getProductCode().trim());
				hivProducts.setBeginningBalance(rrJason.getBeginningBal());
				hivProducts.setQuantityReceived(rrJason.getTotalReceived());
				hivProducts.setQuantityDispensed(rrJason.getQtyDispensed());
				List<Integer> loss = new ArrayList<>();
				loss.add(rrJason.getLossAdjustment());
				hivProducts.setLossesAndAdjustments(loss);
				hivProducts.setNewPatientCount(1);
				hivProducts.setQuantityRequested(rrJason.getQuantityRequested());
				hivProducts.setReasonForRequestedQuantity("");
				products.add(hivProducts);
				reportBean.setProducts(products);
			}
			ObjectMapper mapper = new ObjectMapper();

			try {

				// convert user object to json string, and save to a file
				//mapper.writeValue(new File("d:\\user.json"),reportBean);

				// display to console
				String json = mapper.writeValueAsString(reportBean);
				System.out.println(json);
				BaseFactory.uploadJSON("requisitions", "R&R", json, Long.class);
				

			} catch (IOException e) {

				e.printStackTrace();

			}
			catch (Exception e)
			{
				
			}
		}
	}
	public void generateArvJasonString(Date frmDate, Date toDate, ReportBean reportBean)
	{
		ReportRequisitionJasonDao arvJasonDao = new ReportRequisitionJasonDao();
			
		ReportingPeriodRemarkDao remarkDao = new ReportingPeriodRemarkDao();
		String arvRemark = remarkDao.getRemark(1, frmDate, toDate);
		
		if (arvJasonDao.isReportSubmitted(frmDate, toDate, 1))
			JOptionPane
			.showMessageDialog(
					null,
					"<html>ARV REPORT FOR THE PERIOD <br><font color=red> "+frmDate+" TO "+toDate+" </font>  <br>HAS ALREADY BEEN SUBMITTED</html>",
					"", JOptionPane.ERROR_MESSAGE);
		else
		{
			List<ReportRequisitionJason> arvData = arvJasonDao.jasonArvData(frmDate, toDate, 1);
			List<ProductReportItem> products = new ArrayList<>();
			Iterator<ReportRequisitionJason> arvDataIt = arvData.iterator();

			while(arvDataIt.hasNext())
			{
				ReportRequisitionJason rrJason = arvDataIt.next();
				ArvProductReportItem arvProducts = new ArvProductReportItem();
				arvProducts.setProductCode(rrJason.getProductCode().trim());
				arvProducts.setBeginningBalance(rrJason.getBeginningBal());
				arvProducts.setQuantityReceived(rrJason.getTotalReceived());
				arvProducts.setQuantityDispensed(rrJason.getQtyDispensed());
				List<Integer> loss = new ArrayList<>();
				loss.add(rrJason.getLossAdjustment());
				arvProducts.setLossesAndAdjustments(loss);
				arvProducts.setNewPatientCount(1);
				arvProducts.setQuantityRequested(rrJason.getQuantityRequested());
				arvProducts.setReasonForRequestedQuantity(arvRemark);
				products.add(arvProducts);
				reportBean.setProducts(products);
			}
			ObjectMapper mapper = new ObjectMapper();

			try {

				// convert user object to json string, and save to a file
				//mapper.writeValue(new File("d:\\user.json"),reportBean);

				// display to console
				
				String json = mapper.writeValueAsString(reportBean);
				//System.out.println(json);
				BaseFactory.uploadJSON("requisitions", "R&R", json, Long.class);
			} catch (IOException e) {

				e.printStackTrace();

			}
			catch (Exception e)
			{
				System.out.println("ERROR WITH UPLOAD "+e.getLocalizedMessage());
			}
		}
	}
	public void generateLabJasonString(Date frmDate, Date toDate, ReportBean reportBean)
	{
		ReportRequisitionJasonDao labJasonDao = new ReportRequisitionJasonDao();

		
		if (labJasonDao.isReportSubmitted(frmDate, toDate, 4))
			JOptionPane
			.showMessageDialog(
					null,
					"<html>LAB REPORT FOR THE PERIOD <br><font color=red> "+frmDate+" TO "+toDate+" </font>  <br>HAS ALREADY BEEN SUBMITTED</html>",
					"", JOptionPane.ERROR_MESSAGE);
		else
		{
			List<ReportRequisitionJason> labData = labJasonDao.jasonLabData(frmDate, toDate, 4);
			List<ProductReportItem> products = new ArrayList<>();
			Iterator<ReportRequisitionJason> labDataIt = labData.iterator();

			while(labDataIt.hasNext())
			{
				ReportRequisitionJason rrJason = labDataIt.next();
				LabProductReportItem labProducts = new LabProductReportItem();
				labProducts.setProductCode(rrJason.getProductCode().trim());
				labProducts.setBeginningBalance(rrJason.getBeginningBal());
				labProducts.setQuantityReceived(rrJason.getTotalReceived());
				labProducts.setQuantityDispensed(rrJason.getQtyDispensed());
				List<Integer> loss = new ArrayList<>();
				loss.add(rrJason.getLossAdjustment());
				labProducts.setLossesAndAdjustments(loss);
				labProducts.setNewPatientCount(1);
				labProducts.setQuantityRequested(rrJason.getQuantityRequested());
				labProducts.setReasonForRequestedQuantity("");
				products.add(labProducts);
				reportBean.setProducts(products);
			}
			ObjectMapper mapper = new ObjectMapper();

			try {

				// convert user object to json string, and save to a file
				//mapper.writeValue(new File("d:\\user.json"),reportBean);

				String json = mapper.writeValueAsString(reportBean);
				System.out.println(json);
				BaseFactory.uploadJSON("requisitions", "R&R", json, Long.class);

			} catch (IOException e) {

				e.printStackTrace();

			}
			catch (Exception e)
			{
				System.out.println("ERROR WITH UPLOAD OF LABS R & R "+e.getLocalizedMessage());
			}
		}
	}

	public void generateEmlipJasonString(Date frmDate, Date toDate, ReportBean reportBean)
	{
		ReportRequisitionJasonDao emlipJasonDao = new ReportRequisitionJasonDao();

		if (emlipJasonDao.isReportSubmitted(frmDate, toDate, 2))
			JOptionPane
			.showMessageDialog(
					null,
					"<html>EMLIP REPORT FOR THE PERIOD <br><font color=red> "+frmDate+" TO "+toDate+" </font>  <br>HAS ALREADY BEEN SUBMITTED</html>",
					"", JOptionPane.ERROR_MESSAGE);
		else
		{
			List<ReportRequisitionJason> emlipData = emlipJasonDao.jasonEmlipData(frmDate, toDate, 2);
			List<ProductReportItem> products = new ArrayList<>();
			Iterator<ReportRequisitionJason> emlipDataIt = emlipData.iterator();

			while(emlipDataIt.hasNext())
			{
				ReportRequisitionJason rrJason = emlipDataIt.next();
				EmlipProductReportItem emlipProducts = new EmlipProductReportItem();
				emlipProducts.setProductCode(rrJason.getProductCode().trim());
				emlipProducts.setBeginningBalance(rrJason.getBeginningBal());
				emlipProducts.setQuantityReceived(rrJason.getTotalReceived());
				emlipProducts.setQuantityDispensed(rrJason.getQtyDispensed());
				List<Integer> loss = new ArrayList<>();
				loss.add(rrJason.getLossAdjustment());
				emlipProducts.setLossesAndAdjustments(loss);
				emlipProducts.setStockInHand(0);
				emlipProducts.setStockOutDays(rrJason.getDaysStockedOut());
				emlipProducts.setNewPatientCount(1);
				emlipProducts.setQuantityRequested(rrJason.getQuantityRequested());
				emlipProducts.setReasonForRequestedQuantity("");
				products.add(emlipProducts);
				reportBean.setProducts(products);
			}
			ObjectMapper mapper = new ObjectMapper();

			try {

				// convert user object to json string, and save to a file
				//mapper.writeValue(new File("d:\\user.json"),reportBean);

				String json = mapper.writeValueAsString(reportBean);
				System.out.println(json);
				BaseFactory.uploadJSON("requisitions", "R&R", json, Long.class);

			} catch (IOException e) {

				e.printStackTrace();

			}
			catch (Exception e)
			{
				System.out.println("ERROR WITH UPLOAD OF EMLIP R & R "+e.getLocalizedMessage());
			}
		}

	}
}
