package org.elmis.facility.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.elmis.facility.utils.ElmisProperties;
/**
 * DatabaseSwitcher.java
 * Purpose: Used to switch from one database to another i.e POSTGRES to HSQL
 * @author Michael Mwebaze
 * @version 1.0
 */
//this is a change so i can push...
public class DatabaseSwitcher {

	private static final String USERNAME_HSQL = "dbuser";
	private static final String PASSWORD_HSQL = "dbpassword";
	private static final String USERNAME_POSTGRESQL = ElmisProperties.getElmisProperties().getProperty("dbuser");//postgres
	private static final String PASSWORD_POSTGRESQL = ElmisProperties.getElmisProperties().getProperty("dbpassword");//"p@ssw0rd";
	private static final String CONN_STRING_ELMIS_HSQL = "jdbc:hsqldb:database/explorecalifornia";
	//private static final String CONN_STRING_ELMIS_HSQL = "jdbc:hsqldb:database/elmis_facility";
	private static final String CONN_STRING_ELMIS_POSTGRES = ElmisProperties.getElmisProperties().getProperty("dburl");//"jdbc:postgresql://elmis1:5432/postgres";

	/**
	 * 
	 * @return true if a connection to HSQL database is available otherwise it will return false
	 */
	public static boolean switchToHsql(){

		//Class.forName("org.hsqldb.jdbcDriver");

		try (
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_HSQL, USERNAME_HSQL, PASSWORD_HSQL
						)){

			System.out.println("Connected to HSQLDB ELMIS");
			return true;

		} catch (SQLException e) {
			System.err.println("ERROR HSQL "+e.getMessage());
		}
		return false;
	}
	/**
	 * 
	 * @return true if connection to POSTGRES database is available otherwise it will return false
	 */
	public static boolean switchToPostgres()
	{
		
		try(
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_POSTGRES, USERNAME_POSTGRESQL, PASSWORD_POSTGRESQL
						)){

			System.out.println("Connected to database elmis db on postgres");
			return true;
		}
		catch(SQLException e)
		{
			System.out.println("Error postgress: "+e.getMessage());
		}
		return false;
	}

}
