package org.elmis.facility.connections;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

import javax.swing.JOptionPane;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.utils.ElmisProperties;
/**
 * MyBatisConnectionFactory.java
 * Purpose: Manages all connection resources (Opening & closing) database(s). 
 * @author Michael Mwebaze
 *@version 1.0
 */
public class MyBatisConnectionFactory {

	private static SqlSessionFactory sqlSessionFactory;

	static {
		try {

			String resource = null;
			Reader reader = null;

			/*if (ElmisProperties.getElmisProperties().getProperty("clientType").equals("Satellite"))//checks if facility is a Satelite and connects to HSQL Database
			{
				resource = "hsql-mybatis-config.xml";
				reader = Resources.getResourceAsReader(resource);

				if (sqlSessionFactory == null) {
					sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
				}
			}
			else
			{*///else facility isn't a Satelite and connects to Postgres Database
				System.out.println("POSTGRES DATABASE");

				if (DatabaseSwitcher.switchToPostgres())
				{
					resource = "postgres-mybatis-config.xml";
					reader = Resources.getResourceAsReader(resource);

					if (sqlSessionFactory == null) {
						sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, System.getProperties());
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null,
							"<html><b> POSTGRES DATABASE IS NOT AVAILABLE. <br><font color=red>POSTGRES NECESSARY FOR CREATION OF R & R</font><b></html>",	"POSTGRESS DATABASE CONNECTION ERROR",JOptionPane.WARNING_MESSAGE);
					/*if (JOptionPane.showConfirmDialog(null,"<html><b> POSTGRES DATABASE IS NOT AVAILABLE. <br><font color=red>POSTGRESS NECESSARY FOR CREATIN OF R & R</font><b></html>",	"POSTGRESS DATABASE CONNECTION ERROR",
					        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {*/
						/*resource = "hsql-mybatis-config.xml";
						reader = Resources.getResourceAsReader(resource);

						if (sqlSessionFactory == null) {
							sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
						}*/
					}
				//}
			//}
		}
		catch (FileNotFoundException fileNotFoundException) {
			fileNotFoundException.printStackTrace();
		}
		catch (IOException iOEx) {
			System.out.println("MyBATIS DB CON ISSUES "+iOEx.getMessage());
		}
	}

	/**
	 * 
	 * @return an SqlSessionFactory which is used to open or close a database connection resource.
	 * Each open SqlSessionFactory that is opened must after use be closed
	 */
	public static SqlSessionFactory getSqlSessionFactory() {

		return sqlSessionFactory;
	}
}