/**
 * 
 */
package org.elmis.facility.tools;

//import java.util.LinkedList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Joe
 * 
 */
public class SearchList {

	private List<String> aList = new LinkedList();
	private ListIterator listIterator;// = aList.listIterator();
	private String stringInList;
	private boolean found = false;

	public SearchList() {
	}

	public boolean searchString(List<String> list, String stringIn) {
		listIterator = list.listIterator();
		while (listIterator.hasNext()) {
			stringInList = listIterator.next().toString();

			if (stringIn.equalsIgnoreCase(stringInList)) {

				found = true;
			}// end if

		}// end while loop

		return found;
	}

}
