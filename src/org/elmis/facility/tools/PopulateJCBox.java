/**
 * 
 */
package org.elmis.facility.tools;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Dispensing_point;
import org.elmis.facility.domain.model.Roles;
import org.elmis.facility.network.MyBatisConnectionFactory;

/**
 * @author JBanda
 *
 */
public class PopulateJCBox {
	
	private static List<Roles> rolesList;
	public static Integer roleIdList[]= new Integer[0];
	
	
	private static List<Dispensing_point> dpList;
	private static Integer locationList[] = new Integer[0];

	/**
	 * 
	 */
	public PopulateJCBox() {
		// TODO Auto-generated constructor stub
	}
	
	
	public static javax.swing.ComboBoxModel FillRolesJCBox() {
		
		
		String cmbList[] = new String[0];
		javax.swing.ComboBoxModel cModel;
		// javax.swing.JComboBox jcomboBox;
		int TotalRow = 0;
		int rowNum = 1;
		
		//get list of roles
		
			SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

				SqlSession session = factory.openSession();

				try {

					 rolesList = session.selectList("selectByRoles");//TODO add the order by clause

		

				} finally {
						session.close();
				}


	
			
			
			if (rolesList.size()>0) {

				
				
			
			
				
					cmbList = new String[rolesList.size() + 1];
					cmbList[0] = "Please Select Role";
					
					roleIdList = new Integer[rolesList.size() + 1];
					
					
					for(Roles roles : rolesList){

						cmbList[rowNum] = roles.getName();
						roleIdList[rowNum] = roles.getId();
								
						rowNum++;
						
					}
					
				 
				// cmbList[rowNum++] = "Consultation";
				// cmbList[rowNum++] = "New Matter";
			} else {
				cmbList = new String[TotalRow + 1];
				cmbList[0] = "Add New Role";
			}
		
		
		TotalRow = 0;
		cModel = new javax.swing.DefaultComboBoxModel(cmbList);

		return cModel;
	}
	
	
	
	
	/**************************************************************/
	
	public static javax.swing.ComboBoxModel FillLocationJCBox() {
		
		
		String cmbList[] = new String[0];
		javax.swing.ComboBoxModel cModel;
		// javax.swing.JComboBox jcomboBox;
		int TotalRow = 0;
		int rowNum = 0;
		
	
				
				new MyBatisConnectionFactory();
				SqlSessionFactory factory = MyBatisConnectionFactory
						.getSqlSessionFactory();

				SqlSession session = factory.openSession();

				try {

					dpList = session.selectList("selectBydp");

					

				} finally {
					session.close();
				}

	
			
			
			if (dpList.size()>0) {

				
				
			
			
				
					cmbList = new String[dpList.size() ];
					//cmbList[0] = "Please Select Role";
					
					locationList = new Integer[dpList.size()];
					
					
					for(Dispensing_point dp : dpList){

						cmbList[rowNum] = dp.getName();
					//	locationList[rowNum] = roles.getId();
								
						rowNum++;
						
					}
					
				 
				// cmbList[rowNum++] = "Consultation";
				// cmbList[rowNum++] = "New Matter";
			} else {
				cmbList = new String[TotalRow ];
				cmbList[0] = "Add Location";
			}
		
		
		TotalRow = 0;
		cModel = new javax.swing.DefaultComboBoxModel(cmbList);

		return cModel;
	}


}
