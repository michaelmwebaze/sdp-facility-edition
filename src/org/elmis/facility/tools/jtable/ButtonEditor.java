package org.elmis.facility.tools.jtable;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * @version 1.0 11/09/98
 */
public class ButtonEditor extends DefaultCellEditor {
  protected JTextField button;  
  private String    label;
  private boolean   isPushed;

  public ButtonEditor(JCheckBox checkBox) {
    super(checkBox);
    button = new JTextField(10);
 //  button.setOpaque(true);
 /*   button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        fireEditingStopped();
      }
    });*/
  }

 public Component getTableCellEditorComponent(JTable table, Object value,
                   boolean isSelected, int row, int column) {
    if (isSelected) {
      button.setForeground(table.getSelectionForeground());
      button.setBackground(table.getSelectionBackground());
      button.setSize(20, 2);
    } else{
      button.setForeground(table.getForeground());
      button.setBackground(table.getBackground());
      button.setSize(20, 2);
    }
    
    button.setBounds(0,0, 5, 2);
    
    button.setBackground(Color.red);
  //  label = (value ==null) ? "" : value.toString();
   // button.setText( label );
  //  isPushed = true;
    return button;
  }

/*  public Object getCellEditorValue() {
    if (isPushed)  {
      // 
      // 
      JOptionPane.showMessageDialog(button ,label + ": Ouch!");
      // System.out.println(label + ": Ouch!");
    }
    isPushed = false;
    return new String( label ) ;
  }
  */
 /* public boolean stopCellEditing() {
    isPushed = false;
    return super.stopCellEditing();
  }*/

  /*protected void fireEditingStopped() {
    super.fireEditingStopped();
  }*/
}
