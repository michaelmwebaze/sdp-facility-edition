package org.elmis.facility.tools;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;

/**
 * 
 * @author __USER__
 */
public class LoadingScreen extends Thread {

	/** Creates new form LoadingScreen */
	public LoadingScreen(JDesktopPane desktop) {
		desk = desktop;
		initComponents();
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	// <editor-fold defaultstate="collapsed" desc=" Generated Code
	// ">//GEN-BEGIN:initComponents
	private void initComponents() {
		loadingJIF = new JInternalFrame();
		jPanel1 = new javax.swing.JPanel();
		jTextField1 = new javax.swing.JTextField();
		jTextField1.setEditable(false);
		jTextField1.setText("Loading......");
		loadImageJL = new javax.swing.JLabel(new ImageIcon(
		// "images/serv_hoops.gif"));
				"images_realestate/Animated_Dove_2.gif"));
		jPanel1.setBackground(new java.awt.Color(255, 255, 255));

		org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(
				org.jdesktop.layout.GroupLayout.LEADING).add(jTextField1,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 394,
				Short.MAX_VALUE).add(
				jPanel1Layout.createSequentialGroup().add(107, 107, 107).add(
						loadImageJL,
						org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 140,
						org.jdesktop.layout.GroupLayout.PREFERRED_SIZE).add(
						147, 147, 147)));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(
								org.jdesktop.layout.GroupLayout.TRAILING,
								jPanel1Layout
										.createSequentialGroup()
										.add(84, 84, 84)
										.add(
												loadImageJL,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
												100,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												org.jdesktop.layout.LayoutStyle.RELATED,
												70, Short.MAX_VALUE)
										.add(
												jTextField1,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)));

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
				loadingJIF.getContentPane());
		loadingJIF.getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				org.jdesktop.layout.GroupLayout.LEADING).add(jPanel1,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		desk.add(loadingJIF);
		// Rectangle frame = desk.getBounds();
		// setLocation((screen.width - frame.width)/2, (screen.height -
		// frame.height)/2);

		loadingJIF.setLocation(screen.width / 3, ((screen.height / 4)));
		loadingJIF.setVisible(true);
		loadingJIF.pack();
	}// </editor-fold>//GEN-END:initComponents

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JPanel jPanel1;

	public static javax.swing.JTextField jTextField1;

	private JDesktopPane desk;

	private final Dimension screen = Toolkit.getDefaultToolkit()
			.getScreenSize();

	public static javax.swing.JInternalFrame loadingJIF;

	private javax.swing.JLabel loadImageJL;
	// End of variables declaration//GEN-END:variables

}
