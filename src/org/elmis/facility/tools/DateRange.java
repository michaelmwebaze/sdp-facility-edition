/**
 * 
 */
package org.elmis.facility.tools;

import java.util.Date;

/**
 * @author Mbayi
 * 
 */
public class DateRange implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static java.util.Date startDate_this;
	public static java.util.Date endDate_this;

	/**
	 * 
	 */
	public DateRange() {
		// TODO Auto-generated constructor stub
	}

	public DateRange(java.util.Date startDate, java.util.Date endDate) {

		startDate_this = startDate;
		endDate_this = endDate;
	}

	public Date getStartDate() {
		return startDate_this;
	}

	public void setStartDate(Date startDate) {
		startDate_this = startDate;
	}

	public Date getEndDate() {
		return endDate_this;
	}

	public void setEndDate(Date endDate) {
		endDate_this = endDate;
	}

}
