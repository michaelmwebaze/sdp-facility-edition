package org.elmis.facility.tools;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

public class ImageRenderer extends DefaultTableCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel lbl = new JLabel();
	public static ImageIcon icon = null;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		// ImageIcon icon = new
		// ImageIcon(getClass().getResource("images/newinv.png"));
		// ImageIcon icon = new ImageIcon("images/newinv.png");
		// setText((String)value);
		// protected RightTableCellRenderer() {

		// }
		// setHorizontalAlignment(JLabel.LEFT);
		if (isSelected) {
			setForeground(table.getSelectionForeground());
			setBackground(table.getSelectionBackground());

		} else {
			setForeground(table.getForeground());
			setBackground(UIManager.getColor("Table.background"));

		}

		setIcon(icon);
		// this.setHorizontalAlignment(SwingConstants.LEFT);
		return this;

	}

}
