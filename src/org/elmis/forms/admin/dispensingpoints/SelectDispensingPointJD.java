/*
 * SelectDispensingPointJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.admin.dispensingpoints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Dispensing_point;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.forms.stores.issuing.IssuingJD;

import com.oribicom.tools.TableModel;

/**
 *
 * @author  __USER__
 */
public class SelectDispensingPointJD extends javax.swing.JDialog {

	private static final String[] columns_dispensingPoint = { "Name" };
	private static final Object[] defaultv_dispensingPoint = { "" };
	private static final int rows_dispensingPoint = 0;
	public static TableModel tableModel_dispensingPoint = new TableModel(
			columns_dispensingPoint, defaultv_dispensingPoint,
			rows_dispensingPoint);
	public static int total_dispensingPoint = 0;
	public static Map parameterMap_dispensingPoint = new HashMap();

	private static Dispensing_point dp;// 

	private static ListIterator<Dispensing_point> dpIterator;
	
	public static String selectedDispensingPoint;

	List<Dispensing_point> dpList = new LinkedList();

	/** Creates new form SelectDispensingPointJD */
	public SelectDispensingPointJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		
		this.setSize(500,450);
		this.setLocationRelativeTo(null);
		

		new MyBatisConnectionFactory();
		SqlSessionFactory factory = MyBatisConnectionFactory
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			dpList = session.selectList("selectBydp");

			populatedpTable(dpList);

		} finally {
			session.close();
		}
		
		this.setVisible(true);

		//IssuingJD.searchJFT.requestFocusInWindow();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		dispensingPointJTable = new javax.swing.JTable();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		dispensingPointJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		dispensingPointJTable.setModel(tableModel_dispensingPoint);
		dispensingPointJTable.setRowHeight(60);
		dispensingPointJTable.setTableHeader(null);
		dispensingPointJTable
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						dispensingPointJTableMouseClicked(evt);
					}
				});
		jScrollPane1.setViewportView(dispensingPointJTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400,
				Short.MAX_VALUE).addComponent(jScrollPane1,
				javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 381,
				Short.MAX_VALUE).addComponent(jScrollPane1,
				javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void dispensingPointJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
	//	IssuingJD.jTabbedPane1.addTab("Program", new ProgramsJP());
	//	IssuingJD.jTabbedPane1.setSelectedIndex(1);

		//IssuingJD.searchJFT.requestFocus();
	//	IssuingJD.searchJFT.requestFocusInWindow();
		dp = new Dispensing_point();
		dp.setName(this.dispensingPointJTable.getValueAt(
						this.dispensingPointJTable.getSelectedRow(), 0).toString().trim());
		
		selectedDispensingPoint = dp.getName();
		
		
		new IssuingJD(javax.swing.JOptionPane.getFrameForComponent(this), true, dp);
		
		this.dispose();
	}

	public static void populatedpTable(List dataList) {

		tableModel_dispensingPoint.clearTable();

		dpIterator = dataList.listIterator();

		while (dpIterator.hasNext()) {

			dp = dpIterator.next();
			defaultv_dispensingPoint[0] = dp.getName();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_dispensingPoint.length; j++) {
				cols.add(defaultv_dispensingPoint[j]);

			}

			tableModel_dispensingPoint.insertRow(cols);

			dpIterator.remove();
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SelectDispensingPointJD dialog = new SelectDispensingPointJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JTable dispensingPointJTable;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

}