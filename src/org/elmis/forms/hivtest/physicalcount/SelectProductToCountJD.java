/*
 * SelectProductsJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest.physicalcount;

import java.util.LinkedList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.HivTestPhysicalCount;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.tools.MessageDialog;

import com.oribicom.tools.publicMethods;

/**
 * 
 * @author __USER__
 */
public class SelectProductToCountJD extends javax.swing.JDialog {

	private String searchText = "";
	private char letter;
	private List<Character> charList = new LinkedList();
	public static String barcodeScanned;
	public static int selectedProductID;
	public static Products selectedProduct;

	private List<Products> productsList = new LinkedList();
	private List<HivTestPhysicalCount> PhysicalCountList = new LinkedList();
	private HivTestPhysicalCount physicalCount;

	/** Creates new form SelectProductsJD */
	public SelectProductToCountJD(java.awt.Frame parent, boolean modal,
			String title, String programArea) {
		super(parent, modal);
		initComponents();

		this.setTitle(title);
		//this.getContentPane().getComponent(2).setFont(new Font("Lucida",Font.PLAIN,48));
		//this.getLocation().setLocation(arg0)
		this.setSize(700, 450);
		this.setLocationRelativeTo(null);

		jTabbedPane1.addTab("Products", new ProductsJP(2));

		// dont show this
		// barcodeJTF.setVisible(false);
		// searchJL.setVisible(false);

		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		saveCountJBtn = new javax.swing.JButton();
		cancelJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Stock Control card");

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});

		saveCountJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveCountJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		saveCountJBtn.setText("Save Count ");
		saveCountJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveCountJBtnActionPerformed(evt);
			}
		});

		cancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.setText("Cancel");
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 403,
				Short.MAX_VALUE).addGroup(
				layout.createSequentialGroup().addGap(22, 22, 22).addComponent(
						saveCountJBtn).addGap(24, 24, 24).addComponent(
						cancelJBtn, javax.swing.GroupLayout.PREFERRED_SIZE,
						114, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(116, Short.MAX_VALUE)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												jTabbedPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												374,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																saveCountJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																28,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																cancelJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																27,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { cancelJBtn, saveCountJBtn });

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		this.dispose();
		new MessageDialog().showDialog(this, "Action Cancelled", "Cancelled",
				"Close ");
	}

	private void saveCountJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		ProductsJP.tableModel_products.getRowCount();

		for (int x = 0; x < ProductsJP.tableModel_products.getRowCount(); x++) {

			// if qty field is empty skip the item
			if (!ProductsJP.tableModel_products.getValueAt(x, 2).toString()
					.equals("")) {

				physicalCount = new HivTestPhysicalCount();

				physicalCount.setId(publicMethods.createGUID());

				physicalCount.setQty_count(Integer
						.parseInt(ProductsJP.tableModel_products.getValueAt(x,
								2).toString()));

				physicalCount.setCount_type("Begining count");
				physicalCount.setTesting_site(System.getProperty("dp_name"));
				physicalCount.setCreated_by(AppJFrame.userLoggedIn);
				physicalCount.setProduct_code(ProductsJP.tableModel_products
						.getValueAt(x, 0).toString());

				PhysicalCountList.add(physicalCount);
			} // end if qty field is empty

		}

		//if the list is empty dont save anything

		if (!PhysicalCountList.isEmpty()) {
			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				session.insert("insertPhysicaCountList", PhysicalCountList);

				try {
					session.commit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

				session.close();
				
				//javax.swing.JOptionPane.showMessageDialog(this,"Saved");

				new MessageDialog().showDialog(this, "Physical Count", "Saved",
					"Close  ");

				this.dispose();
				
				

			}

		} else {
			
			//javax.swing.JOptionPane.showMessageDialog(this,"Saved");
			new MessageDialog().showDialog(this,
					"You have not added anything to count", "Error", "Close  ");
		}

	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getSelectedIndex());

		if (jTabbedPane1.getSelectedIndex() == 1
				&& jTabbedPane1.getTabCount() == 3) {// &&
			// jTabbedPane1.getSelectedIndex()
			// ==2){

			// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getTabCount());

			jTabbedPane1.remove(2);
		}

		if (jTabbedPane1.getSelectedIndex() == 0
				&& jTabbedPane1.getTabCount() == 2) {// &&

			// remove the products JTable
			jTabbedPane1.remove(1);

		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SelectProductToCountJD dialog = new SelectProductToCountJD(
						new javax.swing.JFrame(), true, new String(),
						new String());
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton cancelJBtn;
	public static javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JButton saveCountJBtn;
	// End of variables declaration//GEN-END:variables

}