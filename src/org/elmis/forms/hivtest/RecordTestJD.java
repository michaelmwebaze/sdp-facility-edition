/*
 * RecordTestJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest;

import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.HivTest;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.tools.MessageDialog;

import com.oribicom.tools.publicMethods;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author  __USER__
 */
public class RecordTestJD extends javax.swing.JDialog {

	private int selectTabIndex = 0;
	private HivTest hivTest;
	private String typeOfTest;

	/** Creates new form RecordTestJD */
	public RecordTestJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);

		//	this.setTitle(arg0)

		hivTest = new HivTest();

		//UIManager UI=new UIManager();
		// UI.put("OptionPane.background",new ColorUIResource(255,0,0));
		// UI.put("Panel.background",new ColorUIResource(255,0,0));

		initComponents();
		
		backJL.setVisible(false);
		//rend.setHorizontalAlignment(JLabel.LEFT);

		//jtableHeader.setDefaultRenderer(rend);

		//this.counsellingJtable.setOpaque(true);

		/*	this.counsellingJtable.getTableHeader().setDefaultRenderer(new DefaultTableRenderer(){
				  {
				    // you need to set it to opaque
				    setOpaque(true);
				  }

				@Override
				public Component getTableCellRendererComponent(final JTable table,
				  final Object value, final boolean isSelected, final boolean hasFocus,
				  final int row, final int column) {
				    // set the background
				    setBackground(yourDesiredColor);
				  }
				});
				
		 */

		if (System.getProperty("dp_name") != null) {

			this.setTitle("HIV testing at :  " + System.getProperty("dp_name"));

		}

		///jTabbedPane1.remove(6);
		//jTabbedPane1.remove(5);
		//jTabbedPane1.remove(4);
		//jTabbedPane1.remove(3);
		jTabbedPane1.remove(2);
		jTabbedPane1.remove(1);

		//jTable2.setRowSelectionAllowed(true);

		//jTable2.setColumnSelectionAllowed(false);
		//jTable2.setRowSelectionInterval(0,2);
		//jTable2.repaint();

		this.setSize(600, 500);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		//jPanel1.requestFocus();
		//
		//jTable1.repaint();

		//jTable2.getSelectionModel().getLeadSelectionIndex();
		//jTable2.requestFocus();
		//jTable2.setRowSelectionAllowed(true);
		//jTable2.setColumnSelectionAllowed(false);

		//jTable2.repaint();

		//jTable1.setSurrendersFocusOnKeystroke(true);
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		purposeJP = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		purposeJTable = new javax.swing.JTable();
		testTypeJP = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		testTypeJtable = new javax.swing.JTable();
		resultJP = new javax.swing.JPanel();
		jScrollPane3 = new javax.swing.JScrollPane();
		resultJtable = new javax.swing.JTable();
		jLabel3 = new javax.swing.JLabel();
		jLabel3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				dispose();
			}
		});
		backJL = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("HIV Testing");

		jTabbedPane1.setBorder(new javax.swing.border.LineBorder(
				new java.awt.Color(255, 255, 255), 4, true));
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});

		purposeJTable.setFont(new java.awt.Font("Tahoma", 0, 36));
		purposeJTable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] { { "VCT" }, { "PMTCT" },
						{ "Clinical Diagnosis" }, { "Quality Control" },
						{ "Other" } }, new String[] { "Title 1" }));
		purposeJTable.setCellSelectionEnabled(true);
		purposeJTable.setRowHeight(60);
		purposeJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				purposeJTableMouseClicked(evt);
			}
		});
		purposeJTable.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				purposeJTableKeyTyped(evt);
			}
		});
		jScrollPane2.setViewportView(purposeJTable);

		javax.swing.GroupLayout purposeJPLayout = new javax.swing.GroupLayout(
				purposeJP);
		purposeJP.setLayout(purposeJPLayout);
		purposeJPLayout.setHorizontalGroup(purposeJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 486,
				Short.MAX_VALUE));
		purposeJPLayout.setVerticalGroup(purposeJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 367,
				Short.MAX_VALUE));

		jTabbedPane1.addTab("Purpose", new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Purpose.png")), purposeJP); // NOI18N

		testTypeJtable.setFont(new java.awt.Font("Tahoma", 0, 36));
		testTypeJtable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] { { "Screening" }, { "Confirmatory" },
						{ "Tiebreaker" } }, new String[] { "Title 1" }));
		testTypeJtable.setEditingColumn(0);
		testTypeJtable.setEditingRow(0);
		testTypeJtable.setRowHeight(60);
		testTypeJtable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				testTypeJtableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(testTypeJtable);

		javax.swing.GroupLayout testTypeJPLayout = new javax.swing.GroupLayout(
				testTypeJP);
		testTypeJP.setLayout(testTypeJPLayout);
		testTypeJPLayout.setHorizontalGroup(testTypeJPLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jScrollPane1,
						javax.swing.GroupLayout.DEFAULT_SIZE, 486,
						Short.MAX_VALUE));
		testTypeJPLayout.setVerticalGroup(testTypeJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367,
				Short.MAX_VALUE));

		jTabbedPane1.addTab("Test Type", new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/test type.png")), testTypeJP); // NOI18N

		resultJP.setFont(new java.awt.Font("Tahoma", 0, 14));

		resultJtable.setFont(new java.awt.Font("Tahoma", 0, 36));
		resultJtable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] { { "Reactive (R)" }, { "Non-Reactive (NR)" },
						{ "Invalid (I)" } }, new String[] { "Title 1" }));
		resultJtable.setRowHeight(60);
		resultJtable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				resultJtableMouseClicked(evt);
			}
		});
		jScrollPane3.setViewportView(resultJtable);

		javax.swing.GroupLayout resultJPLayout = new javax.swing.GroupLayout(
				resultJP);
		resultJP.setLayout(resultJPLayout);
		resultJPLayout.setHorizontalGroup(resultJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 486,
				Short.MAX_VALUE));
		resultJPLayout.setVerticalGroup(resultJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 367,
				Short.MAX_VALUE));

		jTabbedPane1.addTab("Result", new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Results icon.png")), resultJP); // NOI18N

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		jLabel3.setText("Cancel");

		backJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		backJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Back.png"))); // NOI18N
		backJL.setText("Back");
		backJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				backJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(jTabbedPane1,
								javax.swing.GroupLayout.DEFAULT_SIZE, 499,
								Short.MAX_VALUE)
						.addGroup(
								layout
										.createSequentialGroup()
										.addGap(36, 36, 36)
										.addComponent(
												backJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												76,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												250, Short.MAX_VALUE)
										.addComponent(
												jLabel3,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												79,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(58, 58, 58)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												jTabbedPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												421,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(18, 18, 18)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel3,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																30,
																Short.MAX_VALUE)
														.addComponent(
																backJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																30,
																Short.MAX_VALUE))
										.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void backJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

	/*	if (jTabbedPane1.getTabCount() > 1) {

			jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);

		}*/
		
		//remove tabs 
				int selectedtab = jTabbedPane1.getSelectedIndex() ;
				
				if( selectedtab == 1){
					
					backJL.setVisible(false);
				}
				if(selectedtab > 0 ){

				for (int j = jTabbedPane1.getTabCount(); j > selectedtab; j--) {

					jTabbedPane1.remove(j - 1);

				}
				}

	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//remove tabs 
		int selectedtab = jTabbedPane1.getSelectedIndex() + 1;

		for (int j = jTabbedPane1.getTabCount(); j > selectedtab; j--) {

			jTabbedPane1.remove(j - 1);

		}
	}

	/**
	 * @param evt
	 */
	private void resultJtableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		/***********************************************************/
		//this splits the selected result in the
		//resultJTable to take out the "()" for the selected result 
		//e.g.  Reactive(R) 
		String selectedResult = this.resultJtable.getModel().getValueAt(
				this.resultJtable.getSelectedRow(), 0).toString();
		String[] result = selectedResult.split("\\(");
		String testResult = result[1].toString().replaceAll("\\)", "");//returns ( R , NR ,or I)
		/***********************************************************/

		/*****************************************************/
		//if screening test is non-reactive	(option 1)	
		if (this.typeOfTest.equals("Screening")
				&& testResult.equalsIgnoreCase("NR")) {
			//set test result to NR
			this.hivTest.setResult_screeninig(testResult);
			//set final test result to NR
			this.hivTest.setFinal_negative("NR");
			this.saveTestResults();

			/****************************************************/
			//if screening test is reactive 
		} else if (this.typeOfTest.equals("Screening")
				&& testResult.equalsIgnoreCase("R")) {
			this.hivTest.setResult_screeninig(testResult);

			//prompt if you should do confirmatory rest

			/**************************************************/

			int ok = new MessageDialog()
					.showDialog(
							this,
							"Screening test is reactive would you\nlike to do confirmatory test now?",
							"Screening Test", "YES, Do \nConfirmatroy Test",
							"NO, Save\nScreening Test", "Cancel");

			if (ok == 1) {
				//option 2 screening is reactive 
				//NO confirmatory test

				//save final rest as Indeterminate
				this.hivTest.setFinal_indeterminate("I");
				this.saveTestResults();

			} else if (ok == 0) {

				//yes save confirmatory

				((DefaultTableModel) this.testTypeJtable.getModel())
						.removeRow(0);
				((DefaultTableModel) this.testTypeJtable.getModel())
						.removeRow(1);
				//this.testTypeJtable.setValueAt("Confirmatory", 0, 0);
				jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);
				this.typeOfTest = "Confirmatory";
				//this.resultJtable.getModel().setValueAt(2,this.resultJtable.getSelectedRow(), 0);

			}

		} else if (this.typeOfTest.equals("Screening")
				&& testResult.equalsIgnoreCase("I")) {
			//set test result to NR
			this.hivTest.setResult_screeninig(testResult);
			//set final test result to NR
			this.hivTest.setFinal_indeterminate("I");
			this.saveTestResults();

			/****************************************************/
			//if screening test is reactive 
		} else if (this.typeOfTest.equals("Confirmatory")) {

			this.hivTest.setResult_confirmatory(testResult);

			//option 3 if confirmatory is reactive(R)  
			if (testResult.equalsIgnoreCase("R")) {

				this.hivTest.setFinal_positive("R");

				this.saveTestResults();

			}

			else if (testResult.equalsIgnoreCase("NR")) {

				//check if a screening test was done and was reactive 
				if (this.hivTest.getResult_screeninig() != null) {
					//if Confirmatory is NR and Screening was reactive
					//prompt if facility can do tiebreaker

					int ok = new MessageDialog()
							.showDialog(
									this,
									"Confirmatory test is reactive would you\n like to do tiebreaker test now?",
									"Confirmatory Test",
									"YES, Do \nTie Breaker Test",
									"NO, Save\nConfirmatory Test", "Cancel");

					if (ok == 1) {
						//option 2 screening is reactive 
						//NO Tie breaker test

						//save final rest as Indeterminate
						this.hivTest.setFinal_indeterminate("I");
						this.saveTestResults();

					} else if (ok == 0) {

						//yes save Tie Breaker

						this.testTypeJtable.setValueAt("Tiebreaker", 0, 0);
						jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);
						this.typeOfTest = "Tiebreaker";
						//this.resultJtable.getModel().setValueAt(2,this.resultJtable.getSelectedRow(), 0);

					}

				} else //if user did not do a screening test just went straight to 
				//do confirmatory rest

				{
					this.hivTest.setFinal_positive("NR");

					this.saveTestResults();

				}

			} else

			// if confirmatory is Invalid(I)  
			if (testResult.equalsIgnoreCase("I")) {

				this.hivTest.setFinal_indeterminate("I");

				this.hivTest.setResult_confirmatory("I");

				this.saveTestResults();

			}

		} else if (this.typeOfTest.equals("Tiebreaker")) {

			this.hivTest.setResult_tiebreaker(testResult);

			if (testResult.equalsIgnoreCase("R")) {

				this.hivTest.setFinal_positive("R");

			} else if (testResult.equalsIgnoreCase("NR")) {

				this.hivTest.setFinal_negative("NR");
			} else if (testResult.equalsIgnoreCase("I")) {

				this.hivTest.setFinal_indeterminate("I");
			}

			this.saveTestResults();

		}

	}

	private void testTypeJtableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		this.typeOfTest = this.testTypeJtable.getModel().getValueAt(
				this.testTypeJtable.getSelectedRow(), 0).toString();

		jTabbedPane1.addTab("Result", new javax.swing.ImageIcon(getClass()
				.getResource("/images/cakes3d.png")), resultJP);
		jTabbedPane1.setSelectedIndex(2);
	}

	private void purposeJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		hivTest.setPurpose(this.purposeJTable.getModel().getValueAt(
				this.purposeJTable.getSelectedRow(), 0).toString());
		jTabbedPane1.addTab("Type of Test", testTypeJP);
		jTabbedPane1.setSelectedIndex(1);
		backJL.setVisible(true);
	}

	private void purposeJTableKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		final char c = evt.getKeyChar();

		if (c == KeyEvent.VK_ENTER) {

			selectTabIndex = jTabbedPane1.getSelectedIndex();

			if (selectTabIndex == 0) {

				jTabbedPane1.setSelectedIndex(1);

			} else if (selectTabIndex == 1) {

				jTabbedPane1.setSelectedIndex(2);

			}
		}

	}

	@SuppressWarnings("static-access")
	private void saveTestResults() {

		int ok = new MessageDialog().showDialog(this,
				"Would you like to save test results",
				"Save HIV test Restults", "Save HIV Test Results", "Cancel");

		if (ok == 0) {

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				this.hivTest.setId(publicMethods.createGUID());
				this.hivTest.setTesting_point_id(System.getProperty("dp_name"));

				session.insert("insertSelectiveHivTest", hivTest);

				try {
					session.commit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

				//	javax.swing.JOptionPane
				//			.showMessageDialog(this, "Results saved");

				session.close();

				new MessageDialog().showDialog(this, "Test Results Saved",
						"Saved", "Close and return ");

				this.dispose();

			}

			new RecordTestJD(
					javax.swing.JOptionPane.getFrameForComponent(this), true);

		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				RecordTestJD dialog = new RecordTestJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}
	private javax.swing.JLabel backJL;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JPanel purposeJP;
	private javax.swing.JTable purposeJTable;
	private javax.swing.JPanel resultJP;
	private javax.swing.JTable resultJtable;
	private javax.swing.JPanel testTypeJP;
	private javax.swing.JTable testTypeJtable;
	// End of variables declaration//GEN-END:variables

}