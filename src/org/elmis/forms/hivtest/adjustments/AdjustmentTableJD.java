/*
 * SelectProductsJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest.adjustments;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.HivTestLossesAdustments;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.tools.MessageDialog;

import com.oribicom.tools.TableModel;
import com.oribicom.tools.publicMethods;

/**
 * 
 * @author __USER__
 */
public class AdjustmentTableJD extends javax.swing.JDialog {

	//private String searchText = "";
	private char letter;
	private List<Character> charList = new LinkedList();
	public static String barcodeScanned;
	public static int selectedProductID;
	public static Products selectedProduct;

	private List<HivTestLossesAdustments> adjustmentList = new LinkedList();
	private static ListIterator<HivTestLossesAdustments> adjustmentsIterator;
	private static HivTestLossesAdustments adjustments;

	// Create tableModel for Program Products for each facility
	private static final String[] columns_adjustmenst = { "Product Code",
			"Product name", "Loss & Adjustment", "QTY", "delete" };
	private static final Object[] defaultv_adjustmenst = { "", "", "", "", "" };
	private static final int rows_adjustmenst = 0;
	public static TableModel tableModel_adjustmenst = new TableModel(
			columns_adjustmenst, defaultv_adjustmenst, rows_adjustmenst);

	/** Creates new form SelectProductsJD */
	public AdjustmentTableJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		tableModel_adjustmenst.clearTable();

		class CenterTableCellRenderer extends DefaultTableCellRenderer {
			protected CenterTableCellRenderer() {
				setHorizontalAlignment(JLabel.CENTER);
			}

		}

		CenterTableCellRenderer centerAlign = new CenterTableCellRenderer();

		this.jTable1.getColumnModel().getColumn(0).setCellRenderer(centerAlign);
		this.jTable1.getColumnModel().getColumn(1).setCellRenderer(centerAlign);
		this.jTable1.getColumnModel().getColumn(2).setCellRenderer(centerAlign);
		this.jTable1.getColumnModel().getColumn(3).setCellRenderer(centerAlign);
		this.jTable1.getColumnModel().getColumn(4).setCellRenderer(centerAlign);

		this.jTable1.getColumnModel().getColumn(0).setMinWidth(100);
		this.jTable1.getColumnModel().getColumn(0).setMaxWidth(100);

		this.jTable1.getColumnModel().getColumn(3).setMinWidth(100);
		this.jTable1.getColumnModel().getColumn(3).setMaxWidth(100);

		this.jTable1.getColumnModel().getColumn(4).setMinWidth(50);
		this.jTable1.getColumnModel().getColumn(4).setMaxWidth(50);

		saveAdjustmentJBtn.setVisible(false);

		this.setSize(700, 450);
		this.setLocationRelativeTo(null);

		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		saveAdjustmentJBtn = new javax.swing.JButton();
		cancelJBtn = new javax.swing.JButton();
		jSeparator1 = new javax.swing.JSeparator();
		newAdjustmentJBtn = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTable1 = new javax.swing.JTable();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("HIV Test Kits Losses and Adjustments");

		saveAdjustmentJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveAdjustmentJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Save icon.png"))); // NOI18N
		saveAdjustmentJBtn.setText("Save Adjustment(s)");
		saveAdjustmentJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						saveAdjustmentJBtnActionPerformed(evt);
					}
				});

		cancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.setText("Cancel");
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});

		jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

		newAdjustmentJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		newAdjustmentJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/new adjustmet.png"))); // NOI18N
		newAdjustmentJBtn.setText("New Adjustment");
		newAdjustmentJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						newAdjustmentJBtnActionPerformed(evt);
					}
				});

		jTable1.setModel(tableModel_adjustmenst);
		jTable1.setCellSelectionEnabled(true);
		jTable1.setRowHeight(34);
		jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTable1MouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(jTable1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addComponent(jScrollPane1,
								javax.swing.GroupLayout.Alignment.TRAILING,
								javax.swing.GroupLayout.DEFAULT_SIZE, 484,
								Short.MAX_VALUE)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(saveAdjustmentJBtn)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(cancelJBtn)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												jSeparator1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												15, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(newAdjustmentJBtn)
										.addContainerGap()));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												370,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				jSeparator1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				39,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addGroup(
																				layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								saveAdjustmentJBtn,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								39,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								cancelJBtn,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								39,
																								Short.MAX_VALUE))
																		.addGap(
																				11,
																				11,
																				11))
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				newAdjustmentJBtn)
																		.addContainerGap()))));

		layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { cancelJBtn, jSeparator1,
						newAdjustmentJBtn, saveAdjustmentJBtn });

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		if (jTable1.getSelectedColumn() == 4) {

			//	System.out.println("Delete : " + (String) jTable1.getValueAt(jTable1.getSelectedRow(),1));

			int ok = javax.swing.JOptionPane.showConfirmDialog(null,
					"Are you sure you would like to delete \nselected row?");

			if (ok == 0) {

				tableModel_adjustmenst.deleteRow(jTable1.getSelectedRow());
			}

			if (tableModel_adjustmenst.getRowCount() == 0) {

				saveAdjustmentJBtn.setVisible(false);
			}
		}
	}

	private void saveAdjustmentJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		this.tableModel_adjustmenst.getRowCount();

		for (int rowNumber = 0; rowNumber < this.tableModel_adjustmenst
				.getRowCount(); rowNumber++) {

			// if qty field is empty skip the item
			if (!this.tableModel_adjustmenst.getValueAt(rowNumber, 3)
					.toString().equals("")) {

				adjustments = new HivTestLossesAdustments();

				adjustments.setId(publicMethods.createGUID());
				adjustments.setCreated_by(AppJFrame.userName);
				adjustments.setLoss_adjustment_type(this.tableModel_adjustmenst
						.getValueAt(rowNumber, 2).toString());
				adjustments.setProduct_code(this.tableModel_adjustmenst
						.getValueAt(rowNumber, 0).toString());
				adjustments.setProduct_name(this.tableModel_adjustmenst
						.getValueAt(rowNumber, 1).toString());
				adjustments.setQty(Integer.parseInt(this.tableModel_adjustmenst
						.getValueAt(rowNumber, 3).toString()));
				adjustments.setTesting_site(System.getProperty("dp_name"));

				adjustmentList.add(adjustments);
			} // end if qty field is empty

		}

		if (!adjustmentList.isEmpty()) {
			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				session.insert("insertHivLossAdjustementList", adjustmentList);

				try {
					session.commit();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} finally {

				session.close();

				new MessageDialog().showDialog(this, "Losses and Adjustment",
						"Saved", "Close  ");

				this.dispose();

			}

		} else {

			new MessageDialog().showDialog(this,
					"You have not added anything to count", "Error", "Close  ");
		}

	}

	private void newAdjustmentJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		new AdjustmentSelectJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		saveAdjustmentJBtn.setVisible(true);
	}

	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		this.dispose();
		new MessageDialog().showDialog(this, "Action Cancelled", "Cancelled",
				"Close ");
	}

	public static void populateAdjustmentsTable(List progProducts) {

		tableModel_adjustmenst.clearTable();
		adjustmentsIterator = progProducts.listIterator();

		while (adjustmentsIterator.hasNext()) {

			adjustments = adjustmentsIterator.next();

			defaultv_adjustmenst[0] = adjustments.getProduct_code();
			defaultv_adjustmenst[1] = adjustments.getProduct_name();
			defaultv_adjustmenst[2] = adjustments.getLoss_adjustment_type();
			defaultv_adjustmenst[3] = adjustments.getQty();
			//defaultv_adjustmenst[4] = adjustments.getId();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_adjustmenst.length; j++) {
				cols.add(defaultv_adjustmenst[j]);

			}

			tableModel_adjustmenst.insertRow(cols);

			adjustmentsIterator.remove();
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				AdjustmentTableJD dialog = new AdjustmentTableJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton cancelJBtn;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSeparator jSeparator1;
	private static javax.swing.JTable jTable1;
	public static javax.swing.JButton newAdjustmentJBtn;
	private javax.swing.JButton saveAdjustmentJBtn;
	// End of variables declaration//GEN-END:variables

}