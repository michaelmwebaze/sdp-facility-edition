/*
 * ARTNumber.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import javax.swing.JOptionPane;

import org.elmis.facility.controllers.facilityarvdispensingsessionsmappercalls;
import org.elmis.facility.controllers.facilityproductssetupsessionmapperscalls;
import org.elmis.facility.controllers.facilitysetupsessionsmappercalls;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;

import com.oribicom.tools.TableModel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "serial", "unused", "unchecked" })
public class ARTNumberJP extends javax.swing.JPanel {

	/** Creates new form ARTNumber 
	 * @param b 
	 * @param frame */

//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Code", "Pack size", "Product name", "Quantity Dispensed" };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "" };
	private static final int rows_fproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 3) {

				return getValueAt(0, 3).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	//Facility Approved products JTable for REGIMEN only
	private static final String[] columns_arvfacilityApprovedproducts = {
			"Product Code", "Product name", "Generic Strength", "Pack Size",
			"Quantity Dispensed" };
	private static final Object[] defaultv_arvfacilityapprovedproducts = { "",
			"", "", "", "" };
	private static final int rows_arvfproducts = 0;
	public static TableModel tableModel_arvfproducts = new TableModel(
			columns_arvfacilityApprovedproducts,
			defaultv_arvfacilityapprovedproducts, rows_arvfproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4) {

				return getValueAt(0, 4).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	private Timestamp shipmentdate;
	public String oldDateString;
	public static String searchedARTnumber = "";
	public String artnumber = "";
	public String setARTtxtfield;
	public String setNRCtxtfield;
	public String newDateString;
	private facilityarvdispensingsessionsmappercalls callarvdispenser;
	private Elmis_Patient elmis_patient;
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvpatientdarproductsList = new LinkedList();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	private static ListIterator<Elmis_Dar_Transactions> arvpatientdarproductsIterator;
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	ArrayList<Elmis_Dar_Transactions> elmisstockcontrolcardList = new ArrayList<Elmis_Dar_Transactions>();
	facilityproductssetupsessionmapperscalls Facilityproductsmapper = null;
	facilitysetupsessionsmappercalls callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	Elmis_Dar_Transactions elmis_dar_transactions;
	Elmis_Dar_Transactions arvpatientdarProducts;
	Elmis_Dar_Transactions saveelmis_dar_transactions;
	public String typeCode;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer PACKSIZE;
	private Integer Convert_tablet_to_bottle;
	public boolean artfound;
	public static boolean usenumberpad = true;
	public Object myobject;

	public ARTNumberJP() {
		initComponents();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		PatientARTJTP = new javax.swing.JTabbedPane();
		SearchJP = new javax.swing.JPanel();
		jPanel7 = new javax.swing.JPanel();
		ARTnumberJT2 = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		ARTsearchJB1 = new javax.swing.JButton();
		ARTmessagedisplay = new javax.swing.JLabel();
		RegimenjPanel = new javax.swing.JPanel();
		jPanel4 = new javax.swing.JPanel();
		currentregimenJL = new javax.swing.JLabel();
		dispenseJBtn = new javax.swing.JButton();
		closeJBtn = new javax.swing.JButton();
		jScrollPane3 = new javax.swing.JScrollPane();
		ARVdispenseJT1 = new javax.swing.JTable();
		productQtyjLabel = new javax.swing.JLabel();
		RegisterjPanel = new javax.swing.JPanel();
		RegisterjPanel.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentShown(ComponentEvent arg0) {
				ARTnumberJT1.requestFocus();
			}
		});
		jPanel5 = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		NRCJT = new javax.swing.JTextField();
		RegisterJBtn1 = new javax.swing.JButton();
		closeJBtn1 = new javax.swing.JButton();
		ARTnumberJT1 = new javax.swing.JTextField();

		setBackground(new java.awt.Color(102, 102, 102));
		addComponentListener(new java.awt.event.ComponentAdapter() {
			public void componentShown(java.awt.event.ComponentEvent evt) {
				JPaneldisplayed(evt);
			}
		});

		PatientARTJTP.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent evt) {
				PatientARTJTP(evt);
			}
		});

		SearchJP.setFont(new java.awt.Font("Gulim", 0, 11));

		jPanel7.setBackground(new java.awt.Color(102, 102, 102));
		jPanel7.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Client Search"));

		ARTnumberJT2.setFont(new java.awt.Font("Ebrima", 0, 24));
		ARTnumberJT2.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				ARTnumberJT2KeyPressed(evt);
			}
		});

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Enter ART Number or NRC here:");

		ARTsearchJB1.setFont(new java.awt.Font("Ebrima", 1, 12));
		ARTsearchJB1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Search icon.png"))); // NOI18N
		ARTsearchJB1.setText("Search");
		ARTsearchJB1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ARTsearchJB1MouseClicked(evt);
			}
		});

		ARTmessagedisplay.setFont(new java.awt.Font("Ebrima", 0, 18));

		javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(
				jPanel7);
		jPanel7.setLayout(jPanel7Layout);
		jPanel7Layout
				.setHorizontalGroup(jPanel7Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel7Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel7Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel7Layout
																		.createSequentialGroup()
																		.addComponent(
																				ARTnumberJT2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				382,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																				54,
																				Short.MAX_VALUE)
																		.addComponent(
																				ARTsearchJB1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				117,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				38,
																				38,
																				38))
														.addGroup(
																jPanel7Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel7Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel3)
																						.addComponent(
																								ARTmessagedisplay))
																		.addContainerGap(
																				412,
																				Short.MAX_VALUE)))));
		jPanel7Layout
				.setVerticalGroup(jPanel7Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel7Layout
										.createSequentialGroup()
										.addComponent(jLabel3)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel7Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																ARTnumberJT2,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																42,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																ARTsearchJB1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																42,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18).addComponent(
												ARTmessagedisplay)
										.addContainerGap(54, Short.MAX_VALUE)));

		javax.swing.GroupLayout SearchJPLayout = new javax.swing.GroupLayout(
				SearchJP);
		SearchJP.setLayout(SearchJPLayout);
		SearchJPLayout.setHorizontalGroup(SearchJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				SearchJPLayout.createSequentialGroup().addContainerGap()
						.addComponent(jPanel7,
								javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(40, Short.MAX_VALUE)));
		SearchJPLayout.setVerticalGroup(SearchJPLayout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				SearchJPLayout.createSequentialGroup().addGap(46, 46, 46)
						.addComponent(jPanel7,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addGap(59, 59, 59)));

		PatientARTJTP.addTab("Client Search", SearchJP);

		RegimenjPanel
				.addComponentListener(new java.awt.event.ComponentAdapter() {
					public void componentShown(java.awt.event.ComponentEvent evt) {
						RegimenjPanelComponentShown(evt);
					}
				});

		jPanel4.setBackground(new java.awt.Color(102, 102, 102));
		jPanel4.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Client Details"));

		currentregimenJL.setFont(new java.awt.Font("Gulim", 0, 11));
		currentregimenJL.setForeground(new java.awt.Color(255, 255, 255));

		dispenseJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		dispenseJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Dispense icon.png"))); // NOI18N
		dispenseJBtn.setText("Dispense");
		dispenseJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				dispenseJBtnMouseClicked(evt);
			}
		});
		dispenseJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dispenseJBtnActionPerformed(evt);
			}
		});

		closeJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		closeJBtn.setIcon(new ImageIcon(ARTNumberJP.class.getResource("/elmis_images/new adjustmet.png"))); // NOI18N
		closeJBtn.setText("Change");
		closeJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				closeJBtnMouseClicked(evt);
			}
		});

		ARVdispenseJT1.setFont(new java.awt.Font("Ebrima", 0, 20));
		ARVdispenseJT1.setModel(tableModel_fproducts);
		ARVdispenseJT1
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ARVdispenseJT1PropertyChange(evt);
					}
				});
		jScrollPane3.setViewportView(ARVdispenseJT1);

		productQtyjLabel.setFont(new java.awt.Font("Segoe UI", 1, 15));
		productQtyjLabel.setForeground(new java.awt.Color(255, 255, 255));
		
		JButton btnClose = new JButton();
		btnClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt)  {
				btnCloseMouseClicked(evt);
			}

			
		});
		btnClose.setIcon(new ImageIcon(ARTNumberJP.class.getResource("/elmis_images/Cancel.png")));
		btnClose.setText("Close");
		btnClose.setFont(new Font("Ebrima", Font.BOLD, 12));

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4Layout.setHorizontalGroup(
			jPanel4Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addComponent(productQtyjLabel)
							.addContainerGap(688, Short.MAX_VALUE))
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(currentregimenJL, GroupLayout.DEFAULT_SIZE, 647, Short.MAX_VALUE)
								.addGroup(jPanel4Layout.createSequentialGroup()
									.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(69)
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
										.addGroup(jPanel4Layout.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(closeJBtn, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(dispenseJBtn, GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)))))
							.addGap(41))))
		);
		jPanel4Layout.setVerticalGroup(
			jPanel4Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup()
					.addComponent(productQtyjLabel)
					.addGap(5)
					.addComponent(currentregimenJL)
					.addPreferredGap(ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
					.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addComponent(dispenseJBtn, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(closeJBtn, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
						.addComponent(jScrollPane3, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		jPanel4.setLayout(jPanel4Layout);

		javax.swing.GroupLayout RegimenjPanelLayout = new javax.swing.GroupLayout(
				RegimenjPanel);
		RegimenjPanel.setLayout(RegimenjPanelLayout);
		RegimenjPanelLayout.setHorizontalGroup(RegimenjPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						RegimenjPanelLayout.createSequentialGroup()
								.addContainerGap().addComponent(jPanel4,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		RegimenjPanelLayout.setVerticalGroup(RegimenjPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						RegimenjPanelLayout.createSequentialGroup()
								.addContainerGap().addComponent(jPanel4,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));

		PatientARTJTP.addTab("Regimen Details", RegimenjPanel);

		jPanel5.setBackground(new java.awt.Color(102, 102, 102));
		jPanel5.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Register New Client"));

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Enter ART Number here:");

		jLabel5.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel5.setForeground(new java.awt.Color(255, 255, 255));
		jLabel5.setText("Enter NRC here:");

		NRCJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		NRCJT.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				NRCJTKeyPressed(evt);
			}
		});

		RegisterJBtn1.setFont(new java.awt.Font("Gulim", 1, 11));
		RegisterJBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Register icon.png"))); // NOI18N
		RegisterJBtn1.setText("Register");
		RegisterJBtn1.setMaximumSize(new java.awt.Dimension(159, 41));
		RegisterJBtn1.setMinimumSize(new java.awt.Dimension(159, 41));
		RegisterJBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				RegisterJBtn1MouseClicked(evt);
			}
		});
		RegisterJBtn1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				RegisterJBtn1ActionPerformed(evt);
			}
		});

		closeJBtn1.setFont(new java.awt.Font("Gulim", 1, 11));
		closeJBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		closeJBtn1.setText("Cancel");
		closeJBtn1.setMaximumSize(new java.awt.Dimension(159, 41));
		closeJBtn1.setMinimumSize(new java.awt.Dimension(159, 41));
		closeJBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				closeJBtn1MouseClicked(evt);
			}
		});

		ARTnumberJT1.setFont(new java.awt.Font("Ebrima", 0, 20));
		ARTnumberJT1.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				ARTnumberJT1KeyPressed(evt);
			}
		});

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(
				jPanel5);
		jPanel5Layout.setHorizontalGroup(
			jPanel5Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addGap(14)
							.addComponent(jLabel4)
							.addGap(200))
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addContainerGap()
							.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(NRCJT, GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
								.addComponent(jLabel5)))
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(ARTnumberJT1, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE)))
					.addGap(73)
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(RegisterJBtn1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(closeJBtn1, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
					.addGap(22))
		);
		jPanel5Layout.setVerticalGroup(
			jPanel5Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addComponent(jLabel4)
					.addGap(8)
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addComponent(ARTnumberJT1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(jLabel5))
						.addComponent(RegisterJBtn1, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
					.addGap(2)
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(NRCJT, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
						.addComponent(closeJBtn1, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(60, Short.MAX_VALUE))
		);
		jPanel5.setLayout(jPanel5Layout);

		javax.swing.GroupLayout RegisterjPanelLayout = new javax.swing.GroupLayout(
				RegisterjPanel);
		RegisterjPanel.setLayout(RegisterjPanelLayout);
		RegisterjPanelLayout.setHorizontalGroup(RegisterjPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						RegisterjPanelLayout.createSequentialGroup().addGap(20,
								20, 20).addComponent(jPanel5,
								javax.swing.GroupLayout.PREFERRED_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.PREFERRED_SIZE)
								.addContainerGap(74, Short.MAX_VALUE)));
		RegisterjPanelLayout.setVerticalGroup(RegisterjPanelLayout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						RegisterjPanelLayout.createSequentialGroup()
								.addContainerGap().addComponent(jPanel5,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE).addContainerGap()));

		PatientARTJTP.addTab("Client Registration", RegisterjPanel);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						PatientARTJTP, javax.swing.GroupLayout.PREFERRED_SIZE,
						676, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(554, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addGap(63, 63, 63).addComponent(
						PatientARTJTP, javax.swing.GroupLayout.PREFERRED_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));
	}// </editor-fold>
	//GEN-END:initComponents

	private void RegimenjPanelComponentShown(java.awt.event.ComponentEvent evt) {
		// TODO add your handling code here:

		//fetch the regimens for the patrient here .
		this.ARVdispenseJT1.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
		PopulateProgram_ProductTable(arvpatientdarproductsList);
		if ((!(setNRCtxtfield == null)) && (!(this.artnumber == null))) {
			this.currentregimenJL.setText("Current regimen for ART number :"
					+ artnumber + " nrc number : " + setNRCtxtfield);
			currentregimenJL.setFont(new Font("Ebrima", Font.PLAIN, 20));

		}
	}

	private void closeJBtn1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.PatientARTJTP.remove(RegisterjPanel);
	}

	private void NRCJTKeyPressed(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		//NRCJT.setText("");
		if(this.usenumberpad == true){
		new arvregisterNumberPadJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true, "NRCNUMBER");

		this.NRCJT.setText(arvregisterNumberPadJD.fillregisternrc);
		}else{
			//use keyboard
			//System.out.println("am ready to use keyboard now 2014!!");
             //if(!(NRCJT.getText() == "")){
				
			//}
		}
	}

	private void ARTnumberJT1KeyPressed(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		//pop up touch screen panel 
		//ARTnumberJT1.setText("");
					if(this.usenumberpad == true){
		new arvregisterNumberPadJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true, "ARTNUMBER");
		ARTnumberJT1.setText(arvregisterNumberPadJD.fillregister);
		
		}else{
			//use keyboard
			//System.out.println("am ready to use keyboard now 2014!!");
			//if(!(ARTnumberJT1.getText() == "")){
				
			//}
		}
	}
    public void setBoolean(boolean setkeyboradvalue){
    	this.usenumberpad = setkeyboradvalue;
    }
	private void ARTnumberJT2KeyPressed(java.awt.event.KeyEvent evt) {

	}

	private void JPaneldisplayed(java.awt.event.ComponentEvent evt) {
		// TODO add your handling code here:
		this.PatientARTJTP.remove(RegimenjPanel);
		this.PatientARTJTP.remove(RegisterjPanel);
		this.ARVdispenseJT1.setRowHeight(30);
	}

	private void ARTsearchJB1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		callarvdispenser = new facilityarvdispensingsessionsmappercalls();
		elmis_patient = new Elmis_Patient();
		//elmis_patient = callarvdispenser.selectARTnumber(this.ARTnumberJT.getText());
		try {
			if (!(this.ARTnumberJT2.getText().toString().equals(""))) {
				elmis_patient = callarvdispenser
						.selectARTnumber(this.ARTnumberJT2.getText());
				if (elmis_patient != null) {

					this.setNRCtxtfield = elmis_patient.getNrc_number();
					artnumber = elmis_patient.getArt_number();
					searchedARTnumber = artnumber;
					if (!(artnumber.equals(""))) {

					artfound = true;
					if (artfound == true) {
						this.ARTmessagedisplay.setText("ART Number found" + "="
								+ artnumber);
						this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
						//this.PatientARTJTP.
						this.PatientARTJTP.setSelectedIndex(1);
						JOptionPane.showMessageDialog(this,
								"ART number found - Patient exists!", artnumber,
								JOptionPane.INFORMATION_MESSAGE);
					}
				}
				}else{
					//patient object is null 
					
					if (artnumber.equals("")) {

					artfound = false;
					if (artfound == false) {
						this.ARTmessagedisplay.setText("ART Number  not found !");
						int ok = new arvmessageDialog()
						.showDialog(
								this,
								"Do  you want to Registerclient on ART\n\n",
								"Process Client details",
								"YES, To \n Register",
								"NO, To\n Exit and consult Clinician ", "Cancel");
						
				

						if (ok == JOptionPane.YES_OPTION) {
							
							this.PatientARTJTP.addTab("Client Registration",
									RegisterjPanel);
							this.PatientARTJTP.setSelectedIndex(1);
						} else if (ok == JOptionPane.NO_OPTION) {
							//this.PatientARTJTP.removeTabAt(2);
						}
					}
				}

					
				}
			}
			/*if (!(artnumber.equals(""))) {

				artfound = true;
				if (artfound == true) {
					this.ARTmessagedisplay.setText("ART Number found" + "="
							+ artnumber);
					this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
					//this.PatientARTJTP.
					this.PatientARTJTP.setSelectedIndex(1);
					JOptionPane.showMessageDialog(this,
							"ART number found - Patient exists!", artnumber,
							JOptionPane.INFORMATION_MESSAGE);
				}
			}*/
		/*	if (artnumber.equals("")) {

				artfound = false;
				if (artfound == false) {
					this.ARTmessagedisplay.setText("ART Number  not found !");
					int ok = new arvmessageDialog()
					.showDialog(
							this,
							"Do  you want to Registerclient on ART\n\n",
							"Process Client details",
							"YES, To \n Register",
							"NO, To\n Exit and consult Clinician ", "Cancel");
					
					int confirm = javax.swing.JOptionPane.showConfirmDialog(
							this, "Do  you want to register patient on ART\n\n"
									+ "Yes - to Register\n\n"
									+ "No -to Exit and consult Clinician\n\n",

							"Start patient ART registration",
							JOptionPane.YES_NO_OPTION);

					if (ok == JOptionPane.YES_OPTION) {
						
						this.PatientARTJTP.addTab("Client Registration",
								RegisterjPanel);
						this.PatientARTJTP.setSelectedIndex(1);
					} else if (ok == JOptionPane.NO_OPTION) {
						//this.PatientARTJTP.removeTabAt(2);
					}
				}
			}*/

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

	private void closeJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call dispense form 
		//dispenseReceivingJD
		AppJFrame.glassPane.activate(null);
		new dispenseReceivingJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();

	}
	
	private void btnCloseMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		this.ARTmessagedisplay.setText("");
		ARTnumberJT2.setText("");
		ARTnumberJT2.requestFocus();
		searchedARTnumber = "";
		artnumber = "";
		this.currentregimenJL.setText("");
		arvpatientdarproductsList.clear();
		this.PatientARTJTP.remove(RegimenjPanel);
		
	}
	private void ARVdispenseJT1PropertyChange(java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		ARVdispenseJT1.getColumnModel().getColumn(0).setMinWidth(0);
		ARVdispenseJT1.getColumnModel().getColumn(0).setMaxWidth(0);
		ARVdispenseJT1.getColumnModel().getColumn(0).setWidth(0);
		//setting changes for columns 
		ARVdispenseJT1.getColumnModel().getColumn(1).setMinWidth(0);
		ARVdispenseJT1.getColumnModel().getColumn(1).setMaxWidth(0);
		ARVdispenseJT1.getColumnModel().getColumn(1).setWidth(0);

		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			/*	if (this.ARVdispenseJT1.isColumnSelected(4)) {

				}
				if (this.facilityapprovedProductsJT.isColumnSelected(5)) {

				}*/
			if (this.ARVdispenseJT1.isColumnSelected(3)) {
				if (ARVdispenseJT1.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					colindex = this.ARVdispenseJT1.getSelectedColumn();
					rowindex = this.ARVdispenseJT1.getSelectedRow();
					Pcode = tableModel_fproducts.getValueAt(
							this.ARVdispenseJT1.getSelectedRow(), 0).toString();

				} else if (!ARVdispenseJT1.isEditing()) {

					try {
						createshipmentObj(Pcode, shipmentdate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println("THIS CELL IS NOT EDITING");

				}
			}
		}
	}

	public void createshipmentObj(String apcode, Timestamp amodifieddate)
			throws ParseException {
		//System.out.println("Did object call me ???");

		elmis_dar_transactions = new Elmis_Dar_Transactions();

		if (productdeliverdate == null) {
			java.util.Date date = new java.util.Date();
			amodifieddate = new Timestamp(date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}

		Integer ARVbottle = 0;

		elmis_dar_transactions.setId(this.GenerateGUID());
		elmis_dar_transactions.setProductcode(apcode);

		this.PACKSIZE = Integer.parseInt(tableModel_fproducts.getValueAt(
				rowindex, 1).toString());
		Convert_tablet_to_bottle = Integer.parseInt(tableModel_fproducts
				.getValueAt(rowindex, 3).toString());

		if (!(this.PACKSIZE == null)) {
			if (!(Convert_tablet_to_bottle == null)) {

				ARVbottle = Convert_tablet_to_bottle / PACKSIZE;
				elmis_dar_transactions.setNo_equiv_bottles(ARVbottle);

			}

		}
		elmis_dar_transactions
				.setNo_tablets_dispensed(Convert_tablet_to_bottle);
		elmis_dar_transactions.setTransaction_date(amodifieddate);

		if (!(this.artnumber == null)) {
			elmis_dar_transactions.setArt_number(artnumber);
		}
		//created linked list object 

		elmisstockcontrolcardList.add(elmis_dar_transactions);
		System.out.println(elmisstockcontrolcardList.size() + "ARRAY SIZE???");
		System.out.println("Array size here &&&");
		elmis_dar_transactions = new Elmis_Dar_Transactions();

	}

	public void populateClientregistertxtfields(String now, String fillme) {
		if (now == "ARTNUMBER") {
			this.ARTnumberJT1.setText(fillme);

			this.PatientARTJTP.addTab("Client Registration", RegisterjPanel);
		}
		if (now == "NRCNUMBER") {
			this.NRCJT.setText(fillme);
		}
	}

	private void PatientARTJTP(java.awt.event.FocusEvent evt) {
		// TODO add your handling code here:
		/*Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
		PopulateProgram_ProductTable(arvpatientdarproductsList);*/
	}

	private void PopulateProgram_ProductTable(List fapprovProducts) {

		try {
			callfacility = new facilitysetupsessionsmappercalls();
			Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
			callarvdispenser = new facilityarvdispensingsessionsmappercalls();

			this.facilityprogramcode = "ARV";
			//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

			facility = callfacility.getFacility();

			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();
			if (!(searchedARTnumber.equals(""))) {
				arvpatientdarproductsList = callarvdispenser
						.doselectpatientarvDispensation(this.searchedARTnumber);
			}
			for (@SuppressWarnings("unused")
			Elmis_Dar_Transactions p : arvpatientdarproductsList) {
				System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getProductcode().toString());
				System.out.println(p.getArt_number());
				System.out.println(p.getNo_equiv_bottles());

			}
			tableModel_fproducts.clearTable();

			arvpatientdarproductsIterator = arvpatientdarproductsList
					.listIterator();

			while (arvpatientdarproductsIterator.hasNext()) {

				arvpatientdarProducts = arvpatientdarproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_facilityapprovedproducts[0] = arvpatientdarProducts
						.getProductcode().toString();

				defaultv_facilityapprovedproducts[1] = arvpatientdarProducts
						.getArt_number().toString();

				defaultv_facilityapprovedproducts[2] = arvpatientdarProducts
						.getPrimaryname().toString();

				defaultv_facilityapprovedproducts[3] = arvpatientdarProducts
						.getNo_tablets_dispensed();
				/*	TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());*/

				//defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				arvpatientdarproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	private Timestamp clientregisterdate() throws ParseException {
		try {
			DateFormat dateFormat = new SimpleDateFormat(
					"EEE MMM d HH:mm:ss z yyyy");
			Date date = new Date();
			dateFormat.format(date);

			String mydate = dateFormat.format(date);
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";

			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);

			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		}
		return productdeliverdate;
	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	private void RegisterJBtn1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		callarvdispenser = new facilityarvdispensingsessionsmappercalls();
		elmis_patient = new Elmis_Patient();

		elmis_patient.setId(this.GenerateGUID());
		elmis_patient.setArt_number(this.ARTnumberJT1.getText());
		elmis_patient.setNrc_number(this.NRCJT.getText());
		try {
			elmis_patient.setRegistration_date(clientregisterdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		callarvdispenser.Registerpatient(elmis_patient);
		elmis_patient = new Elmis_Patient();
		searchedARTnumber = this.ARTnumberJT1.getText();
		JOptionPane.showMessageDialog(this, "Registration complete !",
				"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
		//call dispense form 
		//dispenseReceivingJD
		AppJFrame.glassPane.activate(null);
		new dispenseReceivingJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);
		AppJFrame.glassPane.deactivate();
	}

	private void RegisterJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void dispenseJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void dispenseJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
				"Do  you want to save the Received Quantities to the database\n\n"
						+ "Yes - to Save\n\n"
						+ "No -to Edit the Quantity Entered\n\n"
						+ "Cancel - to exit Dialog",
				"Saving Quantity Received Information",
				JOptionPane.YES_NO_OPTION);
		callarvdispenser = new facilityarvdispensingsessionsmappercalls();
		if (confirm == JOptionPane.YES_OPTION) {
			//Insert the object to database
			saveelmis_dar_transactions = new Elmis_Dar_Transactions();
			for (@SuppressWarnings("unused")
			Elmis_Dar_Transactions sp : elmisstockcontrolcardList) {

				saveelmis_dar_transactions.setId(sp.getId());
				saveelmis_dar_transactions.setNo_tablets_dispensed(sp
						.getNo_tablets_dispensed());
				saveelmis_dar_transactions.setNo_equiv_bottles(sp
						.getNo_equiv_bottles());
				saveelmis_dar_transactions.setProductcode(sp.getProductcode());
				saveelmis_dar_transactions.setArt_number(sp.getArt_number());
				saveelmis_dar_transactions.setTransaction_date(sp
						.getTransaction_date());

				callarvdispenser
						.insertdispensedProducts(saveelmis_dar_transactions);
			}
			JOptionPane.showMessageDialog(this,
					"Products Dispensed from Dispensary!", "Updating Database",
					JOptionPane.INFORMATION_MESSAGE);
			this.PatientARTJTP.remove(RegimenjPanel);
			this.artnumber = "";
			this.ARTmessagedisplay.setVisible(false);
			this.ARTnumberJT2.repaint();
			this.ARTnumberJT2.requestFocus();
			
		} else if (confirm == JOptionPane.NO_OPTION) {
			this.ARVdispenseJT1.validate();
		}

	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel ARTmessagedisplay;
	private javax.swing.JTextField ARTnumberJT1;
	private javax.swing.JTextField ARTnumberJT2;
	private javax.swing.JButton ARTsearchJB1;
	private javax.swing.JTable ARVdispenseJT1;
	private javax.swing.JTextField NRCJT;
	private javax.swing.JTabbedPane PatientARTJTP;
	private javax.swing.JPanel RegimenjPanel;
	private javax.swing.JButton RegisterJBtn1;
	private javax.swing.JPanel RegisterjPanel;
	private javax.swing.JPanel SearchJP;
	private javax.swing.JButton closeJBtn;
	private javax.swing.JButton closeJBtn1;
	private javax.swing.JLabel currentregimenJL;
	private javax.swing.JButton dispenseJBtn;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JPanel jPanel7;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JLabel productQtyjLabel;

	// End of variables declaration//GEN-END:variables

	/***************************************************
	 * //start of child class
	 * ***********************************************/

	public static class arvregisterNumberPadJD extends javax.swing.JDialog {

		//private static int numberOfItems = 0;
		//private static String barcodeIn;

		private List<Character> charList = new LinkedList();
		private char c;
		private String ARTnumberstr = "";
		private Products product;
		private String Numberpadcaller;
		//	private ProductQty qty;
		public static String fillregister = null;
		public static String fillregisternrc;
		public String fillsubstr;
		public String filldistsubstr;
		public String fillcitizensubsstr;
		public ARTNumberJP artnumberjp;
		public Boolean usekeyboard = false;

		/** Creates new form NumberPadJD */
		/** Creates new form NumberPadJD */
		public arvregisterNumberPadJD(java.awt.Frame parent, boolean modal, String src) {
			super(parent, modal);
			initComponents();

			Numberpadcaller = src;
			//	this.qtyJTF.setText(numberOfItems + "");

			//this.qtyJTF.setText(qty.getQty() + "");

			this.setSize(320,490);
			this.setLocationRelativeTo(null);

			this.getRootPane().setDefaultButton(okJBtn);
			this.qtyJTF.setFocusable(false);
			/*this.subtractJBtn.setFocusable(false);
			this.deleteJBtn.setFocusable(false);
			this.jButton1.setFocusable(false);
			this.jButton2.setFocusable(false);
			this.jButton3.setFocusable(false);
			this.jButton4.setFocusable(false);
			this.jButton5.setFocusable(false);
			this.jButton6.setFocusable(false);
			this.jButton7.setFocusable(false);
			this.jButton8.setFocusable(false);
			this.jButton9.setFocusable(false);
			this.jButton10.setFocusable(false);*/
			//this.jButton1.setFocusable(false);

			this.setVisible(true);

		}

		/**
		 * @wbp.parser.constructor
		 */
		public arvregisterNumberPadJD(java.awt.Frame parent, boolean modal) {
			super(parent, modal);
			initComponents();
			//this.barcodeIn = IssuingJD.barcodeScanned;

			/*	if (!this.product.getPrimaryname().equals("")) {

					//if the name is too long cut it short for display purpose
					if (this.product.getPrimaryname().length() >= 40) {

						productNameJL.setText(this.product.getPrimaryname().substring(
								0, 40));
					} else {

						productNameJL.setText(this.product.getPrimaryname());
					}
				}

				this.qtyInStockJL.setText(this.qty.getQty() + "");*/
			//	this.qtyJTF.setText(numberOfItems + "");

			//this.qtyJTF.setText(qty.getQty() + "");
			this.setSize(332, 647);
			this.setLocationRelativeTo(null);

			this.getRootPane().setDefaultButton(okJBtn);
			this.qtyJTF.setFocusable(false);
			/*this.subtractJBtn.setFocusable(false);
			this.deleteJBtn.setFocusable(false);
			this.jButton1.setFocusable(false);
			this.jButton2.setFocusable(false);
			this.jButton3.setFocusable(false);
			this.jButton4.setFocusable(false);
			this.jButton5.setFocusable(false);
			this.jButton6.setFocusable(false);
			this.jButton7.setFocusable(false);
			this.jButton8.setFocusable(false);
			this.jButton9.setFocusable(false);
			this.jButton10.setFocusable(false);*/
			//this.jButton1.setFocusable(false);

			this.setVisible(true);

		}

		//GEN-BEGIN:initComponents
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jPanel1 = new javax.swing.JPanel();
			jPanel2 = new javax.swing.JPanel();
			jButton1 = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();
			jButton3 = new javax.swing.JButton();
			jButton4 = new javax.swing.JButton();
			jButton5 = new javax.swing.JButton();
			jButton6 = new javax.swing.JButton();
			jButton7 = new javax.swing.JButton();
			jButton8 = new javax.swing.JButton();
			jButton9 = new javax.swing.JButton();
			jButton10 = new javax.swing.JButton();
			okJBtn = new javax.swing.JButton();
			deleteJBtn = new javax.swing.JButton();
			subtractJBtn = new javax.swing.JButton();
			addJBtn = new javax.swing.JButton();
			qtyJTF = new javax.swing.JTextField();
		    btnusekeyboard =  new javax.swing.JButton();
			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

			jPanel1.setBackground(new java.awt.Color(153, 153, 153));
			jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(
					javax.swing.border.BevelBorder.RAISED));

			jPanel2.setBackground(new java.awt.Color(102, 102, 102));
			jPanel2.setBorder(javax.swing.BorderFactory
					.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

			jButton1.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton1.setText("1");
			jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton1MouseClicked(evt);
				}
			});
			jButton1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton1ActionPerformed(evt);
				}
			});

			jButton2.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton2.setText("2");
			jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton2MouseClicked(evt);
				}
			});
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			jButton3.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton3.setText("3");
			jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton3MouseClicked(evt);
				}
			});
			jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton3ActionPerformed(evt);
				}
			});

			jButton4.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton4.setText("4");
			jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton4MouseClicked(evt);
				}
			});
			jButton4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton4ActionPerformed(evt);
				}
			});

			jButton5.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton5.setText("5");
			jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton5MouseClicked(evt);
				}
			});
			jButton5.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton5ActionPerformed(evt);
				}
			});

			jButton6.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton6.setText("6");
			jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton6MouseClicked(evt);
				}
			});
			jButton6.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton6ActionPerformed(evt);
				}
			});

			jButton7.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton7.setText("7");
			jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton7MouseClicked(evt);
				}
			});
			jButton7.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton7ActionPerformed(evt);
				}
			});

			jButton8.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton8.setText("8");
			jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton8MouseClicked(evt);
				}
			});
			jButton8.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton8ActionPerformed(evt);
				}
			});

			jButton9.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton9.setText("9");
			jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton9MouseClicked(evt);
				}
			});
			jButton9.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton9ActionPerformed(evt);
				}
			});

			jButton10.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton10.setText("0");
			jButton10.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton10MouseClicked(evt);
				}
			});
			jButton10.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton10ActionPerformed(evt);
				}
			});

			okJBtn.setBackground(new java.awt.Color(0, 153, 0));
			okJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
			okJBtn.setForeground(new java.awt.Color(153, 0, 0));
			okJBtn.setText("OK");
			okJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					okJBtnActionPerformed(evt);
				}
			});
			okJBtn.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent evt) {
					okJBtnFocusLost(evt);
				}
			});
			deleteJBtn.setFont(new java.awt.Font("Ebrima", 0, 18));
			deleteJBtn.setText("DEL");
			deleteJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					deleteJBtnActionPerformed(evt);
				}
			});

			subtractJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
			subtractJBtn.setIcon(new javax.swing.ImageIcon(getClass()
					.getResource("/images/sort_descending.png"))); // NOI18N
			subtractJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					subtractJBtnActionPerformed(evt);
				}
			});
			//JButton btnusekeyboard = new JButton("use keyboard");
			btnusekeyboard.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent arg0) {
					//set form variable
					
				}
			});
			btnusekeyboard.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					btnusekeyboardActionPerformed(evt);
				}
			});
			

			addJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
			addJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
					"/images/sort_ascending.png"))); // NOI18N
			addJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					addJBtnActionPerformed(evt);
				}
			});
			addJBtn.addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyTyped(java.awt.event.KeyEvent evt) {
					addJBtnKeyTyped(evt);
				}
			});

			javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
					jPanel2);
			jPanel2.setLayout(jPanel2Layout);
			jPanel2Layout
					.setHorizontalGroup(jPanel2Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel2Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.TRAILING)
															.addComponent(
																	jButton1,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	jButton4,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	jButton7,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	subtractJBtn,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	deleteJBtn,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	addJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	146,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addGroup(
																	jPanel2Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel2Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									jButton10,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									70,
																									Short.MAX_VALUE)
																							.addComponent(
																									jButton8,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									67,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton5,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									67,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton2,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									67,
																									javax.swing.GroupLayout.PREFERRED_SIZE))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addGroup(
																					jPanel2Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									okJBtn,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton9,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton6,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton3,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
											.addContainerGap()));

			jPanel2Layout
					.linkSize(javax.swing.SwingConstants.HORIZONTAL,
							new java.awt.Component[] { deleteJBtn, jButton1,
									jButton10, jButton2, jButton3, jButton4,
									jButton5, jButton6, jButton7, jButton8,
									jButton9, okJBtn, subtractJBtn });

			jPanel2Layout
					.setVerticalGroup(jPanel2Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel2Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	addJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	subtractJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton3,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton2,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton1,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton6,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton5,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton4,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton9,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton8,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton7,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	okJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton10,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	deleteJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addContainerGap(
													javax.swing.GroupLayout.DEFAULT_SIZE,
													Short.MAX_VALUE)));

			jPanel2Layout
					.linkSize(javax.swing.SwingConstants.VERTICAL,
							new java.awt.Component[] { deleteJBtn, jButton1,
									jButton10, jButton2, jButton3, jButton4,
									jButton5, jButton6, jButton7, jButton8,
									jButton9, okJBtn, subtractJBtn });

			qtyJTF.setFont(new java.awt.Font("Ebrima", 0, 14));
			qtyJTF.addFocusListener(new java.awt.event.FocusAdapter() {

				public void focusLost(java.awt.event.FocusEvent evt) {
					qtyJTFFocusLost(evt);
				}
			});
			qtyJTF.addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyTyped(java.awt.event.KeyEvent evt) {
					qtyJTFKeyTyped(evt);
				}
			});
			
			usekeyboardJbtn = new JButton("Use keyboard");
			usekeyboardJbtn.setIcon(new ImageIcon(arvNumberPadJD.class.getResource("/elmis_images/Repeat.png")));
			usekeyboardJbtn.addMouseListener(new MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						usekeyboardJbtnMouseClicked(evt);
				}
			});

			javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
					jPanel1);
			jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(Alignment.LEADING)
					.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
							.addComponent(jPanel2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
							.addComponent(qtyJTF, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
							.addComponent(usekeyboardJbtn, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
						.addContainerGap())
			);
			jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(Alignment.LEADING)
					.addGroup(jPanel1Layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(qtyJTF, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(usekeyboardJbtn, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
						.addGap(384))
			);
			jPanel1.setLayout(jPanel1Layout);

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			layout.setHorizontalGroup(
				layout.createParallelGroup(Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(27, Short.MAX_VALUE))
			);
			layout.setVerticalGroup(
				layout.createParallelGroup(Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 574, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(301, Short.MAX_VALUE))
			);
			getContentPane().setLayout(layout);

			pack();
		}// </editor-fold>

		private void usekeyboardJbtnMouseClicked(MouseEvent evt) {
			// TODO Auto-generated method stub
			System.out.println("close the form now");
			usekeyboard = false;
			usenumberpad = usekeyboard;
			System.out.println(usekeyboard + "~~~");
			System.out.println(usenumberpad + "is now" );
			this.dispose();
		}

		//GEN-END:initComponents

		private void jButton10MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton10.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}
		private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton9.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:

			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton8.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton7MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton7.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton6.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton5.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton4.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:

			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton3.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton2.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton1.getText();
			this.qtyJTF.setText(this.ARTnumberstr);

		}

		private void addJBtnKeyTyped(java.awt.event.KeyEvent evt) {
			// TODO add your handling code here:

			c = evt.getKeyChar();

			charList.add(c);

		}

		private void okJBtnFocusLost(java.awt.event.FocusEvent evt) {
			// TODO add your handling code here:
			okJBtn.requestFocus();
		}

		private void addJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:

		}

		private void subtractJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			if (!qtyJTF.getText().equals("")) {

				qtyJTF.setText(qtyJTF.getText().substring(0,
						qtyJTF.getText().length() - 1));

			}
		}
		
		private void btnusekeyboardActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			
			usekeyboard = true;
			//System.out.println("close the child form");
			//System.out.println("Test Close form");
			//arvNumberPadJD.this.dispose();
	     
		}

		private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButton18A7ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButton1n15ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButtoton13ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButleteJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void okJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//check source of call
	    System.out.println("Check OK button !");
			if (this.Numberpadcaller.equals("ARTNUMBER")) {

				fillregister = this.ARTnumberstr;

			} else if (this.Numberpadcaller.equals("NRCNUMBER")) {
				@SuppressWarnings("unused")
				String split = "/";
				this.fillregister = this.ARTnumberstr;
				fillsubstr = this.fillregister.substring(0, 6);
				filldistsubstr = this.fillregister.substring(6, 8);
				fillcitizensubsstr = this.fillregister.substring(8, 9);
				this.fillregisternrc = fillsubstr + split + filldistsubstr
						+ split + fillcitizensubsstr;

			}
			//arvNumberPadJD.this.setDefaultCloseOperation(HIDE_ON_CLOSE);
			arvregisterNumberPadJD.this.dispose();

		}

		public String getData() {

			return qtyJTF.getText();

		}

		private void qtyJTFFocusLost(java.awt.event.FocusEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.requestFocusInWindow();
		}

		private void deleteJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			if (!qtyJTF.getText().equals("")) {

				qtyJTF.setText("");
				this.ARTnumberstr = "";

			}
		}

		private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "0");

			getStockLimit(0);

		}

		private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "9");
			getStockLimit(9);

		}

		private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "8");
			getStockLimit(8);
		}

		private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "7");

			getStockLimit(7);
		}

		private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "6");
			getStockLimit(6);
		}

		private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "5");
			getStockLimit(5);
		}

		private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "4");
			getStockLimit(4);
		}

		private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "3");
			getStockLimit(3);
		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "2");
			getStockLimit(2);
		}

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

			// TODO add your handling code here:
			//qtyJTF.setText(qtyJTF.getText() + "1");

			//getStockLimit(1);

			this.qtyJTF.setFocusable(true);

		}

		private void qtyJTFKeyTyped(java.awt.event.KeyEvent evt) {
			// TODO add your handling code here:

			final char c = evt.getKeyChar();
			if (!(Character.isDigit(c) || (c == KeyEvent.VK_PERIOD)
					|| (c == KeyEvent.VK_COMMA)
					|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
				this.getToolkit().beep();
				evt.consume();
			}
		}

		private void getStockLimit(int qtyToAdd) {

			//int qtyInStock = Integer.parseInt(qtyInStockJL.getText());
			//int requestedQty = Integer.parseInt(qtyJTF.getText() + qtyToAdd);

			/*if (requestedQty > qtyInStock) {

				javax.swing.JOptionPane.showMessageDialog(this, "Over limit ");
			} else if (requestedQty <= qtyInStock) {

				//javax.swing.JOptionPane.showMessageDialog(this, "Qty requested "
				//		+ requestedQty + "\nStock " + qtyInStock + "\nQty is JTF "
				//		+ qtyJTF.getText());

				qtyJTF.setText(qtyJTF.getText() + qtyToAdd);
			}*/

			//requestedQty = 0;
			//return overLimit;

		
		}
		/**
		 * @param args the command line arguments
		 */
		/*	public static void main(String args[]) {
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
						arvNumberPadJD dialog = new arvNumberPadJD(
								new javax.swing.JFrame(), true);
						dialog.addWindowListener(new java.awt.event.WindowAdapter() {
							public void windowClosing(java.awt.event.WindowEvent e) {
								System.exit(0);
							}
						});
						dialog.setVisible(true);
					}
				});
			}*/

		

			//GEN-BEGIN:variables
			// Variables declaration - do not modify
			private javax.swing.JButton addJBtn;
			private javax.swing.JButton deleteJBtn;
			private javax.swing.JButton jButton1;
			private javax.swing.JButton jButton10;
			private javax.swing.JButton jButton2;
			private javax.swing.JButton jButton3;
			private javax.swing.JButton jButton4;
			private javax.swing.JButton jButton5;
			private javax.swing.JButton jButton6;
			private javax.swing.JButton jButton7;
			private javax.swing.JButton jButton8;
			private javax.swing.JButton jButton9;
			private javax.swing.JPanel jPanel1;
			private javax.swing.JPanel jPanel2;
			private javax.swing.JButton okJBtn;
			private javax.swing.JTextField qtyJTF;
			private javax.swing.JButton subtractJBtn;
			private javax.swing.JButton btnusekeyboard;
			private JButton usekeyboardJbtn;
			// End of variables declaration//GEN-END:variables

		}
}
