/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.controllers.facilityproductssetupsessionmapperscalls;
import org.elmis.facility.controllers.facilitysetupsessionsmappercalls;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Store_Physical_Count;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;

import com.oribicom.tools.TableModel;

/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class arvdispensingProductsPhysicalCountJD extends javax.swing.JDialog {

	TableColumnAligner tablecolumnaligner;
	facilitysetupsessionsmappercalls callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;

	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> searcharvproductsList = new LinkedList();
	List<VW_Systemcalculatedproductsbalance> productsPhysicalcountList = new LinkedList();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();

	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	private Date Startdate;
	private Date Enddate;
	public Boolean productsearchenabled = false;
	public Boolean calltracer = false;
	public Boolean saveresponse = false;

	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
		"Product Code",
			"Product name",
			"Generic Strength ",
			"Physical Count" ,"Remarks"};
	
	private static final String[] columns_searchfacilityApprovedproducts = {
		"Product Code",
		"Product name",
		"Generic Strength ",
		"Physical Count","Remarks"};
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "", "" };
	private static final Object[] defaultv_searchfacilityapprovedproducts = { "", "",
		"", "", "" };
	private static final int rows_fproducts = 0;
	private static final int rows_searchfproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 5) {

				return getValueAt(0,5).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_searchfacilityApprovedproducts,
			defaultv_searchfacilityapprovedproducts, rows_searchfproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {

					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 5) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,5).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/*public Class getColumnClass(int c) 
		{     
		for(int rowIndex = 0; rowIndex < data.size(); rowIndex++)
		{
		    Object[] row = data.get(rowIndex);
		    if (row[c] != null) {
		        return getValueAt(rowIndex, c).getClass();
		    }   
		}
		return String.class;
		}*/

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsbalanceList = new LinkedList();
	ArrayList<Store_Physical_Count> sccproductbalanceList = new ArrayList<Store_Physical_Count>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> productbalanceprogramsList = new LinkedList();
	//ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator sccproductbalanceiterator = productsbalanceList.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProductsbalance;
	private Products facilitysccproduct;
	facilityproductssetupsessionmapperscalls Facilityproductsmapper = null;
	private Timestamp shipmentdate;
	private Store_Physical_Count storephysicalcount;
	private Store_Physical_Count savestorephysicalcount;
	//private Shipped_Line_Items shipped_line_items;
	//private Shipped_Line_Items saveShipped_Line_Items;
	//private VW_Systemcalculatedproductsbalance sccproductelmis_stock_control_card;
	public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int rnrid;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String programname = "";

	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();

	private JComboBox balanceProgramList = new JComboBox();
	MutableComboBoxModel modelbalanceProgramslist = (MutableComboBoxModel) balanceProgramList
			.getModel();

	/** Creates new form receiveProducts */
	public arvdispensingProductsPhysicalCountJD(java.awt.Frame parent,
			boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(1000, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.ShipmentdateJDate.setPreferredSize(new Dimension(150, 20));
		balanceProgramTypeJCB.setPreferredSize(new Dimension(150, 20));
		SearchproductsJTF.setPreferredSize(new Dimension(150, 20));
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		facilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();
		label1 = new java.awt.Label();
		SearchproductsJTF = new java.awt.TextField();
		ShipmentdateJDate = new com.toedter.calendar.JDateChooser();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel2.setVisible(false);
		balanceProgramTypeJCB = new javax.swing.JComboBox();
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Product Physical Count");
		setBackground(new java.awt.Color(51, 51, 255));
		setFont(new java.awt.Font("Ebrima", 0, 20));
		setForeground(new java.awt.Color(204, 153, 0));
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		facilityapprovedProductsJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		facilityapprovedProductsJT.setModel(tableModel_fproducts);
		facilityapprovedProductsJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						facilityapprovedProductsJTMouseClicked(evt);
					}
				});
		facilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						facilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(facilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBtn.setText("Close");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));

		label1.setFont(new java.awt.Font("Ebrima", 1, 12));
		label1.setForeground(new java.awt.Color(255, 255, 255));
		label1.setText("Search");

		SearchproductsJTF.setFont(new java.awt.Font("Ebrima", 0, 20));
		SearchproductsJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				SearchproductsJTFKeyPressed(evt);
			}

			public void keyTyped(java.awt.event.KeyEvent evt) {
				SearchproductsJTFKeyTyped(evt);
			}
		});

		ShipmentdateJDate.setDateFormatString(mydateformat);
		ShipmentdateJDate.setFont(new java.awt.Font("Ebrima", 0, 20));
		ShipmentdateJDate
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Physical count date");

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Program Area");

		balanceProgramTypeJCB.setFont(new java.awt.Font("Ebrima", 0, 18));
		balanceProgramTypeJCB.setModel(modelbalanceProgramslist);
		balanceProgramTypeJCB
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						balanceProgramTypeJCBActionPerformed(evt);
					}
				});
		balanceProgramTypeJCB
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						balanceProgramTypeJCBPropertyChange(evt);
					}
				});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout
				.setHorizontalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(jLabel1)
														.addComponent(jLabel2))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																false)
														.addComponent(
																balanceProgramTypeJCB,
																0,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																ShipmentdateJDate,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																246,
																Short.MAX_VALUE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												140, Short.MAX_VALUE)
										.addComponent(
												label1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												SearchproductsJTF,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												214,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(13, 13, 13)));
		jPanel2Layout
				.setVerticalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap(27, Short.MAX_VALUE)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																SearchproductsJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																36,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																label1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addGroup(
																jPanel2Layout
																		.createSequentialGroup()
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel1)
																						.addComponent(
																								ShipmentdateJDate,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								35,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel2,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								26,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								balanceProgramTypeJCB,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								39,
																								javax.swing.GroupLayout.PREFERRED_SIZE))))));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel2,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel1Layout
										.createSequentialGroup()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addGap(
																				10,
																				10,
																				10)
																		.addComponent(
																				jScrollPane1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				781,
																				Short.MAX_VALUE))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				CancelJBtn)
																		.addGap(
																				15,
																				15,
																				15)
																		.addComponent(
																				SaveJBtn)))
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(49, 49, 49)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												361,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(18, 18, 18)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																CancelJBtn)
														.addComponent(SaveJBtn))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void SearchproductsJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:
				productsearchenabled  = true ;
				System.out.println(this.arvsearchproductsList.size() + "list not null");

				letter = evt.getKeyChar();

				StringBuilder s = new StringBuilder(charList.size());

				if ((letter == KeyEvent.VK_BACK_SPACE)
						|| (letter == KeyEvent.VK_DELETE)) {

					// do nothing
					searchText = this.SearchproductsJTF.getText();

					if (charList.size() > 0) {

						charList.remove(charList.size() - 1);

					}

					if (charList.size() <= 0) {

						s = new StringBuilder(charList.size());
						charList.clear();
					}

					for (char c : charList) {

						s.append(c);

					}
				} else {

					charList.add(letter);

					for (char c : charList) {

						s.append(c);

					}

					//get ARV list 
					try {
						if (!(this.SearchproductsJTF.getText() == "")) {

							if (!(s.substring(0, 1).matches("[0-9]"))) {

								callfacility = new facilitysetupsessionsmappercalls();
								Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

								this.facilityprogramcode = "ARV";
								//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
								//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

								facility = callfacility.getFacility();

								facilitytype = callfacility
										.selectAllwithTypes(facility);
								typeCode = facilitytype.getCode();

								productsList = Facilityproductsmapper
										.dogetcurrentFacilityApprovedProducts(
												facilityprogramcode, typeCode);
								arvsearchproductsList = productsList;

								/*for(VW_Program_Facility_ApprovedProducts p: arvsearchproductsList ){
									
									if(p.getPrimaryname().contains(s)){
										this.arvproductsList.add(p);
									}
								}*/

								for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

									if (p.getPrimaryname().contains(s)) {
										this.arvproductsList.add(p);
									}
								}

								//	System.out.println("Delete " + s);
								//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

								this.searchPopulateProgram_ProductTable(
										arvproductsList, "STRING_FOUND");

							}
						}

					} catch (NullPointerException e) {
						e.getMessage();
					}

				}

				// searchText += Character.toString(letter);
				/*	charList.add(letter);

					for (char c : charList) {

						s.append(c);

					}

					try {
						//check if char is a digit and escape the loop.
						if(!(this.searchjTextField.getText() == "")){
						if(!(s.substring(0,1).matches("[0-9]"))){ 

						callfacility = new facilitysetupsessionsmappercalls();
						Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

						this.facilityprogramcode = "ARV";
						//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
						//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

						facility = callfacility.getFacility();

						facilitytype = callfacility.selectAllwithTypes(facility);
						typeCode = facilitytype.getCode();

						productsList = Facilityproductsmapper.dogetcurrentFacilityApprovedProducts(facilityprogramcode, typeCode);
							
						arvsearchproductsList = productsList;

						for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

							if (p.getPrimaryname().contains(s)) {
								this.arvproductsList.add(p);
							}
						}
						//System.out.println("Delete " + s);
						//arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						searchPopulateProgram_ProductTable(arvproductsList,
								"STRING_FOUND");
						
						}
						
					}
					} catch (NullPointerException e) {
						e.getMessage();
					}*/
	}

	private void SearchproductsJTFKeyPressed(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
	}

	private void balanceProgramTypeJCBPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
	}

	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

			/*	callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : searcharvproductsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
  
			this.facilityapprovedProductsJT
					.setModel(tableModel_searchfproducts);
			tableModel_searchfproducts.clearTable();

			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength().toString();
				/*defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getPacksize();*/
				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				/*	TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());*/

				//defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	public static Date toDate(java.sql.Timestamp timestamp) {
		long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		return new Date(millisec);

	}

	private void balanceProgramTypeJCBActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		if (productdeliverdate != null) {

			//convert to date value 	
			Date convertdate = toDate(productdeliverdate);
			//date split up methods
			Calendar cal = Calendar.getInstance();
			cal.setTime(convertdate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if (!(day == 1)) {
				day = 1;
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.DATE, day);
				cal.set(Calendar.YEAR, year);
				Startdate = cal.getTime();
			}
			Enddate = toDate(productdeliverdate);
		}

		JComboBox cb = (JComboBox) evt.getSource();
		programname = (String) cb.getSelectedItem();
		System.out.println(programname);
		this.facilityprogramcode = programname;
		
		callfacility = new facilitysetupsessionsmappercalls();
		Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
		facility = callfacility.getFacility();
		System.out.println(facility.getCode() + "We have the facility code");
		facilitytype = callfacility.selectAllwithTypes(facility);
		typeCode = facilitytype.getCode();
		
		String dbdriverStatus = System.getProperty("dbdriver");
		if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
			productsbalanceList = Facilityproductsmapper
					.dogetcurrentFacilityApprovedProducts(
							facilityprogramcode, typeCode);
		} else {
			productsbalanceList = Facilityproductsmapper
					.dogetcurrentFacilityApprovedProducts(
							facilityprogramcode, typeCode);
		}
		this.PopulateProgram_ProductTable(productsbalanceList);

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.dispose();
	}

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		try {

			String mydate = this.ShipmentdateJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d;

			d = sdf.parse(oldDateString);

			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productdeliverdate;

	}

	private void facilityapprovedProductsJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {
			if ("tableCellEditor".equals(evt.getPropertyName())) {

				//saveShipped_Line_Items = new Shipped_Line_Items();
				if (this.facilityapprovedProductsJT.isColumnSelected(4)) {

				}

				if (this.facilityapprovedProductsJT.isColumnSelected(3)) {
					if (facilityapprovedProductsJT.isEditing()) {
						System.out.println("THIS CELL HAS STARTED EDITING");
						//get column index
						colindex = this.facilityapprovedProductsJT
								.getSelectedColumn();
						rowindex = this.facilityapprovedProductsJT
								.getSelectedRow();
						Pcode = tableModel_fproducts.getValueAt(
								this.facilityapprovedProductsJT
										.getSelectedRow(), 0).toString();
						

					} else if (!facilityapprovedProductsJT.isEditing()) {
                      	if(calltracer == false){					
						createshipmentObj(Pcode, rnrid, shipmentdate);
                      	}
						//calltracer = true;
						System.out.println("THIS CELL IS NOT EDITING");
						 
					}
				}
			}
		} catch (ParseException p) {
			p.getMessage();
		}
	}
	
	

		

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	private void facilityapprovedProductsJTMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object 

	}

	public void createshipmentObj(String apcode, int arnrid,
			Timestamp amodifieddate) throws ParseException {
		
		String dbdriverStatus = System.getProperty("dbdriver");
		if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
					productsPhysicalcountList = Facilityproductsmapper
					.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
		} else {
			productsPhysicalcountList = Facilityproductsmapper
					.dogetcurrentSystemcalculatedproductbalanceshsqldb(facilityprogramcode,apcode);
		
		}
		//created a shipment object storephysicalcount.
		if (productdeliverdate == null) {
			java.util.Date date = new java.util.Date();
			amodifieddate = new Timestamp(date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}
		storephysicalcount.setProductcode(apcode);
		storephysicalcount.setFacilitycode(this.facility.getCode());
		
		if ( productsearchenabled == true){
			try {
				storephysicalcount.setPhysicalcount(Integer
						.parseInt(tableModel_searchfproducts.getValueAt(rowindex,3)
								.toString()));
				
				//iterate through the arraylist and compare the quantity flag the differences
				for (@SuppressWarnings("unused")
				VW_Systemcalculatedproductsbalance p : productsPhysicalcountList) {
					if(p.getProductcode().equals(storephysicalcount.getProductcode())){
					if( !(p.getBalance() == storephysicalcount.getPhysicalcount()) ){
						
						System.out.println("Im am invisible");
						//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 3);
						int ok = new arvmessageDialog()
						.showDialog(
								this,
								"Do  you want to save the Physical Count Quantities to the database\n\n",
								"Physical count does not match System Calculated physical count",
								"YES, To \n Save",
								"NO, To\n Edit the Quantity Entered ", "Cancel");
								
						if (ok == JOptionPane.YES_OPTION) {
							try {
							//Proceed to save the physical count 
							this.calltracer = true;
							saveresponse = true;
							//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 4);
							}catch(java.lang.ArrayIndexOutOfBoundsException e){
								e.getStackTrace();
							}
						}else if (ok == JOptionPane.NO_OPTION) {
							try {
								this.calltracer = true;
								saveresponse = false;
							this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 3);
							}catch(java.lang.ArrayIndexOutOfBoundsException e){
								e.getStackTrace();
							}
						}
						
						
					 }
					}
				}
				
				}catch(NumberFormatException e){
					e.getStackTrace();
				}
		}else{
		
		try {
		storephysicalcount.setPhysicalcount(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 3)
						.toString()));
		
		//iterate through the arraylist and compare the quantity flag the differences
		for (@SuppressWarnings("unused")
		VW_Systemcalculatedproductsbalance p : productsPhysicalcountList) {
			if(p.getProductcode().equals(storephysicalcount.getProductcode())){
			if( !(p.getBalance() == storephysicalcount.getPhysicalcount()) ){
				
				System.out.println("Im am invisible");
			
				int ok = new arvmessageDialog()
				.showDialog(
						this,
						"Do  you want to save the Physical Count Quantities to the database\n\n",
						"Physical count does not match System Calculated physical count",
						"YES, To \n Save",
						"NO, To\n Edit the Quantity Entered ", "Cancel");
				
				if (ok == JOptionPane.YES_OPTION) {
					try {
						//Proceed to save the physical count 
				     calltracer = true;
					saveresponse = true;
					System.out.println("Got inside yeah!");
					//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 4);
					}catch(java.lang.ArrayIndexOutOfBoundsException e){
						e.getStackTrace();
					}
				}else if (ok == JOptionPane.NO_OPTION) {
					try {
						this.calltracer = true;
						saveresponse = false;
					this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 3);
					}catch(java.lang.ArrayIndexOutOfBoundsException e){
						e.getStackTrace();
					}
				}
			 }
			}
		}
		
		}catch(NumberFormatException e){
			e.getStackTrace();
		}
		}
		
		if ( productsearchenabled == true){
			try {
				/*storephysicalcount.setSystem_physicalcount(Integer
						.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 3)
								.toString()));*/
				}catch(NumberFormatException e){
					e.getStackTrace();
				}
			
		}else {
		try {
		/*storephysicalcount.setSystem_physicalcount(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 3)
						.toString()));*/
		}catch(NumberFormatException e){
			e.getStackTrace();
		}
		}
		if ( productsearchenabled == true){
			storephysicalcount.setComments(tableModel_searchfproducts.getValueAt(
					rowindex, 4).toString());
		}else{
		storephysicalcount.setComments(tableModel_fproducts.getValueAt(
				rowindex, 4).toString());
		
		}
		storephysicalcount.setCreatedby(AppJFrame.userLoggedIn);
		storephysicalcount.setCreateddate(amodifieddate);
		sccproductbalanceList.add(storephysicalcount);

	}

	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";
		
		this.facilityapprovedProductsJT.editCellAt(-1,-1);
		/*int ok = new arvmessageDialog()
		.showDialog(
				this,
				"Do  you want to save the Received Quantities to the database\n\n",
				"Saving Quantity Received Information",
				"YES, To \n Save",
				"NO, To\n Edit the Quantity Entered ", "Cancel");*/
		    //if(ok == 0){
			if(this.saveresponse == true){
		
			 		savestorephysicalcount = new Store_Physical_Count();
					//	System.out.println(elmisstockcontrolcardList.size());
			 		
			 		
					for (@SuppressWarnings("unused")
					Store_Physical_Count sp : sccproductbalanceList) {

						this.savestorephysicalcount.setProductcode(sp.getProductcode());
						this.savestorephysicalcount.setFacilitycode(sp
								.getFacilitycode());
						this.savestorephysicalcount.setPhysicalcount(sp
								.getPhysicalcount());
						this.savestorephysicalcount.setSystem_physicalcount(sp
								.getSystem_physicalcount());
						this.savestorephysicalcount.setComments(sp.getComments());
						this.savestorephysicalcount.setCreatedby(sp.getCreatedby());
						this.savestorephysicalcount.setCreateddate(sp.getCreateddate());
						Facilityproductsmapper
								.Insertproductsphysicalcount(savestorephysicalcount);

					}

					JOptionPane.showMessageDialog(this, "Products saved to database",
							"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
			
			}else{
		//}else{
			this.facilityapprovedProductsJT.validate();
		}

	}

	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner = new TableColumnAligner();
		tablecolumnaligner.rightAlignColumn(this.facilityapprovedProductsJT,3);
		storephysicalcount = new Store_Physical_Count();
		this.facilityapprovedProductsJT.getTableHeader()
		.setFont(new Font("Ebrima", Font.PLAIN, 20));
		this.facilityapprovedProductsJT.setRowHeight(30);

		this.ShipmentdateJDate.setFocusable(isFocusable());
		Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
		productbalanceprogramsList = Facilityproductsmapper.selectAllPrograms();
		for (Programs p : productbalanceprogramsList) {
			modelbalanceProgramslist.addElement(p.getCode());

		}

		//productsbalanceList = Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalances(valueA)

		//	PopulateProgram_ProductTable(productsbalanceList);
		rnrid = 4;
		this.ShipmentdateJDate.setDate(new Date());
		this.ShipmentdateJDate.setFocusable(isFocusable());
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");
	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {

		try {
			callfacility = new facilitysetupsessionsmappercalls();
			Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

			facility = callfacility.getFacility();
			System.out
					.println(facility.getCode() + "We have the facility code");
			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();

			if (fapprovProducts == null) {
				callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				if (!(facilityprogramcode == null)) {
					//convert timestamp to date 
					//Startdate;
					//	Enddate;
					String dbdriverStatus = System.getProperty("dbdriver");
					if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {

						productsbalanceList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					} else {
						productsbalanceList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					}
				}

			} else {
				productsbalanceList = fapprovProducts;
			}

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsbalanceList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = productsbalanceList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProductsbalance = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProductsbalance
						.getCode().toString();

				defaultv_facilityapprovedproducts[1] = facilitysccProductsbalance
						.getPrimaryname().toString();
				;
				defaultv_facilityapprovedproducts[2] = facilitysccProductsbalance
						.getStrength();
				/*defaultv_facilityapprovedproducts[3] = facilitysccProductsbalance
						.getBalance();*/

				/* defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
					TableColumn column1 = facilityapprovedProductsJT.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());
					
				defaultv_facilityapprovedproducts[4] = column1;*/

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				arvdispensingProductsPhysicalCountJD dialog = new arvdispensingProductsPhysicalCountJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JButton SaveJBtn;
	private java.awt.TextField SearchproductsJTF;
	private com.toedter.calendar.JDateChooser ShipmentdateJDate;
	private javax.swing.JComboBox balanceProgramTypeJCB;
	private javax.swing.JTable facilityapprovedProductsJT;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private java.awt.Label label1;
	// End of variables declaration//GEN-END:variables

}