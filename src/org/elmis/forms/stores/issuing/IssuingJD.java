/*
 * IssuingJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.issuing;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.controllers.facilityarvdispensingsessionsmappercalls;
import org.elmis.facility.controllers.facilityproductssetupsessionmapperscalls;
import org.elmis.facility.domain.model.Dispensing_point;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Items_issue;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.forms.admin.dispensingpoints.NumberPadJD;
import org.elmis.forms.admin.dispensingpoints.SelectDispensingPointJD;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * 
 * @author __USER__
 */
public class IssuingJD extends javax.swing.JDialog {

	// private static final String[] columns_issueJTable = { "Name" };
	// private static final Object[] defaultv_issueJTable = { "" };
	// private static final int rows_issueJTable = 0;
	// public static TableModel tableModel_dispensingPoint = new TableModel(
	// columns_dispensingPoint, defaultv_dispensingPoint,
	// rows_dispensingPoint);

	public static TableModel tableModel_issueJTable = new TableModel(
			new String[] { "Product", "Qty", "id", "Code" }, new Object[] { "",
					"", "", "" }, 0);

	//private int selectedProgramID;
	public static String selectedDispensingPointName;
	public static int selectedProductID;
	public static int selectedprogramID  = 0;
	public static Products selectedProduct;
	public static String barcodeScanned;

	private String searchText = "";
	private char letter;
	private List<Character> charList = new LinkedList();

	private List<Products> productsList = new LinkedList();

	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	private Dispensing_point dp;

	private ProductQty qty;
	facilityarvdispensingsessionsmappercalls aFacilityproductsmapper = null;
	facilityproductssetupsessionmapperscalls Facilityproductsmapper = null;
	private ProductQty dispensarypqty;
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	public List<Programs> programsbalList = new LinkedList();
	
	/** Creates new form IssuingJD */
	public IssuingJD(java.awt.Frame parent, boolean modal, Dispensing_point dp) {
		super(parent, modal);
		initComponents();

		this.dp = dp;
		//dont show at start
		barcodeJTF.setVisible(false);
		searchJL.setVisible(false);

		this.jTabbedPane1.setFocusable(false);
		this.finishIssuingJBtn.setFocusable(false);
		this.cancelJBtn.setFocusable(false);
		this.deleteItemLineJBtn.setFocusable(false);
		this.previewJBtn.setFocusable(false);
		this.issueJTable.setFocusable(false);

		this.setTitle("Issuing to : " + dp.getName().toUpperCase());

		class RightTableCellRenderer extends DefaultTableCellRenderer {
			protected RightTableCellRenderer() {
				setHorizontalAlignment(JLabel.RIGHT);
			}

		}

		RightTableCellRenderer rightAlign = new RightTableCellRenderer();

		this.issueJTable.getColumnModel().getColumn(1).setCellRenderer(
				rightAlign);

		// set column width size
		this.issueJTable.getColumnModel().getColumn(1).setMaxWidth(100);
		this.issueJTable.getColumnModel().getColumn(1).setMaxWidth(100);

		this.issueJTable.getColumnModel().getColumn(2).setMinWidth(0);
		this.issueJTable.getColumnModel().getColumn(2).setMaxWidth(0);

		this.issueJTable.getColumnModel().getColumn(3).setMinWidth(0);
		this.issueJTable.getColumnModel().getColumn(3).setMaxWidth(0);

		// jTabbedPane1.addTab("Dispensing Point", new DispensingPointJP());

		jTabbedPane1.addTab("Program", new ProgramsJP());

		// jTabbedPane1.addTab("Products", new ProductsJP());

		// jTabbedPane1.addTab("Quantity", new IssueQtyJP());

		this.setSize(
				(AppJFrame.desktop.getSize().width / 2 + (AppJFrame.desktop
						.getSize().width / 3)),
				(AppJFrame.desktop.getSize().height / 2)
						+ (AppJFrame.desktop.getSize().height / 3));
		this.setLocationRelativeTo(null);

		// set the focus to barcodeJTF
		this.jPanel1.setOpaque(true);
		Vector<Component> order = new Vector<Component>(7);
		order.add(barcodeJTF);
		order.add(deleteItemLineJBtn);
		order.add(previewJBtn);
		order.add(jTabbedPane1);
		// order.add(tf5);
		// order.add(tf6);

		tableModel_issueJTable.clearTable();

		this.barcodeJTF.requestFocusInWindow();
		this.setVisible(true);

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		finishIssuingJBtn = new javax.swing.JButton();
		finishIssuingJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		finishIssuingJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/Save icon.png")));
		finishIssuingJBtn.setText("Finish Issuing");
		cancelJBtn = new javax.swing.JButton();
		cancelJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		cancelJBtn.setText("Cancel");
		previewJBtn = new javax.swing.JButton();
		previewJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		previewJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/eLMIS View details.png")));
		previewJBtn.setText("Preview");
		searchJL = new javax.swing.JLabel();
		barcodeJTF = new javax.swing.JTextField();
		jTabbedPane1 = new javax.swing.JTabbedPane();
		jScrollPane1 = new javax.swing.JScrollPane();
		issueJTable = new javax.swing.JTable();
		jPanel2 = new javax.swing.JPanel();
		deleteItemLineJBtn = new javax.swing.JButton();
		deleteItemLineJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		deleteItemLineJBtn.setText("Delete Selected Item");
		editQtyJBtn = new javax.swing.JButton();
		editQtyJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		editQtyJBtn.setText("Edit Quantity");

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		//finishIssuingJBtn.setIcon(new javax.swing.ImageIcon(getClass()
			//	.getResource("/elmis_images/Finished.png"))); // NOI18N
		finishIssuingJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						finishIssuingJBtnActionPerformed(evt);
					}
				});

		cancelJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});
		previewJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				previewJBtnActionPerformed(evt);
			}
		});

		searchJL.setFont(new Font("Ebrima", Font.BOLD, 12));
		searchJL.setForeground(new java.awt.Color(255, 255, 255));
		searchJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view user small.png"))); // NOI18N
		searchJL.setText("Search");

		barcodeJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				barcodeJTFKeyReleased(evt);
			}

			public void keyTyped(java.awt.event.KeyEvent evt) {
				barcodeJTFKeyTyped(evt);
			}
		});

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});
		jTabbedPane1.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				jTabbedPane1KeyReleased(evt);
			}
		});

		issueJTable.setModel(tableModel_issueJTable);
		issueJTable.setCellSelectionEnabled(true);
		issueJTable.setRowHeight(30);
		jScrollPane1.setViewportView(issueJTable);

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		deleteItemLineJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/eLMIS delete role small.png"))); // NOI18N
		deleteItemLineJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						deleteItemLineJBtnActionPerformed(evt);
					}
				});

		editQtyJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/eLMIS change password small.png"))); // NOI18N

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(deleteItemLineJBtn, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(editQtyJBtn, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(192, Short.MAX_VALUE))
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(deleteItemLineJBtn, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addComponent(editQtyJBtn, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		jPanel2.setLayout(jPanel2Layout);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(jScrollPane1, 0, 0, Short.MAX_VALUE)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(previewJBtn, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(finishIssuingJBtn, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(26)
							.addComponent(searchJL)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(barcodeJTF, GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, 249, Short.MAX_VALUE)))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE))
						.addComponent(jTabbedPane1, GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(searchJL)
						.addComponent(barcodeJTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(previewJBtn, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(finishIssuingJBtn, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout
				.setHorizontalGroup(layout.createParallelGroup(
						javax.swing.GroupLayout.Alignment.LEADING).addGroup(
						layout.createSequentialGroup().addContainerGap()
								.addComponent(jPanel1,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void previewJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		List list = new ArrayList();
		Items_issue items;
		for (int i = 0; i < this.tableModel_issueJTable.getRowCount(); i++) {

			items = new Items_issue();

			items.setProduct_code(this.tableModel_issueJTable.getValueAt(i, 3)
					.toString());
			items.setProduct_id(Integer.parseInt(this.tableModel_issueJTable
					.getValueAt(i, 2).toString()));
			items.setProduct_name(this.tableModel_issueJTable.getValueAt(i, 0)
					.toString());
			items.setQtyToIssue(Integer.parseInt(this.tableModel_issueJTable
					.getValueAt(i, 1).toString()));

			list.add(items);
		}

		// new Items_issue().createBeanCollection()
		try {

			parameterMap.put("bg_text", "Draft");
			parameterMap.put("issued_by", AppJFrame.userLoggedIn);
			parameterMap.put("dispensing_point",
					SelectDispensingPointJD.selectedDispensingPoint);

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/items_issue.jasper",
					parameterMap, new JRBeanCollectionDataSource(list));

			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}

	}

	private void barcodeJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = barcodeJTF.getText();

			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}
			for (char c : charList) {

				s.append(c);

			}

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				System.out.println("Delete " + s);

				this.productsList = session.selectList(
						"selectProductBySearchTerm", s.toString());

				ProductsJP.populateProductsTable(this.productsList);

			} finally {
				session.close();
			}

		} else {

			// searchText += Character.toString(letter);
			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				System.out.println(s.toString());

				this.productsList = session.selectList(
						"selectProductBySearchTerm", s.toString());

				ProductsJP.populateProductsTable(this.productsList);

			} finally {
				session.close();
			}

		}

	}

	private void barcodeJTFKeyReleased(java.awt.event.KeyEvent evt) {
		// TODO Barcode scanner

		//last event is enter after barcode scan event
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			//get the scanned barcode number
			this.barcodeScanned = barcodeJTF.getText();

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				this.selectedProduct = session.selectOne("getByProductBarcode",
						this.barcodeScanned);// ("getProgramesSupported");

			} finally {
				session.close();
			}

			//product found via barcode scanner
			if (this.selectedProduct != null) {

				//if qty in stock is zero , show message dialog 
				qty = this.selectedProduct.getProductQty();

				//if 
				if (qty.getQty() != 0) {

					// show the number pad to get quantity
					new NumberPadJD(javax.swing.JOptionPane
							.getFrameForComponent(this), true,
							this.selectedProduct);

				} else {

					javax.swing.JOptionPane
							.showMessageDialog(
									this,
									"<html><body bgcolor=\"#E6E6FA\"><b><p><p><p><p><font size=\"4\" color=\"red\">"
											+ ""
											+ this.selectedProduct
													.getPrimaryname()
											+ " is currently stocked out. </font><p><p><p><p></b></body></html>");
				}

				barcodeJTF.setText("");

				if (barcodeJTF.getText().equals("")) {

					charList.clear();
				}
				barcodeJTF.requestFocusInWindow();

			} else {

				javax.swing.JOptionPane.showMessageDialog(null,
						"Product not found");
			}

			barcodeJTF.setText("");

		}

	}

	private void deleteItemLineJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		tableModel_issueJTable.deleteRow(this.issueJTable.getSelectedRow());
	}

	private void finishIssuingJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO Improve to a batch input
	      
		  //Moses_Kausa_2014
		   dispensarypqty = new ProductQty();
		   Facilityproductsmapper = new  facilityproductssetupsessionmapperscalls();
		   aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
		   //END_2014
			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();
			
			
			
			StockControlCard scc;
			try {
			
			for (int i = 0; i < this.tableModel_issueJTable.getRowCount(); i++) {
				
				

				scc = new StockControlCard();
				
				scc.setId(com.oribicom.tools.publicMethods.createGUID());

				scc.setProductcode(this.tableModel_issueJTable.getValueAt(i, 3)
						.toString());
				scc.setProductid(Integer.parseInt(this.tableModel_issueJTable
						.getValueAt(i, 2).toString()));
				;
				scc.setQty_isssued(Integer.parseInt(this.tableModel_issueJTable
						.getValueAt(i, 1).toString()));
				scc.setIssueto_receivedfrom(SelectDispensingPointJD.selectedDispensingPoint);
				scc.setCreatedby(AppJFrame.userLoggedIn);
				scc.setCreateddate(com.oribicom.tools.publicMethods.toDay());
				scc.setRemark("Products issue");
				//reduce the stock control card balance by issue qty
								
				if(!(scc.getQty_isssued() == null)){
					
					//Changes introduced by Moses Kausa
					// Save the issued quantity to the Dispensary stock  on hand table elmis_productQty
					if(!(selectedprogramID == 0)){
						this.programsbalList =  this.Facilityproductsmapper.dogetselectedprogram(selectedprogramID);
					}
					dispensarypqty.setProduct_code(scc.getProductcode());
					dispensarypqty.setQty(scc.getQty_isssued());
					dispensarypqty.setLocation(scc.getIssueto_receivedfrom());
					
					productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(scc.getProductcode());
					for( Programs p: programsbalList ){
						if(!(p.getCode() == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(p.getCode(),scc.getProductcode());
						}else{
							electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(p.getCode(),scc.getProductcode());
						}
					}
					
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(dispensarypqty.getProduct_code())){
							System.out.println(b.getProductcode()  + "= " + dispensarypqty.getProduct_code());
							dispensarypqty.setStockonhand_scc(b.getBalance());
							@SuppressWarnings("unused")
							int reduce = b.getBalance() - scc.getQty_isssued();
							scc.setBalance(reduce);
						}
					}
					
					if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(scc.getProductcode())){
							//update call here now 
							Facilityproductsmapper.doupdatedispensaryqty(dispensarypqty.getProduct_code(),dispensarypqty.getQty(),dispensarypqty.getLocation(),dispensarypqty.getStockonhand_scc());
						}else{
							Facilityproductsmapper.InsertproductQuantity(dispensarypqty);
						}
						
						}
					}else{
						Facilityproductsmapper.InsertproductQuantity(dispensarypqty);
					}
					//END _update- change
					
										
				}
                //END - changes mkausa
			session.insert("insertSelectiveSCC", scc);
		 
			session.commit();
		
			}
			
			
			
			
			

				

			} finally {

				

				session.close();

				javax.swing.JOptionPane.showMessageDialog(this,
						"Products issued to "+this.dp.getName());

				

				this.dispose();

			}
			
		

		this.dispose();
		AppJFrame.glassPane.deactivate();
		
		

		// TODO Remove and change jasperReport To SCC class object

		List list = new ArrayList();
		Items_issue items;
		for (int i = 0; i < this.tableModel_issueJTable.getRowCount(); i++) {

			items = new Items_issue();

			items.setProduct_code(this.tableModel_issueJTable.getValueAt(i, 3)
					.toString());
			items.setProduct_id(Integer.parseInt(this.tableModel_issueJTable
					.getValueAt(i, 2).toString()));
			items.setProduct_name(this.tableModel_issueJTable.getValueAt(i, 0)
					.toString());
			items.setQtyToIssue(Integer.parseInt(this.tableModel_issueJTable
					.getValueAt(i, 1).toString()));

			list.add(items);
		}

		// new Items_issue().createBeanCollection()
		try {

			parameterMap.put("bg_text", "");
			parameterMap.put("issued_by", AppJFrame.userLoggedIn);
			parameterMap.put("dispensing_point",
					SelectDispensingPointJD.selectedDispensingPoint);

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/items_issue.jasper",
					parameterMap, new JRBeanCollectionDataSource(list));

			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}

	
		
		
		
			

	}

	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		// this.cancelJBtn.getFocusListeners();
		// this.cancelJBtn.transferFocus();
		this.cancelJBtn.requestFocusInWindow();
		AppJFrame.glassPane.deactivate();
		this.dispose();

	}

	private void jTabbedPane1KeyReleased(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			// your code is scanned and you can access it using
			// frame.getBarCode()
			// now clean the bar code so the next one can be read
			// frame.setBarCode(new String());
			// javax.swing.JOptionPane.showMessageDialog(null, "Here");

		}
	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getSelectedIndex());

		if (jTabbedPane1.getSelectedIndex() == 1
				&& jTabbedPane1.getTabCount() == 3) {// &&
			// jTabbedPane1.getSelectedIndex()
			// ==2){

			// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getTabCount());

			jTabbedPane1.remove(2);
		}

		if (jTabbedPane1.getSelectedIndex() == 0
				&& jTabbedPane1.getTabCount() == 2) {// &&

			//remove the products JTable
			jTabbedPane1.remove(1);

			barcodeJTF.setVisible(false);
			searchJL.setVisible(false);
		}

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				IssuingJD dialog = new IssuingJD(new javax.swing.JFrame(),
						true, new Dispensing_point());
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	public static javax.swing.JTextField barcodeJTF;
	private javax.swing.JButton cancelJBtn;
	private javax.swing.JButton deleteItemLineJBtn;
	private javax.swing.JButton editQtyJBtn;
	private javax.swing.JButton finishIssuingJBtn;
	private javax.swing.JTable issueJTable;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	public static javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JButton previewJBtn;
	public static javax.swing.JLabel searchJL;
	// End of variables declaration//GEN-END:variables

}