/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.receiving;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.MutableComboBoxModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.controllers.facilityproductssetupsessionmapperscalls;
import org.elmis.facility.controllers.facilitysetupsessionsmappercalls;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import com.oribicom.tools.TableModel;
import com.toedter.calendar.JDateChooserCellEditor;
import java.awt.Font;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;


/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class ReceiveProductsJD extends javax.swing.JDialog {

	facilitysetupsessionsmappercalls callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	TableColumnAligner tablecolumnaligner; 
	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	public static String searchprogramcode =  null;
    Integer ProdQty = null;
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Id", "Product Code", "Product name", "Generic Strength",
			"Expiry Date","Quantity Received" , "Remarks" };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "", "", "", "" };
	private static final int rows_fproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false, true,
				true, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 6) {

				return getValueAt(0, 6).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};
	
	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false, true,
				true, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {

					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 3) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0, 3).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/*public Class getColumnClass(int c) 
		{     
		for(int rowIndex = 0; rowIndex < data.size(); rowIndex++)
		{
		    Object[] row = data.get(rowIndex);
		    if (row[c] != null) {
		        return getValueAt(rowIndex, c).getClass();
		    }   
		}
		return String.class;
		}*/

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};


	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator elmistockcontrolcarditerator = elmisstockcontrolcardList
			.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	facilityproductssetupsessionmapperscalls Facilityproductsmapper = null;
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	
	
	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	//List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//private VW_Systemcalculatedproductsbalance facilitysccProducts;
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	
	

	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public ReceiveProductsJD(java.awt.Frame parent, boolean modal,String selectedprogramCode ) {
		super(parent, modal);
		initComponents();
		searchprogramcode = selectedprogramCode;
		this.setSize(1000, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		ShipmentdateJDate.setPreferredSize(new Dimension(150, 20));
		SearchproductssccJTF.setPreferredSize(new Dimension(150, 20));
		refnojTextField.setPreferredSize(new Dimension(150, 20));

	}

	

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		ShipmentdateJDate = new com.toedter.calendar.JDateChooser();
		jLabel2 = new javax.swing.JLabel();
		refnojTextField = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		facilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();
		SearchproductssccJTF = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Products Receipts");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Shipment date");

		ShipmentdateJDate.setDateFormatString(mydateformat);
		ShipmentdateJDate.setFont(new java.awt.Font("Ebrima", 0, 18));
		ShipmentdateJDate
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Despatch Number");

		refnojTextField.setFont(new Font("Ebrima", Font.PLAIN, 18));

		facilityapprovedProductsJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		facilityapprovedProductsJT.setModel(tableModel_fproducts);
		facilityapprovedProductsJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						facilityapprovedProductsJTMouseClicked(evt);
					}
				});
		facilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						facilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(facilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBtn.setText("Close");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		SearchproductssccJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				SearchproductssccJTFKeyTyped(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Search");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 749, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
							.addComponent(CancelJBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(SaveJBtn)
							.addGap(32))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(jLabel2)
							.addGap(3))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
									.addComponent(jLabel1)
									.addGap(17)
									.addComponent(ShipmentdateJDate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
									.addGap(104)
									.addComponent(refnojTextField, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)))
							.addGap(90)
							.addGroup(jPanel1Layout.createSequentialGroup()
								.addComponent(jLabel4)
								.addPreferredGap(ComponentPlacement.RELATED))
							.addComponent(SearchproductssccJTF, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(jLabel1)
								.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jLabel2)
								.addComponent(refnojTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
							.addComponent(SearchproductssccJTF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(jLabel4)))
					.addGap(26)
					.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 317, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(CancelJBtn)
						.addComponent(SaveJBtn))
					.addContainerGap(101, Short.MAX_VALUE))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	@SuppressWarnings("static-access")
	private void SearchproductssccJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		
		// TODO add your handling code here:

		System.out.println(this.arvsearchproductsList.size() + "list not null");

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = this.SearchproductssccJTF.getText();

			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}

			for (char c : charList) {

				s.append(c);

			}
		} else {

			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			//get ARV list 
			try {
				if (!(this.SearchproductssccJTF.getText() == "")) {

					if (!(s.substring(0, 1).matches("[0-9]"))) {

						callfacility = new facilitysetupsessionsmappercalls();
						Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
                        
						if(!(searchprogramcode == null)){
						this.facilityprogramcode = searchprogramcode;}
						//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
						//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

						facility = callfacility.getFacility();

						facilitytype = callfacility
								.selectAllwithTypes(facility);
						typeCode = facilitytype.getCode();

						productsList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
						arvsearchproductsList = productsList;

						/*for(VW_Program_Facility_ApprovedProducts p: arvsearchproductsList ){
							
							if(p.getPrimaryname().contains(s)){
								this.arvproductsList.add(p);
							}
						}*/

						for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

							if (p.getPrimaryname().contains(s)) {
								this.arvproductsList.add(p);
							}
						}

						//	System.out.println("Delete " + s);
						//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						this.searchPopulateProgram_ProductTable(
								arvproductsList, "STRING_FOUND");

					}
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}

		}

		// searchText += Character.toString(letter);
		/*	charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			try {
				//check if char is a digit and escape the loop.
				if(!(this.searchjTextField.getText() == "")){
				if(!(s.substring(0,1).matches("[0-9]"))){ 

				callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper.dogetcurrentFacilityApprovedProducts(facilityprogramcode, typeCode);
					
				arvsearchproductsList = productsList;

				for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

					if (p.getPrimaryname().contains(s)) {
						this.arvproductsList.add(p);
					}
				}
				//System.out.println("Delete " + s);
				//arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

				searchPopulateProgram_ProductTable(arvproductsList,
						"STRING_FOUND");
				
				}
				
			}
			} catch (NullPointerException e) {
				e.getMessage();
			}*/
	}
	
	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

			/*	callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}

			this.facilityapprovedProductsJT
					.setModel(tableModel_searchfproducts);
			tableModel_searchfproducts.clearTable();

			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getProductid().toString();

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength().toString();
				//Set the value of the entered product quantity 

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}
				tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.dispose();
	}

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {

			String mydate = this.ShipmentdateJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		return productdeliverdate;

	}

	private void facilityapprovedProductsJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMinWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMaxWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setWidth(0);
		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			if (this.facilityapprovedProductsJT.isColumnSelected(4)) {

			}
			if (this.facilityapprovedProductsJT.isColumnSelected(6)) {

			}
			if (this.facilityapprovedProductsJT.isColumnSelected(5)) {
				if (facilityapprovedProductsJT.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					colindex = this.facilityapprovedProductsJT
							.getSelectedColumn();
					rowindex = this.facilityapprovedProductsJT.getSelectedRow();
					Pcode = tableModel_fproducts
							.getValueAt(
									this.facilityapprovedProductsJT
											.getSelectedRow(), 1).toString();
					
					/*ProdQty = (Integer
							.parseInt(tableModel_fproducts.getValueAt(rowindex, 6)
									.toString()));*/

                      System.out.println(ProdQty + "SAVE without tab");
					/*System.out.println(colindex);	
					System.out.println(rowindex);	
					System.out.println(Pcode);*/

				} else if (!facilityapprovedProductsJT.isEditing()) {
					try {
						
						ProdQty = Integer.parseInt(tableModel_fproducts
								.getValueAt(this.facilityapprovedProductsJT
												.getSelectedRow(),6).toString());
						}catch (NumberFormatException n){
							n.getStackTrace();
						}
                     //this.facilityapprovedProductsJT.editCellAt(-1,-1);
                     //check if createob method has been called already skip if not call me 
					createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);

					System.out.println("THIS CELL IS NOT EDITING");

				}
				// System.out.println(ProdQty + "SAVE without tab");
				//createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
			}
			
			//System.out.println(ProdQty + "SAVE without tab");
			//check if create obj has been called an dcall 
			//createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
		}
		
		System.out.println(ProdQty + "SAVE without tab");
		//createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
	}

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	private void facilityapprovedProductsJTMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object 

	}

	public void createshipmentObj(String apcode, int arnrid,
			Timestamp amodifieddate,Integer productQty) {
		//System.out.println("Did object call me ???");

		//created a shippment object 
		try {
		Integer A = 0;
		Integer B = 0;
		Integer C = 0;
		if (productdeliverdate == null) {
			java.util.Date date = new java.util.Date();
			amodifieddate = new Timestamp(date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}

		System.out.println(amodifieddate);
		System.out.println(A);
		System.out.println(B);
		System.out.println(C);

		String prodremark = tableModel_fproducts.getValueAt(rowindex,6)
				.toString();

		if (prodremark != null) {
			elmis_stock_control_card.setRemark(prodremark);
		}

		String prodexpiredate = tableModel_fproducts.getValueAt(rowindex, 4)
				.toString();
		System.out.println(prodexpiredate + "EXPIRY DATE ??");
		if (prodexpiredate != null) {

			Timestamp expiredate = Convertdatestr(prodexpiredate);
			System.out.println(expiredate + "EXPIRY DATE NEW ??");
			elmis_stock_control_card.setExpirydate(expiredate);
		}
		elmis_stock_control_card.setRefno(this.refnojTextField.getText()
				.toString());
		System.out.println(elmis_stock_control_card.getRefno());

		elmis_stock_control_card.setProductcode(apcode);
		elmis_stock_control_card.setIssueto_receivedfrom(facilityproductsource);
		
	
		elmis_stock_control_card.setQty_received(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
						.toString()));
		elmis_stock_control_card.setProductid(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
						.toString()));

		elmis_stock_control_card.setQty_isssued(0);

		elmis_stock_control_card.setAdjustments(0);
		
		
		elmis_stock_control_card.setId(com.oribicom.tools.publicMethods
				.createGUID());//this.GenerateGUID());

		A = elmis_stock_control_card.getQty_received();
		B = elmis_stock_control_card.getQty_isssued();
		C = elmis_stock_control_card.getAdjustments();

		System.out.println(A);
		System.out.println(B);
		System.out.println(C);
		//changes Mkausa - 2014
		if(!(searchprogramcode == null)){
		electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(searchprogramcode,apcode);
		}
		
		if(electronicsccbal.size() > 0){
		for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
			if( b.getProductcode().equals(apcode)){
								
				@SuppressWarnings("unused")
				int increase = b.getBalance() + elmis_stock_control_card.getQty_received();
				elmis_stock_control_card.setBalance(increase);
			}else{
				elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
			}
		}
		}else{
			elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
		}
	
		elmis_stock_control_card.setAdjustmenttype(this.adjustmentname);
		System.out.println(elmis_stock_control_card.getBalance());
		elmis_stock_control_card.setStoreroomadjustment(false);
		elmis_stock_control_card.setProgram_area(facilityprogramcode);
		//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));

		elmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
		elmis_stock_control_card.setCreateddate(amodifieddate);

		//created linked list object 

		//elmisstockcontrolcardList.add(savestockcontrolcard);
		elmisstockcontrolcardList.add(elmis_stock_control_card);
		System.out.println(elmisstockcontrolcardList.size() + "ARRAY SIZE???");
		System.out.println("Array size here &&&");
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		
		}catch (NumberFormatException n){
			n.getStackTrace();
		}catch (NullPointerException l){
			 l.getMessage();
		}catch(IndexOutOfBoundsException i){
			i.getStackTrace();
		}
		//return saveShipped_Line_Items;
      
	}

	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";
		this.facilityapprovedProductsJT.editCellAt(-1,-1);
		
		int ok = new arvmessageDialog()
		.showDialog(
				this,
				"Do  you want to save the Received Quantities to the database\n\n",
				"Saving Quantity Received Information",
				"YES, To \n Save",
				"NO, To\n Edit the Quantity Entered ", "Cancel");
		
	/*int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
				"Do  you want to save the Received Quantities to the database\n\n"
						+ "Yes - to Save\n\n"
						+ "No -to Edit the Quantity Entered\n\n"
						+ "Cancel - to exit Dialog",
				"Saving Quantity Received Information",
				JOptionPane.YES_NO_OPTION);*/

		if(!(refnojTextField.getText().equals(""))){
		if (ok == JOptionPane.YES_OPTION) {
			//Insert the object to database
			
			
			savestockcontrolcard = new Elmis_Stock_Control_Card();
			//	System.out.println(elmisstockcontrolcardList.size());
			for (@SuppressWarnings("unused")
			Elmis_Stock_Control_Card sp : elmisstockcontrolcardList) {
                
				if (!(sp.getQty_received() == null)){
								
				savestockcontrolcard.setRefno(sp.getRefno());
				savestockcontrolcard.setProductcode(sp.getProductcode());
				savestockcontrolcard.setIssueto_receivedfrom(sp
						.getIssueto_receivedfrom());
				savestockcontrolcard.setQty_received(sp.getQty_received());

				savestockcontrolcard.setQty_isssued(sp.getQty_isssued());
				savestockcontrolcard.setAdjustments(sp.getAdjustments());
				savestockcontrolcard.setBalance(sp.getBalance());
				savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
				savestockcontrolcard.setProductid(sp.getProductid());

				savestockcontrolcard.setCreatedby(sp.getCreatedby());
				savestockcontrolcard.setCreateddate(sp.getCreateddate());
				savestockcontrolcard.setProgram_area(sp.getProgram_area());
				savestockcontrolcard.setStoreroomadjustment(sp
						.isStoreroomadjustment());
				savestockcontrolcard.setId(sp.getId());
				if (sp.getRemark() != null) {
					savestockcontrolcard.setRemark(sp.getRemark());
				}
				if (sp.getExpirydate() != null) {

					savestockcontrolcard.setExpirydate(sp.getExpirydate());
				}
				
				if(sp.getProgram_area().toString().equals("ARV")){
					System.out.println("foo save  to elmis_prodQty");
					
					
				}
				Facilityproductsmapper
						.InsertstockcontrolcardProducts(savestockcontrolcard);
				
				}else {
					
										
						savestockcontrolcard.setRefno(sp.getRefno());
						savestockcontrolcard.setProductcode(sp.getProductcode());
						savestockcontrolcard.setIssueto_receivedfrom(sp
								.getIssueto_receivedfrom());
					  //savestockcontrolcard.setQty_received(sp.getQty_received());
						savestockcontrolcard.setQty_received(Integer
								.parseInt(tableModel_fproducts.getValueAt(rowindex, 6)
										.toString()));
						savestockcontrolcard.setQty_isssued(sp.getQty_isssued());
						savestockcontrolcard.setAdjustments(sp.getAdjustments());
						savestockcontrolcard.setBalance(sp.getBalance());
						savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
						savestockcontrolcard.setProductid(sp.getProductid());

						savestockcontrolcard.setCreatedby(sp.getCreatedby());
						savestockcontrolcard.setCreateddate(sp.getCreateddate());
						savestockcontrolcard.setProgram_area(sp.getProgram_area());
						savestockcontrolcard.setStoreroomadjustment(sp
								.isStoreroomadjustment());
						savestockcontrolcard.setId(sp.getId());
						if (sp.getRemark() != null) {
							savestockcontrolcard.setRemark(sp.getRemark());
						}
						if (sp.getExpirydate() != null) {

							savestockcontrolcard.setExpirydate(sp.getExpirydate());
						}
						Facilityproductsmapper
								.InsertstockcontrolcardProducts(savestockcontrolcard);
					
					
				}

			}

			JOptionPane.showMessageDialog(this, "Products saved to database",
					"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
			
			
		} else if (ok == JOptionPane.NO_OPTION) {
			this.facilityapprovedProductsJT.validate();
		}
		}else{
			JOptionPane.showMessageDialog(this, "Dispatch Number is required!",
					"Saving Failed", JOptionPane.INFORMATION_MESSAGE);
			
			
		}

	}

	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.facilityapprovedProductsJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 22));
		this.facilityapprovedProductsJT.setRowHeight(30);
		tablecolumnaligner.rightAlignColumn(this.facilityapprovedProductsJT,6);
		for (Losses_Adjustments_Types l : facilityAdjustmentList) {
			modelAdjustments.addElement(l.getName());

		}
		PopulateProgram_ProductTable(productsList);
		rnrid = 4;
		this.ShipmentdateJDate.setDate(new Date());
		this.ShipmentdateJDate.setFocusable(isFocusable());
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");
	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {
		checkbox = new JCheckBox();
		try {
			callfacility = new facilitysetupsessionsmappercalls();
			Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

			this.facilityprogramcode = ProgramsJP.selectedprogramCode;
			this.facilityproductsource = ProductsSourceJP.selectedproductsource;
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

			facility = callfacility.getFacility();

			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();
			String dbdriverStatus = System.getProperty("dbdriver");

			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				productsList = Facilityproductsmapper
						.dogetcurrentFacilityApprovedProducts(
								facilityprogramcode, typeCode);
			} else {
				productsList = Facilityproductsmapper
						.dogetcurrentFacilityApprovedProductshsqldb(
								facilityprogramcode, typeCode);
			}

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = productsList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getProductid().toString();
				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength();

				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				TableColumn column1 = facilityapprovedProductsJT
						.getColumnModel().getColumn(4);
				column1.setCellRenderer(new JDateChooserRenderer());
				column1.setCellEditor(new JDateChooserCellEditor());

				//				defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ReceiveProductsJD dialog = new ReceiveProductsJD(
						new javax.swing.JFrame(), true, searchprogramcode);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JButton SaveJBtn;
	private javax.swing.JTextField SearchproductssccJTF;
	private com.toedter.calendar.JDateChooser ShipmentdateJDate;
	private javax.swing.JTable facilityapprovedProductsJT;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextField refnojTextField;
	// End of variables declaration//GEN-END:variables

}