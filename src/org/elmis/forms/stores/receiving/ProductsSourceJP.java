/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.receiving;

import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFileChooser;

import org.elmis.facility.main.gui.AppJFrame;

import org.elmis.forms.stores.receiving.ReceivingJD;
import org.elmis.forms.stores.receiving.ProgramsJP;
import org.elmis.forms.stores.receiving.sccfilechooser;

/**
 *
 * @author  __USER__
 * 
 * 
 */

public class ProductsSourceJP extends javax.swing.JPanel {

	public static String selectedproductsource = null;
	@SuppressWarnings("unused")
	private sccfilechooser mysccfilechooser;

	//ReceivingJD receivingjd = null;
	/** Creates new form ProgramsJP */
	public ProductsSourceJP() {
		initComponents();
		this.setSize(500, 300);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		ProductsSourcejTable = new javax.swing.JTable();

		setBackground(new java.awt.Color(102, 102, 102));

		ProductsSourcejTable.setBackground(new java.awt.Color(102, 102, 102));
		ProductsSourcejTable.setFont(new java.awt.Font("Ebrima", 0, 24));
		ProductsSourcejTable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {{"MSL"},{"CHAZ"}

				}, new String[] {"Product Source"

				}));
		ProductsSourcejTable.setRowHeight(60);
		ProductsSourcejTable.setTableHeader(null);
		ProductsSourcejTable
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						ProductsSourcejTableMouseClicked(evt);
					}
				});
		jScrollPane1.setViewportView(ProductsSourcejTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 775,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addComponent(jScrollPane1,
						javax.swing.GroupLayout.PREFERRED_SIZE, 220,
						javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(121, Short.MAX_VALUE)));
	}// </editor-fold>
	//GEN-END:initComponents

	private void ProductsSourcejTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// enable Program Area tab.
		@SuppressWarnings("unused")
		String productsourceselection = this.ProductsSourcejTable.getValueAt(
				ProductsSourcejTable.getSelectedRow(), 0).toString();
		if (productsourceselection == "MSL") {
			selectedproductsource = productsourceselection;
			int tabcnt = ReceivingJD.jTabbedPane1.getTabCount();
		
			if ( tabcnt <= 1){
			ReceivingJD.jTabbedPane1.addTab("Program", new ProgramsJP());
			ReceivingJD.jTabbedPane1.setSelectedIndex(1);
			}
			productsourceselection = "";
		} else if (productsourceselection == "CHAZ") {
			
			int tabcnt = ReceivingJD.jTabbedPane1.getTabCount();
			selectedproductsource = productsourceselection;
			if ( tabcnt <= 1){
			ReceivingJD.jTabbedPane1.addTab("Program", new ProgramsJP());
			ReceivingJD.jTabbedPane1.setSelectedIndex(1);
			}
			productsourceselection = "";
		}
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JTable ProductsSourcejTable;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

}