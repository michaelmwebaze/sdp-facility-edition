package org.elmis.forms.stores.receiving;

/***************************************************
	 * STARTING CHILD FORM FOR ADJUSTMENTS 
	 ***************************************************/
/**
 *
 * @author  __MKausa__
 */

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.controllers.facilityarvdispensingsessionsmappercalls;
import org.elmis.facility.controllers.facilityproductssetupsessionmapperscalls;
import org.elmis.facility.controllers.facilitysetupsessionsmappercalls;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.forms.stores.arv_dispensing.arvmessageDialog;
import com.oribicom.tools.TableModel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Label;

import org.elmis.forms.stores.receiving.storeAdjustmentTypeJD;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.MutableComboBoxModel;
import javax.swing.table.TableColumn;
import org.elmis.facility.controllers.facilityproductssetupsessionmapperscalls;
import org.elmis.facility.controllers.facilitysetupsessionsmappercalls;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
//import com.oribicom.tools.TableModel;
import com.toedter.calendar.JDateChooserCellEditor;
import java.awt.Font;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.GroupLayout;
import java.awt.Toolkit;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;


public class testMultipleAdjustments extends javax.swing.JDialog   {
	

	facilitysetupsessionsmappercalls callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	TableColumnAligner tablecolumnaligner; 
	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	Integer ProdQty = null;
	public String paramAdjustment = null; 
	//Facility Approved Products JTable *******************
	List<VW_Program_Facility_ApprovedProducts> adjustmensproductsList = new LinkedList();
	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card; 
	private Elmis_Stock_Control_Card Adjustmentsavestockcontrolcard;
	public Boolean  productsearchenabled = false;
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	//public static int total_programs = 0;
	//public static Map parameterMap_fproducts = new HashMap();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	facilityarvdispensingsessionsmappercalls aFacilityproductsmapper = null;
	private ListIterator<Losses_Adjustments_Types> registeradjustmentfapprovedproductsIterator = null;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator elmistockcontrolcarditerator = elmisstockcontrolcardList
			.listIterator();
	private Losses_Adjustments_Types registerAdjustmentsfacilitysccProducts;
	facilityproductssetupsessionmapperscalls Facilityproductsmapper = null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	//public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	ArrayList<Elmis_Stock_Control_Card> adjustmentstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	//List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//private VW_Systemcalculatedproductsbalance facilitysccProducts;
	public  List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private final String[] columns_RegisterAdjustmentsfacilityApprovedproducts = {
		"Adjustment Type", "Quantity", "Remarks"};
	private static final int rows_RegisterAdjustmentsfproducts = 0;
    private final Object[] defaultv_RegisterAdjustmentsfacilityapprovedproducts = { "", "",
		""};

	public TableModel tableModel_RegisterAdjustments= new TableModel(
			columns_RegisterAdjustmentsfacilityApprovedproducts,
			defaultv_RegisterAdjustmentsfacilityapprovedproducts, rows_RegisterAdjustmentsfproducts) {

		boolean[] canEdit = new boolean[] { false, true, true};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				
				if (columnIndex == 2) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,2).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		

	};


	/*private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();*/

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public testMultipleAdjustments(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname) {
		super(parent, modal);
		productdeliverdate = adjustmentdateandtime;
		Pcode = adjustpcode;
		Productid = adjustproductid;
		facilityprogramcode = progname;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 350);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}

	
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		SaveJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Store Adjustment Type ");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnCancel = new JButton();
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCancelMouseClicked(evt);
			}

			
		});
		btnCancel.setText("Cancel");
		btnCancel.setFont(new Font("Ebrima", Font.BOLD, 12));
		
		faciliytynameJTF = new JTextField();
		faciliytynameJTF.setColumns(10);
		
		enterfacilityname = new JLabel("Enter facility Name");
		enterfacilityname.setForeground(Color.WHITE);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(39)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(SaveJBtn))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 532, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(enterfacilityname)
						.addComponent(faciliytynameJTF, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(92, Short.MAX_VALUE))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(35)
					.addComponent(enterfacilityname)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(faciliytynameJTF, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(SaveJBtn)
						.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(240, Short.MAX_VALUE))
		);

	
		RegisterAdjustmentJT = new JTable();
		scrollPane.setViewportView(RegisterAdjustmentJT);
		RegisterAdjustmentJT.setShowVerticalLines(false);
		RegisterAdjustmentJT.setShowHorizontalLines(true);
		RegisterAdjustmentJT.setShowGrid(false);
		RegisterAdjustmentJT.setRowSelectionAllowed(true);
		RegisterAdjustmentJT.setModel(tableModel_RegisterAdjustments);
		RegisterAdjustmentJT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		RegisterAdjustmentJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						RegisterAdjustmentJTMouseClicked(evt);
					}
				});
		RegisterAdjustmentJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						RegisterAdjustmentJTPropertyChange(evt);
					}

					
				});
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addComponent(jPanel1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 995, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		return productdeliverdate;

	}
	public  void RegisterAdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		System.out.println("test pop up adjustments");		

	}

	
	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List registerAdjustmentfapprovProducts) {
		checkbox = new JCheckBox();
		
		tableModel_RegisterAdjustments.clearTable();

		registeradjustmentfapprovedproductsIterator = registerAdjustmentfapprovProducts.listIterator();

			while (registeradjustmentfapprovedproductsIterator.hasNext()) {

				registerAdjustmentsfacilitysccProducts = registeradjustmentfapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_RegisterAdjustmentsfacilityapprovedproducts[0] = registerAdjustmentsfacilitysccProducts.getName().toString();
									
				
				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_RegisterAdjustmentsfacilityApprovedproducts.length; j++) {
					cols.add(defaultv_RegisterAdjustmentsfacilityapprovedproducts[j]);

				}
				System.out.print(cols.size()
						+ "What is the SIZE of Array to Table??");
				tableModel_RegisterAdjustments.insertRow(cols);

				registeradjustmentfapprovedproductsIterator.remove();
			

	}
	
	}	
	
	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
       String displaymessage = "";
		
		this.RegisterAdjustmentJT.editCellAt(-1,-1);
		System.out.println(this.RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(),1 ) + "DID WE  GET THE VALUE");
		int ok = new arvmessageDialog()
		.showDialog(
				this,
				"Do  you want to save the Received Quantities to the database\n\n",
				"Saving Quantity Received Information",
				"YES, To \n Save",
				"NO, To\n Edit the Quantity Entered ", "Cancel");
	

		if(ok == 0){
			//Insert the object to database
			Adjustmentsavestockcontrolcard = new Elmis_Stock_Control_Card();
			System.out.println(adjustmentstockcontrolcardList.size() + "TEST SIZE MOSES");
			for (@SuppressWarnings("unused")
			Elmis_Stock_Control_Card sp : adjustmentstockcontrolcardList) {

				Adjustmentsavestockcontrolcard.setProductcode(sp
						.getProductcode());
				Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(sp.getIssueto_receivedfrom());
				Adjustmentsavestockcontrolcard.setQty_received(sp
						.getQty_received());

				Adjustmentsavestockcontrolcard.setQty_isssued(sp
						.getQty_isssued());
				Adjustmentsavestockcontrolcard.setAdjustments(sp
						.getAdjustments());
				Adjustmentsavestockcontrolcard.setBalance(sp.getBalance());
				Adjustmentsavestockcontrolcard.setAdjustmenttype(sp
						.getAdjustmenttype());
				Adjustmentsavestockcontrolcard.setProductid(sp.getProductid());

				Adjustmentsavestockcontrolcard.setCreatedby(sp.getCreatedby());
				Adjustmentsavestockcontrolcard.setCreateddate(sp
						.getCreateddate());
				Adjustmentsavestockcontrolcard.setProgram_area(sp
						.getProgram_area());
				Adjustmentsavestockcontrolcard.setStoreroomadjustment(sp
						.isStoreroomadjustment());
				Adjustmentsavestockcontrolcard.setId(sp.getId());
				if (sp.getRemark() != null) {
					Adjustmentsavestockcontrolcard.setRemark(sp.getRemark());
				}
				Facilityproductsmapper
						.InsertstockcontrolcardProducts(Adjustmentsavestockcontrolcard);

			}

			JOptionPane.showMessageDialog(this, "Products saved to database",
					"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
		} else if (ok == 1) {
			this.RegisterAdjustmentJT.validate();
		}
    
		
		testMultipleAdjustments.this.dispose();
		
	}
	private void btnCancelMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		testMultipleAdjustments.this.dispose();
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}
	
	protected void RegisterAdjustmentJTMouseClicked(
			MouseEvent evt) {
		System.out.println("test me now ");
		this.adjustmentname = this.RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(),0).toString();
		System.out.println(adjustmentname);
		
		if(this.adjustmentname.equals("TRANSFER_IN") || this.adjustmentname.equals("TRANSFER_OUT")){
		this.faciliytynameJTF.setVisible(true);
        this.enterfacilityname.setVisible(true);
        if(this.faciliytynameJTF.getText().equals("")){
        this.faciliytynameJTF.requestFocus();
        }
		//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		}else{
			this.faciliytynameJTF.setVisible(false);
	        this.enterfacilityname.setVisible(false);
		}
		
	}
	
	
private void RegisterAdjustmentJTPropertyChange(
			PropertyChangeEvent evt) {
		// TODO Auto-generated method stub

if ("tableCellEditor".equals(evt.getPropertyName())) {       

	if (this.RegisterAdjustmentJT.isColumnSelected(1)) {
		System.out.println("IM READY TO POP FORM- 2014");
		int col = RegisterAdjustmentJT.getSelectedColumn();
		 if (RegisterAdjustmentJT.isEditing())
		    {
		    	//get the adjustment type , quantity entered , and  or remarks 
			 
				colindex = this.RegisterAdjustmentJT
						.getSelectedColumn();
				rowindex = this.RegisterAdjustmentJT
						.getSelectedRow();
			
				
		    }else{
		    	createshipmentObj(Pcode, Productid, productdeliverdate);
		
	}					


}
	
		
		
}
	


	}

	public void createshipmentObj(String apcode, int prodid,
			Timestamp amodifieddate) {
		//System.out.println("Did object call me ???");
				//created a shipment object 
				Integer A = 0;
				Integer B = 0;
				Integer C = 0;
				Integer negativeadjust = 0;
				Integer positive = 0;
				if (productdeliverdate == null) {
					java.util.Date date = new java.util.Date();
					amodifieddate = new Timestamp(date.getTime());
				} else {
					amodifieddate = productdeliverdate;
				}

				System.out.println(amodifieddate);
				System.out.println(A);
				System.out.println(B);
				System.out.println(C);
		
				
				String prodAdjustremark = this.tableModel_RegisterAdjustments.getValueAt(rowindex,2)
						.toString();
				

				if (prodAdjustremark != null) {
					Adjustmentelmis_stock_control_card.setRemark(prodAdjustremark);
				}
				Adjustmentelmis_stock_control_card.setProductcode(apcode);

				Adjustmentelmis_stock_control_card.setProductid(prodid);
				
		        
				if (this.adjustmentname.equals("TRANSFER_OUT")) {
					
				
					try {
						
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					if(!(this.faciliytynameJTF.getText().toString().equals(""))){
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.faciliytynameJTF.getText());
					}
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
						
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						//negativeadjust = Adjustmentelmis_stock_control_card.getAdjustments();
							
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					

				}
				
				
				if (this.adjustmentname.equals("TRANSFER_IN")) {
					
		
					try {
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(positive);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}

					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.faciliytynameJTF.getText());
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									//p.setQty(p.getQty()- positive );
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}

				}
			 
				this.adjustmentname.trim();
				if (this.adjustmentname.equals("DAMAGED")) {
				
	
					
		         try{
		        	 negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
		         }catch(NumberFormatException e){
						e.getStackTrace();
					}

					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				 

				if (this.adjustmentname.equals("LOST")) {

					
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
					 
				
				if (this.adjustmentname.equals("STOLEN")) {

					
					
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
					 
				

				if (this.adjustmentname.equals("EXPIRED")) {

								
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					
					
					
				}
				

				if (this.adjustmentname.equals("PASSED_OPEN_VIAL_TIME_LIMIT")) {
					

					
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					} 
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				

				if (this.adjustmentname.equals("COLD_CHAIN_FAILURE")) {
					

					
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
												
							@SuppressWarnings("unused")
							int increase = b.getBalance() -positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				
				if (this.adjustmentname.equals("CLINIC_RETURN")) {
	
					
					
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}  
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
										
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentname.equals("FOUND")) {

					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							int increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentname.equals("PURCHASE")) {
					
	
					try{
						negativeadjust = (Integer
								.parseInt(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							int increase = b.getBalance() + positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j){
							j.getMessage();
						}
			
	}
				Adjustmentelmis_stock_control_card
						.setId(com.oribicom.tools.publicMethods.createGUID());//this.GenerateGUID());
				A = Adjustmentelmis_stock_control_card.getQty_received();
				B = Adjustmentelmis_stock_control_card.getQty_isssued();
				C = Adjustmentelmis_stock_control_card.getAdjustments();

			  
				System.out.println(A + "WHAT IS  THE NUMBER");
				System.out.println(B);
				System.out.println(C);

				Adjustmentelmis_stock_control_card
						.setAdjustmenttype(this.adjustmentname);
				System.out.println(Adjustmentelmis_stock_control_card.getBalance());
				//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));
				Adjustmentelmis_stock_control_card.setStoreroomadjustment(true);
				Adjustmentelmis_stock_control_card.setProgram_area(facilityprogramcode);
				Adjustmentelmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
				Adjustmentelmis_stock_control_card.setCreateddate(amodifieddate);

				//created linked list object 

				//adjustmentstockcontrolcardList.add(savestockcontrolcard);
				adjustmentstockcontrolcardList.add(Adjustmentelmis_stock_control_card);
				System.out.println(adjustmentstockcontrolcardList.size()
						+ "ARRAY SIZE???");
				System.out.println("Array size here &&&");
				Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
				//return saveShipped_Line_Items; 		
 }
				

				
	

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.RegisterAdjustmentJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		RegisterAdjustmentJT.setFont(new java.awt.Font(
				"Ebrima", 0, 20));
		this.RegisterAdjustmentJT.setRowHeight(30);
		this.PopulateProgram_ProductTable(facilityAdjustmentList);
		this.faciliytynameJTF.setVisible(false);
        this.enterfacilityname.setVisible(false);
		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		
	}
	
		/**
	 * @param args the command line arguments
	 */

	private javax.swing.JButton SaveJBtn;
	private javax.swing.JPanel jPanel1;
	private JTable RegisterAdjustmentJT;
	private JTextField faciliytynameJTF;
	private JLabel enterfacilityname;

}
	
	/**********************************************************
	 * END CHILD FORM 
	 **********************************************************/
			 
